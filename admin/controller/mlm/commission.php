<?php    
class ControllerMlmCommission extends Controller { 
	private $error = array();
  
  	public function index() {
		$this->load->language('mlm/commission');
		
		$this->document->setTitle($this->language->get('heading_title'));
		 
		$this->load->model('mlm/commission');
		
    	$this->getList();
  	}
  
  	public function insert() {
  	
		$this->load->language('mlm/commission');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('mlm/commission');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm(0)) {
		
			$this->model_mlm_commission->addCommission($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('mlm/commission', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
    
    	$this->getForm();
  	} 
   
  	public function update() {
  	
		$this->load->language('mlm/commission');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('mlm/commission');
		
    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm( $this->request->get['commission_id'] )) {
    	
			$this->model_mlm_commission->editCommission($this->request->get['commission_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('mlm/commission', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
    
    	$this->getForm();
  	}   

  	public function delete() {
  	
		$this->load->language('mlm/commission');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('mlm/commission');
			
    	if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $commission_id) {
				$this->model_mlm_commission->deleteCommission($commission_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('mlm/commission', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    	}
	
    	$this->getList();
  	}  
    
  	private function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'level';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
				
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('mlm/commission', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
							
		$this->data['insert'] = $this->url->link('mlm/commission/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('mlm/commission/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['commissions'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$commission_total = $this->model_mlm_commission->getTotalCommissions();
	
		$results = $this->model_mlm_commission->getCommissions($data);
        $this->load->model('mlm/packages');
       
    	foreach ($results as $result) {
    	   
           
            $packdetails = $this->model_mlm_packages->getPackagedetails($result['package_id']) ;
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('mlm/commission/update', 'token=' . $this->session->data['token'] . '&commission_id=' . $result['incentive_id'] . $url, 'SSL')
			);
						
			$this->data['commissions'][] = array(
				'commission_id'    => $result['incentive_id'],
				'level'            => $result['incentive_level'],
				'commission'       => $result['incentive_val'],
                'packname'         => $packdetails['package_name'],
				'selected'         => isset($this->request->post['selected']) && in_array($result['commission_id'], $this->request->post['selected']),
				'action'           => $action
			);
		}	
	
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_level'] = $this->language->get('column_level');
		$this->data['column_commission'] = $this->language->get('column_commission');
		$this->data['column_action'] = $this->language->get('column_action');		
		
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_level'] = $this->url->link('mlm/commission', 'token=' . $this->session->data['token'] . '&sort=level' . $url, 'SSL');
		$this->data['sort_commission'] = $this->url->link('mlm/commission', 'token=' . $this->session->data['token'] . '&sort=commission' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $commission_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('mlm/commission', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			
		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'mlm/commission_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
  
  	private function getForm() {
    	$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['entry_level'] = $this->language->get('entry_level');
		$this->data['entry_commission'] = $this->language->get('entry_commission');
		  
    	$this->data['button_save'] = $this->language->get('button_save');
    	$this->data['button_cancel'] = $this->language->get('button_cancel');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['level'])) {
			$this->data['error_level'] = $this->error['level'];
		} else {
			$this->data['error_level'] = '';
		}
		
		if (isset($this->error['commission'])) {
			$this->data['error_commission'] = $this->error['commission'];
		} else {
			$this->data['error_commission'] = '';
		}
		
		if (isset($this->error['level_commission_exists'])) {
			$this->data['error_level_commission_exists'] = $this->error['level_commission_exists'];
		} else {
			$this->data['error_level_commission_exists'] = '';
		}
		    
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('mlm/commission', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
							
		if (!isset($this->request->get['commission_id'])) {
			$this->data['action'] = $this->url->link('mlm/commission/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('mlm/commission/update', 'token=' . $this->session->data['token'] . '&commission_id=' . $this->request->get['commission_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('mlm/commission', 'token=' . $this->session->data['token'] . $url, 'SSL');

    	if (isset($this->request->get['commission_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$commission_info = $this->model_mlm_commission->getCommission($this->request->get['commission_id']);
    	}

		$this->data['token'] = $this->session->data['token'];

    	if (isset($this->request->post['level'])) {
      		$this->data['level'] = $this->request->post['level'];
    	} elseif (!empty($commission_info)) {
			$this->data['level'] = $commission_info['incentive_level'];
		} else {
      		$this->data['level'] = '';
    	}
        
        
        if (isset($this->request->post['level'])) {
      		$this->data['level'] = $this->request->post['level'];
    	} elseif (!empty($commission_info)) {
			$this->data['level'] = $commission_info['incentive_level'];
		} else {
      		$this->data['level'] = '';
    	}
    	
        
    	
    	if (isset($this->request->post['commission'])) {
      		$this->data['commission'] = $this->request->post['commission'];
    	} elseif (!empty($commission_info)) {
			$this->data['commission'] = $commission_info['incentive_val'];
		} else {	
      		$this->data['commission'] = '';
    	}
		
		$this->template = 'mlm/commission_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}  
	 
  	private function validateForm( $commission_id ) {
  		$this->load->model('mlm/commission');
  		
    	if (!$this->user->hasPermission('modify', 'mlm/commission')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}

    	if ($this->request->post['level'] < 1) {
      		$this->error['level'] = $this->language->get('error_level');
    	}
    	
    	if ($this->request->post['commission'] == '') {
      		$this->error['commission'] = $this->language->get('error_commission');
    	}
    	/*
    	$levelCommissionAlreadyExists = $this->model_mlm_commission->levelCommissionAlreadyExists( $this->request->post['level'], $commission_id );
		if($levelCommissionAlreadyExists) {
			$this->error['level_commission_exists'] = $this->language->get('error_level_commission_exists');
		}
		*/
		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}    

  	private function validateDelete() {
    	if (!$this->user->hasPermission('modify', 'mlm/commission')) {
			$this->error['warning'] = $this->language->get('error_permission');
    	}	
		
		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}  
  	}
}
?>
