<?php 
error_reporting(0);   
class ControllerMlmDetails extends Controller
 { 
	private $error = array();
  
  	public function index() 
	{
		$this->document->setTitle('Downline Details');
		$this->load->model('mlm/downline');
		$url = '';
		$this->data['breadcrumbs'] = array();
   		$this->session->data['error']='';
      	///Grab Joining options from DB
		///
		$this->data['userid'] ='';
        
          
		
	   if($this->request->server['REQUEST_METHOD'] == 'POST')
		{
		  
            
            //echo "<pre>";print_r($_POST);
            
		  	$username = $_POST['username'];
			
			$this->data['userid'] = $username ;
						
			$this->data['selectedusername'] = $username;
			
			$this->data['leftdownloadlink'] = $this->url->link('mlm/details/printLeft&username='.$username, 'token=' . $this->session->data['token'] , 'SSL');
			
			$this->data['rightdownloadlink'] = $this->url->link('mlm/details/printRight&username='.$username, 'token=' . $this->session->data['token'] , 'SSL');

			$memberresults =  $this->db->query("select * from ". DB_PREFIX . "customer where member_id = '".strtoupper(trim($username))."'")->row;
			
            
            //echo "<pre>";print_r($memberresults);
			
			if(!isset($memberresults["customer_id"]) && empty($memberresults["customer_id"]) )
			{
				$this->session->data['error'] ="Invalid username or this username doest not exist in database";
			
			} else 
            {


               
				$userid = $memberresults['customer_id'];
                

				$this->data['lefPVtcount']   = $this->model_mlm_downline->cntlftPV($userid);
				$this->data['rightPVcount']  = $this->model_mlm_downline->cntrgtPV($userid);
                $this->data['middlePVcount']  = $this->model_mlm_downline->cntmiddPV($userid);
              
			
				$this->data['leftcount']     = $this->model_mlm_downline->cntlft($username);
                $this->data['rightcount']     = $this->model_mlm_downline->cntrgt($username);
                $this->data['middlecount']     = $this->model_mlm_downline->cntmidd($username);
                
                $this->data['lmemids']    = $this->model_mlm_downline->leftmembersid($userid);  
                $this->data['cmemids']    = $this->model_mlm_downline->middlemembersid($userid);
    			$this->data['rmemids']    = $this->model_mlm_downline->rightmembersid($userid); 
			}
           
    		
            
            //echo "left-".$this->data['lmemids'].'middle'.$this->data['cmemids']."right".$this->data['rmemids'];
            
           
			
	 	}
		///
		if(isset($this->session->data['error']))
		{
			$this->data["error"] = $this->session->data['error'];
		}
		else{
			$this->data["error"] = '';
		}
		
		if(isset($this->session->data['success']))
		{
			$this->data["success"] = $this->session->data['success'];
		}
		else{
			$this->data["success"] = '';
		}
		
		$this->data["heading_title"] = "Get Downline Details";
		$this->template = 'mlm/downlinedetails.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
		$this->response->setOutput($this->render());
  	}
    
    
    
     public function cntlft($rt)
   {	
		$cnt=0;
		$res = mysql_query("select * from ".DB_PREFIX."membertree where member_id = '".$rt."' and left_sponsorcode <> '' ");
		if(mysql_num_rows($res) > 0)
		{
			$cnt++;
			$r  = mysql_fetch_array($res);
			$msrn1 = mysql_fetch_array(mysql_query("select * from ".DB_PREFIX."membertree where member_id = '".$r["left_sponsorcode"]."'"));
			if(isset($msrn1['left_sponsorcode']) && !empty($msrn1['left_sponsorcode']))
			{
				$msrn2 = mysql_fetch_array(mysql_query("select * from ".DB_PREFIX."membertree where member_id = '".$msrn1["left_sponsorcode"]."'"));
				$cnt++;
				$ids = $msrn2['member_id'];
				$cnt += $this->cntlft($ids) + $this->cntrgt($ids);
			}
			if(isset($msrn1['right_sponsorcode']) && !empty($msrn1['right_sponsorcode']))
			{
				$msrn3 = mysql_fetch_array(mysql_query("select * from ".DB_PREFIX."membertree where member_id ='".$msrn1["right_sponsorcode"]."'"));
				$cnt++;
				$ids = $msrn3['member_id'];
				$cnt += $this->cntlft($ids) + $this->cntrgt($ids);
			}
		}  
		return $cnt;
	}

   public function cntrgt($rt)
   {
		$cnt=0;
		$res = mysql_query("select * from ".DB_PREFIX."membertree where member_id = '".$rt."' and right_sponsorcode <> '' ");
		if(mysql_num_rows($res) > 0)
		{
			$cnt++;
			$r  = mysql_fetch_array($res);
			$msrn1 = mysql_fetch_array(mysql_query("select * from ".DB_PREFIX."membertree where member_id = '".$r["right_sponsorcode"]."'"));
			if(isset($msrn1['left_sponsorcode']) && !empty($msrn1['left_sponsorcode'] ))
			{
				$msrn2 = mysql_fetch_array(mysql_query("select * from ".DB_PREFIX."membertree where member_id ='".$msrn1["left_sponsorcode"]."'"));
				$cnt++;
				$ids = $msrn2['member_id'];
				
				$cnt += $this->cntlft($ids) + $this->cntrgt($ids);
			}
			if(isset($msrn1['right_sponsorcode']) && !empty($msrn1['right_sponsorcode']))
			{
				$msrn3 = mysql_fetch_array(mysql_query("select * from ".DB_PREFIX."membertree where member_id ='". $msrn1["right_sponsorcode"]."'"));
				$cnt++;
				$ids = $msrn3['member_id'];
				$cnt += $this->cntlft($ids) + $this->cntrgt($ids);
			}			
		}
		return $cnt;
    }


public function leftmembersid($rt)
{
    $leftids='';
    $res = $this->db->query("select * from ".DB_PREFIX."membertree where customer_id = '".$rt."' and left_sponsorcode <> '' ")->row;
    if(isset($res["left_sponsorcode"]) && !empty($res["left_sponsorcode"]))
    {
	$msrn1 = $this->db->query("select * from ".DB_PREFIX."membertree where member_id = '".$res["left_sponsorcode"]."'")->row;
	$mid = $msrn1['customer_id'];			
	if(isset($mid) && $mid > 0 )
	{
	     $endchar =  substr($leftids,-1);
             if((isset($endchar) && $endchar == ',') || empty($leftids))
             {		           
	 	$leftids = $leftids .$mid;
	
             }else{
		
                $leftids = $leftids .",".$mid ;
	     }
         
         }
			
	if(isset($msrn1['left_sponsorcode']) && !empty($msrn1['left_sponsorcode']))
        {
	  $msrn2 = $this->db->query("select * from ".DB_PREFIX."membertree where member_id = '".$msrn1["left_sponsorcode"]."'")->row;
	  $ids = $msrn2['customer_id'];
	  if(isset($ids) && $ids > 0 ) 
	  {
                $endchar =  substr($leftids,-1);
	        if((isset($endchar) && $endchar == ',') || empty($leftids))
		{		           
		   $leftids = $leftids.$ids ;
		}else{
		   $leftids = $leftids.",".$ids  ;
	        }
           }	
	
         if(isset($ids) && strlen($this->leftmembersid($ids)) > 0 )
         {
		 $leftids = $leftids.",".$this->leftmembersid($ids);
	 }
         
         if(isset($ids) && strlen($this->rightmembersid($ids)) > 0 )
          {
                 $leftids = $leftids.",".$this->rightmembersid($ids); 
	  }
        }  
 			
        if(isset($msrn1['right_sponsorcode']) && !empty($msrn1['right_sponsorcode']))
	{
	  $msrn3 = $this->db->query("select * from ".DB_PREFIX."membertree where member_id = '".$msrn1["right_sponsorcode"]."'")->row;
	  $ids = $msrn3['customer_id'];
	  if(isset($ids) && $ids > 0 )
          {
		 $endchar =  substr($leftids,-1);
	         if(isset($endchar) && $endchar == ',' || empty($leftids))
	         {		           
		   $leftids = $leftids.$ids   ;
	
                 }else{
          	  
                   $leftids = $leftids.",".$ids ;
		
                 }
           }
	  if(isset($ids) && strlen($this->leftmembersid($ids)) > 0 )
	  {
		 $leftids = $leftids.",".$this->leftmembersid($ids);
	  }
          if(isset($ids) && strlen($this->rightmembersid($ids)) > 0 )
	  {
		 $leftids = $leftids.",".$this->rightmembersid($ids); 
	  }			
         }
    }
    return $leftids;
}
public function rightmembersid($rt)
 {
	$leftids='';
	$res = $this->db->query("select * from ".DB_PREFIX."membertree where customer_id = '".$rt."' and right_sponsorcode <> '' ")->row;
	if(isset($res["right_sponsorcode"]) && !empty($res["right_sponsorcode"]))
	{
 	$msrn1 = $this->db->query("select * from ".DB_PREFIX."membertree where member_id = '".$res["right_sponsorcode"]."'")->row;
	if(isset($msrn1["customer_id"]))
        {
	    $mid = $msrn1['customer_id'];		
            if(isset($mid) && $mid > 0 )  
            {	
                $endchar =  substr($leftids,-1);
                if(isset($endchar) && $endchar == ',' || empty($leftids)) 
		 {		           
                        $leftids = $leftids.$mid;
			
                 }else{
			         	 
		 	$leftids = $leftids.",".$mid;
			           
                 }  
	
             }
        if(isset($msrn1['left_sponsorcode']) && !empty($msrn1['left_sponsorcode']))                         
	{
            $msrn2 = $this->db->query("select * from ".DB_PREFIX."membertree where member_id = '".$msrn1["left_sponsorcode"]."'")->row;
            $ids = $msrn2['customer_id'];
    	   if(isset($ids) && $ids > 0 )
            {
		$endchar =  substr($ids,-1);
		if(isset($endchar) && $endchar == ',' || empty($leftids))
                 {		           
			$leftids = $leftids.$ids;			        
                        
                 } else {
                        $leftids = $leftids.",".$ids  ;
			
                }
		if(isset($ids) && strlen($this->leftmembersid($ids)) > 0 )
		{
                       $leftids = $leftids.",".$this->leftmembersid($ids);
                }
                if(isset($ids) && strlen($this->rightmembersid($ids)) > 0 )
		{
                       $leftids = $leftids.",".$this->rightmembersid($ids); 
                } 
             }
         }			
	 if(isset($msrn1['right_sponsorcode']) && !empty($msrn1['right_sponsorcode']))    
	  {
		$msrn3 = $this->db->query("select * from ".DB_PREFIX."membertree where member_id = '".$msrn1["right_sponsorcode"]."'")->row;
		$ids  = $msrn3['customer_id'];
		if(isset($ids) && $ids > 0 )
		{
                    $endchar =  substr($ids,-1);
		    if(isset($endchar) && $endchar == ',' || empty($leftids))
		    {		           
			$leftids = $leftids.$ids   ;
		
                    }else{
		
                        $leftids = $leftids.",".$ids  ;
			
                    }   
		    if(isset($ids) && strlen($this->leftmembersid($ids)) > 0 )
                    {
		
                        $leftids = $leftids.",".$this->leftmembersid($ids);
                    }
                    if(isset($ids) && strlen($this->rightmembersid($ids)) > 0 )
                    {
			 $leftids = $leftids.",".$this->rightmembersid($ids); 
                    } 
                }
           }
       }
    }
	return $leftids;
} 
   }
  
?>