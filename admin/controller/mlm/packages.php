<?php
/*
 * @ Dated: 14 June 2014
 * @ This file is used for manage the registration packages in system.
 */ 
class ControllerMlmPackages extends Controller { 
	private $error = array();

	public function index() {            
		$this->load->language('mlm/packages');
		$this->document->setTitle($this->language->get('heading_title'));		 
		$this->load->model('mlm/packages');
		$this->getList();
	}
   
	public function update() {
            
		$this->load->language('mlm/packages');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('mlm/packages');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) { // && $this->validateForm()
			$this->model_mlm_packages->editPackage($this->request->get['packageid'], $this->request->post);
			if (isset($_FILES["package_image"]["name"]) && !empty($_FILES["package_image"]["name"])) 
                    {
                            $package_image = $_FILES["package_image"]["name"];
                            $tim = time();
                            if ($package_image != '') {
                                $package_image = $tim . $package_image;
                            } else {
                                $package_image = '';
                            }
                            $folder_path = DIR_IMAGE."packages/";                    
                            if ($package_image != "") {
                                @copy(@$_FILES["package_image"]["tmp_name"], $folder_path . $package_image);                        
                            }                  
                            $this->request->post["package_image"] = $package_image;                    
                            $this->model_mlm_packages->updatePackageSrc($this->request->get['packageid'], $this->request->post);
                            
        			}
                    $this->session->data['success'] = $this->language->get('text_success');
					$url = '';
					if (isset($this->request->get['sort'])) {
						$url .= '&sort=' . $this->request->get['sort'];
				    }

					if (isset($this->request->get['order'])) {
						$url .= '&order=' . $this->request->get['order'];
					}

					if (isset($this->request->get['page'])) {
						$url .= '&page=' . $this->request->get['page'];
					}
			
					$this->redirect($this->url->link('mlm/packages', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
                    $this->getForm();
	} 
	public function delete() {
			$this->load->language('mlm/packages');
			$this->document->setTitle($this->language->get('heading_title'));
			$this->load->model('mlm/packages');
			if (isset($this->request->get['packageid']) && $this->validateDelete()) {
				$this->model_membership_packages->deletePackage($this->request->get['packageid']);			
				$this->session->data['success'] = $this->language->get('text_success');
				$url = '';
				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}
				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}
				
				$this->redirect($this->url->link('mlm/packages', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
				$this->getList();
	}    

 ############## Show packages list  ##########################
	private function getList() {
				if (isset($this->request->get['sort'])) {
					$sort = $this->request->get['sort'];
				} 
		        else 
		        {
					$sort = 'package_adddate';
				}
				
				if (isset($this->request->get['order'])) {
					$order = $this->request->get['order'];
				} else {
					$order = 'ASC';
				}
				
				if (isset($this->request->get['page'])) {
					$page = $this->request->get['page'];
				} else {
					$page = 1;
				}
				
				$url = '';
					
				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}
		
				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}
				
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}
		
		  		$this->data['breadcrumbs'] = array();
		
		   		$this->data['breadcrumbs'][] = array(
		                        'text'      => $this->language->get('text_home'),
					'href'      => $this->url->link('mlm/packages', 'token=' . $this->session->data['token'], 'SSL'),
		      		'separator' => false
		   		);
		
		   		$this->data['breadcrumbs'][] = array(
		                        'text'      => $this->language->get('heading_title'),
					'href'      => $this->url->link('mlm/packages', 'token=' . $this->session->data['token'] . $url, 'SSL'),
		                        'separator' => ' :: '
		   		);
									
				$this->data['insert'] = $this->url->link('mlm/packages/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
				$this->data['delete'] = $this->url->link('mlm/packages/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		
				$this->data['informations'] = array();
		
				$data = array(
					'sort'  => $sort,
					'order' => $order,
					'start' => ($page - 1) * $this->config->get('config_admin_limit'),
					'limit' => $this->config->get('config_admin_limit')
				);
				
				$packages_total = $this->model_mlm_packages->getTotalPackages();
				$results = $this->model_mlm_packages->getPackages($data);
	    		foreach ($results as $result) {
					$action = array();							
					$action[] = array(
						'text' => $this->language->get('text_edit'),
						'href' => $this->url->link('mlm/packages/update', 'token=' . $this->session->data['token'] . '&packageid=' . $result['package_id'] . $url, 'SSL')
					);
				    $action[] = array(
						'text' => $this->language->get('text_delete'),
						'href' => $this->url->link('mlm/packages/delete', 'token=' . $this->session->data['token'] . '&packageid=' . $result['package_id'] . $url, 'SSL')
					);
                    
                    $date= date('d/m/Y',strtotime($result['package_adddate']));
                    
					$this->data['packages'][] = array(
				                'package_id' =>         $result['package_id'],
                                'package_name'          => $result['package_name'],  
                                'package_monthly'       => $this->currency->format($result['package_monthly_fees']),
                                'package_annual'        => $this->currency->format($result['package_annual_fees']),
                                
                                'pv_value'              =>$result['pv_value'],
				                'package_description'   => $result['package_descr'],                           
                                'package_image'         => $result['package_img'],
                                'package_adddate'       => $date,				
			                 	'action'                => $action
					);
		          }	
				    //$_['entry_pv_value']
                    $this->data['entry_pv_value'] = $this->language->get('entry_pv_value');
					$this->data['heading_title'] = $this->language->get('heading_title');
					$this->data['text_no_results'] = $this->language->get('text_no_results');
					$this->data['column_title'] = $this->language->get('column_title');
					$this->data['column_sort_order'] = $this->language->get('column_sort_order');
					$this->data['column_action'] = $this->language->get('column_action');		
					$this->data['button_insert'] = $this->language->get('button_insert');
					$this->data['button_delete'] = $this->language->get('button_delete'); 
			 		if (isset($this->error['warning'])) {
						$this->data['error_warning'] = $this->error['warning'];
					} else {
						$this->data['error_warning'] = '';
					}		
					if (isset($this->session->data['success'])) {
						$this->data['success'] = $this->session->data['success'];		
						unset($this->session->data['success']);
					} else {
						$this->data['success'] = '';
					}
			
					$url = '';
			
					if ($order == 'ASC') {
						$url .= '&order=DESC';
					} else {
						$url .= '&order=ASC';
					}
			
					if (isset($this->request->get['page'])) {
						$url .= '&page=' . $this->request->get['page'];
					}
		
					$this->data['sort_title'] = $this->url->link('mlm/packages', 'token=' . $this->session->data['token'] . '&sort=id.package_id' . $url, 'SSL');
					$this->data['sort_sort_order'] = $this->url->link('mlm/packages', 'token=' . $this->session->data['token'] . '&sort=i.package_id' . $url, 'SSL');
					
					$url = '';
			
					if (isset($this->request->get['sort'])) {
						$url .= '&sort=' . $this->request->get['sort'];
					}
															
					if (isset($this->request->get['order'])) {
						$url .= '&order=' . $this->request->get['order'];
					}
			
					$pagination = new Pagination();
					$pagination->total = $packages_total;
					$pagination->page = $page;
					$pagination->limit = $this->config->get('config_admin_limit');
					$pagination->text = $this->language->get('text_pagination');
					$pagination->url = $this->url->link('mlm/packages', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
						
					$this->data['pagination'] = $pagination->render();
			
					$this->data['sort'] = $sort;
					$this->data['order'] = $order;
			
					$this->template = 'mlm/packages_list.tpl';
					$this->children = array(
						'common/header',
						'common/footer'
					);
							
					$this->response->setOutput($this->render());
		}
################################## update packages form###########################
	private function getForm() {	   
        $this->load->model('tool/image');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');		
		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_bottom'] = $this->language->get('entry_bottom');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');    	
		$this->data['tab_general'] = $this->language->get('tab_general');
        $this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_design'] = $this->language->get('tab_design');        
        $this->data['entry_pkgname'] = $this->language->get('entry_pkgname');
        $this->data['entry_pkg_month_cost'] = $this->language->get('entry_pkg_month_cost');
        $this->data['entry_pkg_annual_cost'] = $this->language->get('entry_pkg_annual_cost');
        $this->data['entry_pkg_dsc'] = $this->language->get('entry_pkg_dsc');
        $this->data['entry_pkg_image'] = $this->language->get('entry_pkg_image');               
                
        if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['title'])) {
			$this->data['error_title'] = $this->error['title'];
		} else {
			$this->data['error_title'] = array();
		}
               
        if (isset($this->error['error_package_name'])) {
			$this->data['error_package_name'] = $this->error['error_package_name'];
		} else {
			$this->data['error_package_name'] = '';
		}
        if (isset($this->error['error_package_month_cost'])) {
			$this->data['error_package_month_cost'] = $this->error['error_package_month_cost'];
		} else {
			$this->data['error_package_month_cost'] = '';
		} 
        if (isset($this->error['error_package_annual_cost'])) {
			$this->data['error_package_annual_cost'] = $this->error['error_package_annual_cost'];
		} else {
			$this->data['error_package_annual_cost'] = '';
		} 
        if (isset($this->error['error_package_description'])) {
			$this->data['error_package_description'] = $this->error['error_package_description'];
		} else {
			$this->data['error_package_description'] = '';
		} 
        
         
	 
		
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('mlm/packages', 'token=' . $this->session->data['token'], 'SSL'),     		
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('mlm/packages', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
							
		if (!isset($this->request->get['packageid'])) {
			$this->data['action'] = $this->url->link('mlm/packages/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('mlm/packages/update', 'token=' . $this->session->data['token'] . '&packageid=' . $this->request->get['packageid'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('mlm/packages', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['packageid']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
                    
			$package_details = $this->model_mlm_packages->getPackagedetails($this->request->get['packageid']);
		}       
		$this->data['token'] = $this->session->data['token'];
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
        
		if (isset($this->request->post['package_name'])) {
			$this->data['package_name'] = $this->request->post['package_name'];
		} elseif (!empty($package_details)) {
			$this->data['package_name'] = $package_details['package_name'];
		} else {
			$this->data['package_name'] = '';
		}  
                  
        if (isset($this->request->post['package_description'])) {
			$this->data['package_description'] = $this->request->post['package_description'];
		} elseif (!empty($package_details)) {
			$this->data['package_description'] = $package_details['package_descr'];
		} else {
			$this->data['package_description'] = '';
		}
               
		if (isset($this->request->post['package_annual_cost'])) {
			$this->data['package_annual_fees'] = $this->request->post['package_annual_cost'];
		} elseif (!empty($package_details)) {
			$this->data['package_annual_fees'] = ($package_details['package_annual_fees']);
		} else {
			$this->data['package_annual_fees'] = '';
		}
        
        
        
        if (isset($this->request->post['pv_value'])) {
			$this->data['pv_value'] = $this->request->post['pv_value'];
		} elseif (!empty($package_details)) {
			$this->data['pv_value'] = ($package_details['pv_value']);
		} else {
			$this->data['pv_value'] = '';
		}
        
        
        
       // echo $this->currency->format('85');die;
        
        if (isset($this->request->post['package_month_cost'])) {
			$this->data['package_monthly_fees'] = $this->request->post['package_month_cost'];
            
		} elseif (!empty($package_details)) {
			$this->data['package_monthly_fees'] =  ($package_details['package_monthly_fees']);
		} else {
			$this->data['package_monthly_fees'] = '';
		}
                 

       if (isset($this->request->post['package_image'])) 
            {
                $this->data['thumb'] =$this->request->post['package_image'];
			
            } 
     	elseif(!empty($package_details['package_img'])) 
            {
                
                //HTTP_IMAGE."packages/".$package_details['package_image'];
                $this->data['thumb'] = HTTP_IMAGE."packages/".$package_details['package_img'];
            }
        else 
            {
			     $this->data['thumb'] = '';
            }
		 
		$this->load->model('design/layout');		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		 
		$this->template = 'mlm/packages_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	private function validateForm() {
		if (!$this->user->hasPermission('modify', 'mlm/packages')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
	   	if ((utf8_strlen($this->request->post['package_name']) < 1)) {
			$this->error['error_package_name'] = $this->language->get('error_package_name');
		}
        
        if ((utf8_strlen($this->request->post['package_month_cost']) < 1)) {
			$this->error['error_package_month_cost'] = $this->language->get('error_package_month_cost');
		}
        
         if ((utf8_strlen($this->request->post['package_annual_cost']) < 1)) {
			$this->error['error_package_annual_cost'] = $this->language->get('error_package_annual_cost');
		}
        
         if ((utf8_strlen($this->request->post['package_description']) < 1)) {
			$this->error['error_package_description'] = $this->language->get('error_package_description');
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
        //echo "<pre>";print_r($this->error);die;
			
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>