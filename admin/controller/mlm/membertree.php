<?php 
class ControllerMlmMembertree extends Controller {
	private $error = array();
	      
  	public function index() {
  
      /*  if (!$this->customer->isLogged()) 
        {
			$this->session->data['redirect'] = $this->url->link('mlm/edit', '', 'SSL');

			$this->redirect($this->url->link('mlm/login', '', 'SSL'));
        }*/	   
        $this->language->load('mlm/membertree');
      //  $this->load->model('mlm/downline');
        $this->document->setTitle($this->language->get('heading_title'));
    	 
      	$this->data['breadcrumbs'] = array();
      	
        $this->data['breadcrumbs'][] = array(
                        'text'      => $this->language->get('text_home'),
						'href'      => $this->url->link('common/home'),        	
                        'separator' => false
                ); 
      	$this->data['breadcrumbs'][] = array(
			        	'text'      => $this->language->get('text_account'),
						'href'      => $this->url->link('mlm/dashboard', '', 'SSL'),      	
			        	'separator' => $this->language->get('text_separator')
			   	);
		
      	$this->data['breadcrumbs'][] = array(
			        	'text'      => $this->language->get('text_membertree'),
						'href'      => $this->url->link('mlm/membertree', '', 'SSL'),      	
			        	'separator' => $this->language->get('text_separator')
      			);
		
	/*	if(!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');	  
            $this->redirect($this->url->link('account/login', '', 'SSL'));
    	}   */                              
   
		$this->data['heading_title'] = $this->language->get('heading_title');		
		$this->data['button_continue'] = $this->language->get('button_continue');
    	$this->data["backbtn"] = $this->url->link('mlm/membertree', '', 'SSL');  
       
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		      
		if(isset($_REQUEST["memberid"])) {
		  	$memberid = $_REQUEST["memberid"];
			if(!empty($memberid))
			{
		  	$customerProfile =  $this->db->query("select customer_id from ".DB_PREFIX."customer where member_id  = '".$memberid."'")->row ; 
 			$rootNode  =  $customerProfile["customer_id"]; 
			}
                        else
                        {
				$rootNode = 1;
			}
		}else if(isset($_REQUEST["rootNode"]) && $_REQUEST["rootNode"] > 0 )
		{
			$rootNode = $_REQUEST["rootNode"];
		} else 
                    {

        	$rootNode =1;
		}
			 
		$this->data["link"] = $this->url->link('mlm/membertree', '', 'SSL');
                $this->data['rootNode'] 	= $rootNode;
		$this->data['leftcount'] 	= $this->cntlft($rootNode);
		$this->data['rightcount'] 	= $this->cntrgt($rootNode );		
			      
        
	    
			$this->template = 'mlm/membertree.tpl';
		
		
		$this->children = array(
			
			'common/footer',
			'common/header'	
		);
				
		$this->response->setOutput($this->render());	
  	
	}


   public function AddTree($cid)
   {
            $listing_arr=$this->db->query("SELECT t.customer_id as tcid ,  t.* ,  (select concat_ws(' ',firstname,lastname) from ".DB_PREFIX."customer where customer_id = t.customer_id) as cname FROM ".DB_PREFIX."membertree t where t.sponsor_customerid = '". $cid."'")->rows;
            $strChildTree = "children: [";
            
            foreach ($listing_arr as $cus)
            {
                $customer_id = (int)$cus["tcid"];
                $customer_name = (string)$cus["member_id"];
                $strChildTree  .= '{
                          id: '.$customer_id.',
                          name:"'.$customer_name.'",
                          data: {} ,';            
                $strChildTree .= $this->AddTree($customer_id);              
                $strChildTree .= "},";
                
            }
            
               if(isset($strChildTree) && !empty($strChildTree) && strlen($strChildTree) > 12)
               {
                   $strChildTree = substr($strChildTree, 0,-1);
               }
               $strChildTree .= "]" ;
               return $strChildTree;
   }

public function GetMemberDetail()
 {
          if(isset($_REQUEST["memberid"]))
          {
              
            $memberid=$_REQUEST["memberid"];
            $customerinfo = $this->db->query("select c.*,t.* from ".DB_PREFIX."customer as c , ".DB_PREFIX."membertree as t where c.customer_id = t.customer_id and c.customer_id = '".$memberid."'")->row;
            $string = ''; 
            $string .= "<table cellpadding='5' cellspacing='5'>";
            if(isset($customerinfo["customer_id"]))
            {
               $string .= "<tr><td>Full Name:</td><td>".$customerinfo["firstname"].'&nbsp;'.$customerinfo["lastname"]."</td></tr>";               
               $string .= "<tr><td>Member ID:</td><td>".$customerinfo["member_id"]."</td></tr>";               
               $string .= "<tr><td>Sponsor Code:</td><td>".$customerinfo["sponsor_code"]."</td></tr>";               
               $string .= "<tr><td>Register On:</td><td>".date("d M Y",  strtotime($customerinfo["date_added"]))."</td></tr>";               
            }
            $string .= "</table>";
            echo $string;
            exit();
          }
 }

public function cntlft($rt)
   {	
		$cnt=0;
		$res = $this->db->query("select * from ".DB_PREFIX."membertree where member_id = '".$rt."' and left_sponsorcode <> '' ")->row;
		if(isset($res["left_sponsorcode"]) && !empty($res["left_sponsorcode"]))
		{
			$cnt++;
			$msrn1 = $this->db->query("select * from ".DB_PREFIX."membertree where member_id = '".$res["left_sponsorcode"]."'")->row;
			if(isset($msrn1['left_sponsorcode']) && !empty($msrn1['left_sponsorcode']))
			{
				$msrn2 = $this->db->query("select * from ".DB_PREFIX."membertree where member_id = '".$msrn1["left_sponsorcode"]."'")->row;
				$cnt++;
				$ids = $msrn2['member_id'];
				$cnt += $this->cntlft($ids) + $this->cntrgt($ids);
			}
			if(isset($msrn1['right_sponsorcode']) && !empty($msrn1['right_sponsorcode']))
			{
				$msrn3 = $this->db->query("select * from ".DB_PREFIX."membertree where UPPER(member_id) ='".strtoupper($msrn1["right_sponsorcode"])."'")->row;
				$cnt++;
				$ids = $msrn3['member_id'];
				$cnt += $this->cntlft($ids) + $this->cntrgt($ids);
			}
		}  
		return $cnt;
	}
        
    public function cntrgt($rt)
    {
		$cnt=0;
		$res = $this->db->query("select * from ".DB_PREFIX."membertree where member_id = '".$rt."' and right_sponsorcode <> '' ")->row;
		if(isset($res["right_sponsorcode"]) && !empty($res["right_sponsorcode"]))
		{
			$cnt++;
			$msrn1 = $this->db->query("select * from ".DB_PREFIX."membertree where member_id = '".$res["right_sponsorcode"]."'")->row;
			if(isset($msrn1['left_sponsorcode']) && !empty($msrn1['left_sponsorcode'] ))
			{
				$msrn2 = $this->db->query("select * from ".DB_PREFIX."membertree where member_id ='".$msrn1["left_sponsorcode"]."'")->row;
				$cnt++;
				$ids = $msrn2['member_id'];
				
				$cnt += $this->cntlft($ids) + $this->cntrgt($ids);
			}
			if(isset($msrn1['right_sponsorcode']) && !empty($msrn1['right_sponsorcode']))
			{
				$msrn3 = $this->db->query("select * from ".DB_PREFIX."membertree where member_id ='". $msrn1["right_sponsorcode"]."'")->row;
				$cnt++;
				$ids = $msrn3['member_id'];
				$cnt += $this->cntlft($ids) + $this->cntrgt($ids);
			}			
		}
		return $cnt;
 	}	
}
?>