<?php
/*
 * @ Dated: 14 June 2014
 * @ This file is used for manage the registration packages in system.
 */ 
class ControllerMlmFeedback extends Controller { 
	private $error = array();

	public function index() {            
		$this->load->language('mlm/feedback');
		$this->document->setTitle($this->language->get('heading_title'));		 
		$this->load->model('mlm/feedback');
		$this->getList();
	}
    /*
	public function insert() {
		$this->load->language('mlm/packages');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('mlm/packages');
		$package_image = '';		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
                    
                   if (isset($_FILES["package_image"]["name"]) && !empty($_FILES["package_image"]["name"])) {
                    $package_image = $_FILES["package_image"]["name"];
                    $tim = time();
                    if ($package_image != '') {
                        $package_image = $tim . $package_image;
                    } else {
                        $package_image = '';
                    }
                    $folder_path = DIR_IMAGE;                    
                    if ($package_image != "") {
                        @copy(@$_FILES["package_image"]["tmp_name"], $folder_path . $package_image);                        
                    }                  
                    
                }
                       $this->request->post["package_image"] = $package_image;
               
			$this->model_mlm_packages->addPackages($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = ''; 			
			$this->redirect($this->url->link('mlm/packages', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}*/
	public function update() {
            
		$this->load->language('mlm/feedback');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('mlm/feedback');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') ) { // && $this->validateForm()
			$this->model_mlm_feedback->editPackage($this->request->get['feedbackid'], $this->request->post);
		
                    $this->session->data['success'] = $this->language->get('text_success');
					$url = '';
					if (isset($this->request->get['sort'])) {
						$url .= '&sort=' . $this->request->get['sort'];
				    }

					if (isset($this->request->get['order'])) {
						$url .= '&order=' . $this->request->get['order'];
					}

					if (isset($this->request->get['page'])) {
						$url .= '&page=' . $this->request->get['page'];
					}
			
					$this->redirect($this->url->link('mlm/feedback', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
                    $this->getForm();
	} 


 ############## Show packages list  ##########################
	private function getList() {
				if (isset($this->request->get['sort'])) {
					$sort = $this->request->get['sort'];
				} 
		        else 
		        {
					$sort = 'package_adddate';
				}
				
				if (isset($this->request->get['order'])) {
					$order = $this->request->get['order'];
				} else {
					$order = 'ASC';
				}
				
				if (isset($this->request->get['page'])) {
					$page = $this->request->get['page'];
				} else {
					$page = 1;
				}
				
				$url = '';
					
				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}
		
				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}
				
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}
		
		  		$this->data['breadcrumbs'] = array();
		
		   		$this->data['breadcrumbs'][] = array(
		             'text'      => $this->language->get('text_home'),
					'href'      => $this->url->link('mlm/feedback', 'token=' . $this->session->data['token'], 'SSL'),
		      		'separator' => false
		   		);
		
		   		$this->data['breadcrumbs'][] = array(
		                        'text'      => $this->language->get('heading_title'),
					           'href'      => $this->url->link('mlm/feedback', 'token=' . $this->session->data['token'] . $url, 'SSL'),
		                        'separator' => ' :: '
		   		);
									
				$this->data['insert'] = $this->url->link('mlm/feedback/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
				$this->data['delete'] = $this->url->link('mlm/feedback/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		
				$this->data['informations'] = array();
		
				$data = array(
					'sort'  => $sort,
					'order' => $order,
					'start' => ($page - 1) * $this->config->get('config_admin_limit'),
					'limit' => $this->config->get('config_admin_limit')
				);
				
				$packages_total = $this->model_mlm_feedback->getTotalFeedback();
				$results = $this->model_mlm_feedback->getFeedback($data);
	    		foreach ($results as $result) {
					$action = array();							
					$action[] = array(
						'text' => $this->language->get('text_reply'),
						'href' => $this->url->link('mlm/feedback/update', 'token=' . $this->session->data['token'] . '&feedbackid=' . $result['feedback_id'] . $url, 'SSL')
					);
				    $action[] = array(
						'text' => $this->language->get('text_delete'),
						'href' => $this->url->link('mlm/feedback/delete', 'token=' . $this->session->data['token'] . '&feedbackid=' . $result['feedback_id'] . $url, 'SSL')
					);
                   
                       // $date= date('d/m/Y',strtotime($result['package_adddate']));
                        
    					$this->data['feedback'][]= array(
    				                'feedback_id'           => $result['feedback_id'],
                                    'name'                  => $result['name'],  
                                    'email'                 => $result['email'],
                                    'status'                 => $result['status'],
                                    'date_added'            => $result['date_added'],				
    			                 	'action'                => $action
    					);
    		          }	
				   // echo "<pre>";print_r(	$this->data['feedback']);die;
					$this->data['heading_title'] = $this->language->get('heading_title');
					$this->data['text_no_results'] = $this->language->get('text_no_results');
					$this->data['column_title'] = $this->language->get('column_title');
					$this->data['column_sort_order'] = $this->language->get('column_sort_order');
					$this->data['column_action'] = $this->language->get('column_action');		
					$this->data['button_insert'] = $this->language->get('button_insert');
					$this->data['button_delete'] = $this->language->get('button_delete'); 
			 		if (isset($this->error['warning'])) {
						$this->data['error_warning'] = $this->error['warning'];
					} else {
						$this->data['error_warning'] = '';
					}		
					if (isset($this->session->data['success'])) {
						$this->data['success'] = $this->session->data['success'];		
						unset($this->session->data['success']);
					} else {
						$this->data['success'] = '';
					}
			
					$url = '';
			
					if ($order == 'ASC') {
						$url .= '&order=DESC';
					} else {
						$url .= '&order=ASC';
					}
			
					if (isset($this->request->get['page'])) {
						$url .= '&page=' . $this->request->get['page'];
					}
		
					$this->data['sort_title'] = $this->url->link('mlm/feedback', 'token=' . $this->session->data['token'] . '&sort=id.package_id' . $url, 'SSL');
					$this->data['sort_sort_order'] = $this->url->link('mlm/feedback', 'token=' . $this->session->data['token'] . '&sort=i.package_id' . $url, 'SSL');
					
					$url = '';
			
					if (isset($this->request->get['sort'])) {
						$url .= '&sort=' . $this->request->get['sort'];
					}
															
					if (isset($this->request->get['order'])) {
						$url .= '&order=' . $this->request->get['order'];
					}
			
					$pagination = new Pagination();
					$pagination->total = $packages_total;
					$pagination->page = $page;
					$pagination->limit = $this->config->get('config_admin_limit');
					$pagination->text = $this->language->get('text_pagination');
					$pagination->url = $this->url->link('mlm/feedback', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
						
					$this->data['pagination'] = $pagination->render();
			
					$this->data['sort'] = $sort;
					$this->data['order'] = $order;
			
					$this->template = 'mlm/feedback_list.tpl';
					$this->children = array(
						'common/header',
						'common/footer'
					);
							
					$this->response->setOutput($this->render());
		}
################################## update packages form###########################
	private function getForm() {	   
       
       
     
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');		
		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_bottom'] = $this->language->get('entry_bottom');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');    	
		$this->data['tab_general'] = $this->language->get('tab_general');
        $this->data['tab_data'] = $this->language->get('tab_data');
	     
        $this->data['entry_pkgname'] = $this->language->get('entry_pkgname');
        
        $this->data['entry_pkg_dsc'] = $this->language->get('entry_pkg_dsc');
        
                   
                
        if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

        if (isset($this->error['error_package_description'])) {
			$this->data['error_package_description'] = $this->error['error_package_description'];
		} else {
			$this->data['error_package_description'] = '';
		} 
        
         
	 
		
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('mlm/feedback', 'token=' . $this->session->data['token'], 'SSL'),     		
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('mlm/feedback', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
							
		if (!isset($this->request->get['feedbackid'])) {
			$this->data['action'] = $this->url->link('mlm/feedback/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('mlm/feedback/update', 'token=' . $this->session->data['token'] . '&feedbackid=' . $this->request->get['feedbackid'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('mlm/feedback', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['feedbackid']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
                    
			$feedback_details = $this->model_mlm_feedback->getFeedbackById($this->request->get['feedbackid']);
		}       
		$this->data['token'] = $this->session->data['token'];
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
        
	   if (!empty($feedback_details)) {
			$this->data['feedback_name'] = $feedback_details['name'];
		} else {
			$this->data['feedback_name'] = '';
		}  
        
        
        
        if (!empty($feedback_details)) {
			$this->data['reply_dsc'] = $feedback_details['reply'];
		} else {
			$this->data['reply_dsc'] = '';
		}  
        
        
        if (!empty($feedback_details)) {
			$this->data['status'] = $feedback_details['status'];
		} else {
			$this->data['status'] = '';
		}                  
         

      
		 
		$this->load->model('design/layout');		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		 
		$this->template = 'mlm/feedback_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	private function validateForm() {
		if (!$this->user->hasPermission('modify', 'mlm/feedback')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
	   
         if ((utf8_strlen($this->request->post['reply_dsc']) < 1)) {
			$this->error['error_reply_dsc'] = $this->language->get('error_reply_dsc');
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
        //echo "<pre>";print_r($this->error);die;
			
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>