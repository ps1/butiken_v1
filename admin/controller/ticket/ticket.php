<?php 
class Controllerticketticket extends Controller { 
    public function index() 
         {
	 $this->language->load('ticket/ticket');
         $this->document->setTitle($this->language->get('request_heading'));
         $this->data['breadcrumbs'] = array();
         $this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
		      'href'      => $this->url->link('common/home'),
        	'separator' => false
        ); 
  	
        $this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_tickets'),
		'href'      => $this->url->link('affiliate/commission/submitrequest', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
      if (isset($this->session->data['success'])) {
    	  $this->data['success'] = $this->session->data['success'];
          unset($this->session->data['success']);
         
      } 
      else 
       {
        	$this->data['success'] = '';
	}
        $this->data['heading_title'] = $this->language->get('request_heading');    	
    	$this->data['users'] = array();    	
    	if(isset($this->request->get['id']))
    	{
            $id = $this->request->get['id'];
    	}
    	else{
         //   $id = $this->customer->getId();
    	}
      //  $root_customer_id = $this->customer->isLogged();                
       $message_lists1 = $this->db->query("select (SELECT concat_ws(' ',firstname,lastname) FROM ".DB_PREFIX."customer where customer_id = m.customer_id) as custname , m.* from ".DB_PREFIX."cust_messages as m")->rows;
       /*    
        $action = array();
        $action[] = array(
				'text' => $this->language->get('text_orders'),
				'href' => $this->url->link('customermlm/customer/viewcustomerorders', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, 'SSL')
			);*/
       
       
       //$this->data["message_lists"] = $message_lists ;
       
       
       foreach ($message_lists1 as $result) {
	 $action = array();
	 $supportmsgheader=$this->db->query("select distinct user_type,concat_ws(' ',firstname,lastname) as custname,msg_desc,msg_subject,msg_adddate,msg_status,".DB_PREFIX."cust_messages.customer_id,".DB_PREFIX."cust_messages.msg_id from  ".DB_PREFIX."cust_messages inner join 
        ".DB_PREFIX."customer on ".DB_PREFIX."cust_messages.customer_id=".DB_PREFIX."customer.customer_id 
         where ".DB_PREFIX."cust_messages.msg_id='".$result['msg_id']."' and msg_status=0")->rows;
         $this->data['supportmsgheader']=$supportmsgheader;

         $supportmsglists=$this->db->query("select * from  ".DB_PREFIX."cust_support 
         inner join ".DB_PREFIX."cust_messages on ".DB_PREFIX."cust_messages.msg_id=".DB_PREFIX."cust_support.msg_id where ".DB_PREFIX."cust_support.msg_id='".$result['msg_id']."' and readstatus='N' and identity!='A'")->rows;
        
         $usertype=$this->db->query("select user_type from  ".DB_PREFIX."customer where customer_id='".$result['customer_id']."'")->row;
        
         
        
        $unread=count(array_merge($supportmsgheader,$supportmsglists)); 
        
        $this->data['unread']=count(array_merge($supportmsgheader,$supportmsglists)); 
                     $action[] = array(
				    'text' => 'Edit',
			     	'href' => $this->url->link('ticket/ticket/newticket&token='.$this->session->data['token'].'&msg_id='.$result['msg_id'], '', 'SSL')
			);	
		          	$action[] = array(
				    'text' => 'REPLY',
                                'unread' =>$unread,
				'href' => $this->url->link('ticket/ticket/support&token='.$this->session->data['token'].'&msg_id='.$result['msg_id'], 'SSL')
			);
                      $this->data['message_lists'][] = array(
                				'msg_id'    => $result['msg_id'],
                				'custname'           => $result['custname'],
                                'msg_adddate'           => $result['msg_adddate'],
                                'msg_subject'     => $result['msg_subject'],
                                'msg_desc'     => $result['msg_desc'],
			                     'msg_status'          => $result['msg_status'],
                                'identity'  =>  $usertype['user_type'],
				                'new_ticket'         => $action,
                                'customer_id'         => $result['customer_id']
			);
		}
       
        $this->data['back'] = $this->url->link('ticket/ticket&token='.$this->session->data['token'], '', 'SSL');
       /* $this->data['new_ticket'] = $this->url->link('ticket/ticket/newticket&token='.$this->session->data['token'], '', 'SSL');*/
        $this->template = 'ticket/messages.tpl';
	
	$this->children = array(
			'common/footer',
			'common/header'
		);				
	$this->response->setOutput($this->render());	
	
    }
    
   public function newticket()
   {
      $this->language->load('ticket/ticket');
      //$this->load->model('ticket/ticket');
      $this->document->setTitle($this->language->get('new_request_heading'));
      $this->data['breadcrumbs'] = array();
      $this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
		'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 
  	
      $this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_new_tickets'),
		'href'      => $this->url->link('ticket/ticket', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
      if (isset($this->session->data['success'])) {
    	  $this->data['success'] = $this->session->data['success'];
          unset($this->session->data['success']);
         
      } else {
            
        	$this->data['success'] = '';
	}
		
    	$this->data['heading_title'] = $this->language->get('new_request_heading');  
        $this->data['back'] = $this->url->link('ticket/ticket&token='.$this->session->data['token'], '', 'SSL');
    	$this->data['users'] = array();    	
    	if(isset($this->request->get['id']))
    	{
            $id = $this->request->get['id'];
    	}
    	else
        {
           // $id = $this->customer->getId();
    	}
        if(isset($_POST['submit']))
        {
            $msg_id = trim($_REQUEST['msg_id']);                
            $subject=trim($_POST["msg_subject"]);
            $status=trim($_REQUEST["status"]);
            $message=trim($_POST["msg_desc"]);
       //echo  "update  mlm_cust_messages set msg_subject='".$subject."',msg_desc='".$message."',msg_status='".$status."' where msg_id='".$msg_id."'"; exit;
            $this->db->query("update  ".DB_PREFIX."cust_messages set msg_subject='".$subject."',msg_desc='".$message."',msg_status='".$status."' where msg_id='".$msg_id."'");
            $this->data["success"] = "Request has been send.";
        }
        $msg_id=$_REQUEST['msg_id'];
        $message_lists = $this->db->query("select (SELECT concat_ws(' ',firstname,lastname) FROM ".DB_PREFIX."customer where customer_id = m.customer_id) as custname , m.* from ".DB_PREFIX."cust_messages as m where msg_id='".$msg_id."'")->row;
        $this->data["message_lists"] = $message_lists;
        $this->template = 'ticket/new_messages.tpl';
	$this->children = array(
			'common/footer',
			'common/header'
		);				
	$this->response->setOutput($this->render());
   }
     public function support()
   {
      $this->language->load('ticket/ticket');
      //$this->load->model('ticket/ticket');
      $this->document->setTitle($this->language->get('new_request_heading'));
    
      $this->data['breadcrumbs'] = array();
      $this->data['breadcrumbs'][] = 
              array(
        	'text'      => $this->language->get('text_home'),
		    'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 
  	
         $this->data['breadcrumbs'][] = array(       	
        	   'text'      => $this->language->get('text_new_tickets'),
		       'href'      => $this->url->link('ticket/ticket', '', 'SSL'),
                'separator' => $this->language->get('text_separator')
      	);
      if (isset($this->session->data['success'])) {
    	  $this->data['success'] = $this->session->data['success'];
          unset($this->session->data['success']);
      } 
      else
       { 
          $this->data['success'] = '';
       }
		
    	$this->data['heading_title'] = $this->language->get('new_request_heading');    	
    	$this->data['users'] = array();    	
    	if(isset($this->request->get['id']))
    	{
            $id = $this->request->get['id'];
    	}
    	else{
          //  $id = $this->customer->getId();
    	}
        if(isset($_POST['submit']))
        {
            $msg_id=trim($_REQUEST["msg_id"]);
            $identity=A;
            $message=trim($_REQUEST["msg_desc"]);
            $msg_date=date('Y-m-d');
            $this->db->query("update ".DB_PREFIX."cust_support set readstatus='R' where msg_id='".$msg_id."'");
            $this->db->query("update ".DB_PREFIX."cust_messages set  msg_status='1' where msg_id='".$msg_id."'");
            $this->db->query("insert into ".DB_PREFIX."cust_support (message,msg_date,readstatus,msg_id,identity) values ('".$message."','".$msg_date."','N','".$msg_id."','A')"); 
            $this->data["success"] = "Request has been send.";
            //header("Location:index.php?route=ticket/ticket/support&msg_id=".$msg_id."&token=".$this->session->data['token']);
            header("Location:index.php?route=ticket/ticket&token=".$this->session->data['token']);
        }
        
         $msgid=$_REQUEST['msg_id'];
         
         $supportmsgheader=$this->db->query("select distinct user_type,concat_ws(' ',firstname,lastname) as custname,msg_desc,msg_subject,msg_adddate,msg_status,".DB_PREFIX."cust_messages.customer_id,".DB_PREFIX."cust_messages.msg_id from  ".DB_PREFIX."cust_messages inner join ".DB_PREFIX."customer on ".DB_PREFIX."cust_messages.customer_id=".DB_PREFIX."customer.customer_id where ".DB_PREFIX."cust_messages.msg_id='".$msgid."'")->rows;
         
         $this->data['supportmsgheader']=$supportmsgheader;
         $supportmsglists=$this->db->query("select * from  ".DB_PREFIX."cust_support inner join ".DB_PREFIX."cust_messages on ".DB_PREFIX."cust_messages.msg_id=".DB_PREFIX."cust_support.msg_id where ".DB_PREFIX."cust_support.msg_id='".$msgid."'")->rows;
         $this->data['supportmsglists']=$supportmsglists;
         $this->data['back'] = $this->url->link('ticket/ticket&token='.$this->session->data['token'], '', 'SSL');	
	       $this->template = 'ticket/support.tpl';
	       $this->children = array(
			'common/footer',
			'common/header'
		);				
	$this->response->setOutput($this->render());
   }
   
}
?>
