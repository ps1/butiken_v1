<?php
// HTTP
define('HTTP_SERVER', 'http://www.infinity-stores.co.uk/butiken/admin/');
define('HTTP_CATALOG', 'http://www.infinity-stores.co.uk/butiken/');
define('HTTP_IMAGE', 'http://www.infinity-stores.co.uk/butiken/image/');

// HTTPS
define('HTTPS_SERVER', 'http://www.infinity-stores.co.uk/butiken/admin/');
define('HTTPS_CATALOG', 'http://www.infinity-stores.co.uk/butiken/');

// DIR
define('DIR_APPLICATION', '/home2/sanjeevu/public_html/butiken/admin/');
define('DIR_SYSTEM', '/home2/sanjeevu/public_html/butiken/system/');
define('DIR_DATABASE', '/home2/sanjeevu/public_html/butiken/system/database/');
define('DIR_LANGUAGE', '/home2/sanjeevu/public_html/butiken/admin/language/');
define('DIR_TEMPLATE', '/home2/sanjeevu/public_html/butiken/admin/view/template/');
define('DIR_CONFIG', '/home2/sanjeevu/public_html/butiken/system/config/');
define('DIR_IMAGE', '/home2/sanjeevu/public_html/butiken/image/');
define('DIR_CACHE', '/home2/sanjeevu/public_html/butiken/system/cache/');
define('DIR_DOWNLOAD', '/home2/sanjeevu/public_html/butiken/download/');
define('DIR_LOGS', '/home2/sanjeevu/public_html/butiken/system/logs/');
define('DIR_CATALOG', '/home2/sanjeevu/public_html/butiken/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'sanjeevu_butiken');
define('DB_PASSWORD', 'pass@123');
define('DB_DATABASE', 'sanjeevu_butikennew');
define('DB_PREFIX', 'butiken_');
?>