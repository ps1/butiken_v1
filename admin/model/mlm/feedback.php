<?php
class ModelMlmFeedback extends Model
{
    ##############Function By Rachana on 12-6-14 to get Total number of pakages##########################
    
    public function getTotalFeedback() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM ". DB_PREFIX ."feedback");
		
		return $query->row['total'];
	}
    public function editPackage($feed_id, $data) {            


        $sql= "UPDATE ". DB_PREFIX ."feedback SET reply = '" .$data['reply_dsc']. "', status = '".$data['status']."'   WHERE feedback_id  = '" . (int)$feed_id . "'";
       
		$this->db->query($sql);
		$this->cache->delete('packages');
	   }
    ##############Function By Rachana on 12-6-14 to get Rows of pakages##########################		
	public function getFeedback($data = array()) {
		if ($data) {
           
			$sql = "SELECT * FROM ". DB_PREFIX ."feedback p";
		
            ##############Sorting order is default add date##########################
            $sort_data =' p.feedback_id';
            if(isset($sort_data))
            {
                $sql.=" ORDER BY ".$sort_data;
            
            }
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
		
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}		

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}	
	
			$query = $this->db->query($sql);
			
			return $query->rows;
		} 
        else 
        {
			//$package_data = $this->cache->get('packages.' . (int)$this->config->get('config_language_id'));
		
			if (!$package_data) {
				$query = $this->db->query("SELECT * FROM  ". DB_PREFIX ."feedback p  ORDER BY p.feedback_id ASC");
	
				$package_data = $query->rows;
			
				$this->cache->set('packages.' . (int)$this->config->get('config_language_id'), $package_data);
			}	
	
			return $package_data;			
		}
	}

	

	public function getFeedbackById($feed_id) {
		$query = $this->db->query("SELECT  *  FROM ". DB_PREFIX ."feedback WHERE feedback_id = '" . (int)$feed_id . "'");
		
		return $query->row;
	}
	
}
?>