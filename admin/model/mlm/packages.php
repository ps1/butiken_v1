<?php
class ModelMlmPackages extends Model
{
    ##############Function By Rachana on 12-6-14 to get Total number of pakages##########################
    
    public function getTotalPackages() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM ". DB_PREFIX ."mst_packages");
		
		return $query->row['total'];
	}
    ##############Function By Rachana on 12-6-14 to get Rows of pakages##########################		
	public function getPackages($data = array()) {
		if ($data) {
           
			$sql = "SELECT * FROM ". DB_PREFIX ."mst_packages p";
		
            ##############Sorting order is default add date##########################
            $sort_data =' p.package_adddate';
            if(isset($sort_data))
            {
                $sql.=" ORDER BY ".$sort_data;
            
            }
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
		
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}		

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}	
	
			$query = $this->db->query($sql);
			
			return $query->rows;
		} 
        else 
        {
			$package_data = $this->cache->get('packages.' . (int)$this->config->get('config_language_id'));
		
			if (!$package_data) {
				$query = $this->db->query("SELECT * FROM  ". DB_PREFIX ."mst_packages p  ORDER BY p.package_id ASC");
	
				$package_data = $query->rows;
			
				$this->cache->set('packages.' . (int)$this->config->get('config_language_id'), $package_data);
			}	
	
			return $package_data;			
		}
	}
	public function addPackages($data) {
           
	   $this->db->query("INSERT INTO ". DB_PREFIX ."mst_packages SET  package_img = '".$data["package_image"]."' ,  package_name = '" .$data['package_name']. "', package_annual_fees = '".$data['package_annual_cost']."' ,  package_monthly_fees = '" . ($data['package_month_cost'])."',   package_descr = '" .$data['package_description'] . "', package_adddate = '".date("Y-m-d H:i:s")."'");
              
	}
	
	public function editPackage($package_id, $data) {            


        $sql= "UPDATE ". DB_PREFIX ."mst_packages SET package_name = '" .$data['package_name']. "', package_annual_fees = '".$data['package_annual_cost']."' ,  package_monthly_fees = '" . ($data['package_month_cost'])."' ,  pv_value = '" . ($data['pv_value'])."',   package_descr = '" .$data['package_description'] . "'  WHERE package_id  = '" . (int)$package_id . "'";
       
		$this->db->query($sql);
		$this->cache->delete('packages');
	   }
        
        public function updatePackageSrc($package_id, $data)
        {
            $this->db->query("UPDATE ". DB_PREFIX ."mst_packages SET package_img = '".$data["package_image"]."'  WHERE package_id  = '" . (int)$package_id . "'");

        }

        public function deletePackage($packageid) {
		$this->db->query("DELETE FROM mlm_packages WHERE package_id  = '" . (int)$packageid . "'");
		$this->cache->delete('packages');
	}	

	public function getPackagedetails($package_id) {
		$query = $this->db->query("SELECT  *  FROM ". DB_PREFIX ."mst_packages WHERE package_id = '" . (int)$package_id . "'");
		
		return $query->row;
	}
	
}
?>