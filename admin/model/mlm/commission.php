<?php
class ModelMlmCommission extends Model {
	public function addCommission($data) {
		$dated = date('Y-m-d H:i:s');
      	$this->db->query("INSERT INTO " . DB_PREFIX . "mst_incentives SET incentive_level 	 = '" . $this->db->escape($data['level']) . "', incentive_val = '" . $this->db->escape($data['commission']) . "', dated = '$dated'");
		$this->cache->delete('commission');
	}
	
	public function editCommission($commission_id, $data) {
      	$this->db->query("UPDATE " . DB_PREFIX . "mst_incentives SET incentive_level = '" . $this->db->escape($data['level']) . "', incentive_val = '" . $this->db->escape($data['commission']) . "' WHERE incentive_id = '" . (int)$commission_id . "'");
		$this->cache->delete('commission');
	}
	
	public function deleteCommission($commission_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "mst_incentives WHERE incentive_id = '" . (int)$commission_id . "'");			
		$this->cache->delete('commission');
	}	
	
	public function getCommission($commission_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "mst_incentives WHERE incentive_id = '" . (int)$commission_id . "'");
		return $query->row;
	}
	
	public function getCommissions($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "mst_incentives";
		
		$sort_data = array(
			'incentive_level , incentive_id	',
			'incentive_val'
		);	
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY incentive_level";
		}
		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}				
		
		$query = $this->db->query($sql);
	
		return $query->rows;
	}
	
	public function getTotalCommissions() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "mst_incentives");
		return $query->row['total'];
	}
	
	public function levelCommissionAlreadyExists($level, $commission_id) {
		$addQuery = '';
		if($commission_id > 0) {
			$addQuery = " AND incentive_id != '$commission_id'";
		}
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "mst_incentives WHERE incentive_level 	='$level' $addQuery LIMIT 0,1");
        
        
        echo "SELECT * FROM " . DB_PREFIX . "mst_incentives WHERE incentive_level 	='$level' $addQuery LIMIT 0,1";
        
       // echo $query;die;
        
		if( isset($query->row) && !empty($query->row) ) {
			return true;
		}
		
		return false;
	}
}
?>
