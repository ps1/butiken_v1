<?php
/*
 * @Dated: 14 June 2014 Saturday
 * This file has mlm packages language variable text user can edit the display text from this file.
 */

// Heading
$_['heading_title']     = 'Membership Packages';

// Text
$_['text_success']      = 'Success: You have modified package detail!';
$_['text_default']      = 'Default';

$_['text_delete']      = 'Delete';


// Column
$_['column_title']      = 'Membership Package';
$_['column_sort_order']	= 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_title']       = 'Information Title:';
$_['entry_description'] = 'Description:';
$_['entry_store']       = 'Stores:';
$_['entry_keyword']     = 'SEO Keyword:<br /><span class="help">Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.</span>';
$_['entry_bottom']      = 'Bottom:<br/><span class="help">Display in the bottom footer.</span>';
$_['entry_status']      = 'Status:';
$_['entry_sort_order']  = 'Sort Order:';
$_['entry_layout']      = 'Layout Override:';
$_['entry_pkgname'] ='Package name';
$_['entry_pkg_month_cost']      ='Package Monthly cost';
$_['entry_pkg_annual_cost']     ='Package Annual cost';
$_['entry_pkg_dsc']             ='Package description';
$_['entry_pkg_image']           ='Package image';
$_['entry_pv_value']            ='PV Value';
// Error 
$_['error_warning']     = 'Warning: Please check the form carefully for errors!';
$_['error_permission']  = 'Warning: You do not have permission to modify information!';

$_['error_package_name']                = 'Please enter package name!';
$_['error_package_month_cost']          = 'Please enter monthly cost!';
$_['error_package_annual_cost']         = 'Please enter annual name!';
$_['error_package_description']         = 'Please enter description!';




$_['error_description'] = 'Description must be more than 3 characters!';
$_['error_account']     = 'Warning: This information page cannot be deleted as it is currently assigned as the store account terms!';
$_['error_checkout']    = 'Warning: This information page cannot be deleted as it is currently assigned as the store checkout terms!';
$_['error_affiliate']   = 'Warning: This information page cannot be deleted as it is currently assigned as the store affiliate terms!';
$_['error_store']       = 'Warning: This information page cannot be deleted as its currently used by %s stores!';
?>