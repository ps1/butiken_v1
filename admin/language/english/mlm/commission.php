<?php
// Heading
$_['heading_title']      = 'Bonus Levels';

// Text
$_['text_success']       = 'Success: You have modified commissions!';

// Column
$_['column_level']       = 'Level';
$_['column_commission']  = 'Bonus';
$_['column_action']      = 'Action';

// Entry
$_['entry_level']        = 'Level:';
$_['entry_commission']   = 'Bonus:';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify bonus!';
$_['error_level']        = 'Level must be greater than 0!';
$_['error_commission']   = 'Bonus can cot be empty!';
$_['error_level_commission_exists']     = 'Duplicate entry for this level!';
?>
