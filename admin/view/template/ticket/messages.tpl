<?php echo $header; ?>
<?php if ($success) { ?>
	<div class="success"><?php echo $success; ?></div>
<?php } ?>
<div id="content">
    <div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
    </div>
    <?php // if(isset($column_right)) echo $column_right; ?>
    <h1><?php echo $heading_title; ?></h1>
   <!-- <div style="float:right;"><a href="<?php echo $new_ticket; ?>"> New Ticket</a></div>-->
    <table cellpadding="5" cellspacing="5" border="1" class="list" style="width:100% !important;">
        <thead>
        <tr>
            <td align="left">Serial No.</td>
             <td align="left">Ticket No.</td>
            <td align="left">Customer name</td>
            <td align="left">Message Subject</td>
            <td align="left">Message Date</td>
            <td align="left">Status</td>
            <td align="left">Edit</td>
            <td align="left">Response</td>
        </tr>
        </thead>     
        <tbody>
    <?php  
    if(isset($message_lists) && sizeof($message_lists) > 0 )
    {
    $count=1;
    foreach($message_lists as $msg)
    {   
        $status = 'Pending';
        if(isset($msg["msg_status"]) && $msg["msg_status"] == '1')
        {
           $status = 'Solved';
        }
       $zero=0; 
       $totalzero=intval(2)-strlen($msg['msg_id']);
        for($i=0;$i<$totalzero;$i++)
       {
         $zero=$zero.'0';   
       }
        $ticketno=$zero.''.$msg['msg_id']; 
       if($msg['identity']=='C')
       {
       $user="&nbsp;(Customer)";
       }
       else
       {
       $user="&nbsp;(Member)";
       }
        echo "<tr>";
        echo "<td>".$count."</td>";
        echo "<td>".$ticketno."</td>";
        echo "<td>".$msg["custname"]." ".$user."</td>";
        echo "<td>".$msg["msg_subject"]."</td>";
        echo "<td>".date("m/d/Y",strtotime($msg["msg_adddate"]))."</td>";
        echo "<td>".$status."</td>";
        //echo "<pre>";
        //print_r($msg['new_ticket']);        
                        
         foreach ($msg['new_ticket'] as $new_ticket) { 
         $action=$new_ticket['href'];
         if(isset($new_ticket['unread']))
         {                  
         $newmsg=$new_ticket['unread'];
         }         
         if(isset($newmsg) && $newmsg>0)
         {
         $newmsg1="&nbsp;&nbsp;(".$newmsg.")";
         }
         else
         {
          $newmsg1="";
         }
         ?>
       <td><a href="<?php echo $action;?>"><?php echo $new_ticket['text'];?><span style="color:red;font-weight:bold;"><?php echo $newmsg1;?></span></a></td>
        <?php
        }
        echo "</tr>";    
        $count = $count + 1;
     }
    }else{
        echo "<tr><td colspan='5'>No message found!!</td></tr>";
    }    
    ?>      
        </tbody>
    </table>
</div>

<?php echo $footer; ?>