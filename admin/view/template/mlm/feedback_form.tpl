<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/information.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">      
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
       
    
        <div id="tab-data">
          <table class="form">
            <tr>
              <td><?php echo $entry_pkgname?></td>
              <td>
                <span><?php echo $feedback_name;?></span>
              </td>
            </tr>
            
           
            <tr>
              <td><?php echo $entry_pkg_dsc;?></td>
              <td><textarea name="reply_dsc"  id="reply_dsc"  cols="40" rows="5"><?php echo isset($reply_dsc) ? $reply_dsc : ''; ?></textarea><span class="error"><?php if($error_package_description) {echo $error_package_description;}?></span></td>
            </tr> 
            
             <tr>
              <td><?php echo $entry_status;?></td>
              <td>
                <select name="status">
                    <option value="0" <?php if($status==0) echo "selected";?>>Disable</option>
                     <option value="1" <?php if($status==1) echo "selected";?> >Enable</option>
                </select>
              
              </td>
            </tr>             
      
          </table>
        </div>         
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>