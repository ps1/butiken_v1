<?php echo $header; ?>

<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>

	<?php if ($success) { ?>
		<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	
	<div class="box">
		<div class="heading"><h1><img src="view/image/customer.png" alt="" /> <?php echo $heading_title; ?></h1></div>
		
		<div class="content">
			<form action="" method="post" enctype="multipart/form-data" id="form">
				<table class="list">
					<thead>
						<tr>
                            <td>ID#</td>     
							<td class="left">
								<?php if ($sort == 'name') { ?>
									<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
								<?php } else { ?>
									<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
								<?php } ?>
							</td>
							<td>Member ID</td>
                            <td>Phone</td>
                            <td>Status</td>
                            <td>PV Value</td>
							<td class="left">
								<?php if ($sort == 'c.email') { ?>
									<a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
								<?php } else { ?>
									<a href="<?php echo $sort_email; ?>"><?php echo $column_email; ?></a>
								<?php } ?>
							</td>
							
							<td class="left">
								<?php if ($sort == 'c.date_added') { ?>
									<a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
								<?php } else { ?>
									<a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
								<?php } ?>
							</td>
                            <td class="left">
							 Package Name
							</td>
                            
							<td class="left">Sponsor code</td>
							
							<td class="right"><?php echo $column_action; ?></td>
						</tr>
					</thead>
					
					<tbody>
<!--						<tr class="filter">
							<td><input type="text" name="filter_name" value="<?php echo $filter_name; ?>" /></td>
							<td><input type="text" name="filter_email" value="<?php echo $filter_email; ?>" /></td>
							<td><input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" size="12" id="date" /></td>
						<td>&nbsp;</td>
							<td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
						</tr>-->

						<?php if ($customers) { ?>
							<?php foreach ($customers as $customer) { ?>
								<tr>
                                    <td class="left"><?php echo $customer["customer_id"]; ?></td> 
									<td class="left"><?php echo $customer['name']; ?></td>
                                    <td class="left"><?php echo $customer['member_id']; ?></td>
                                    <td class="left"><?php echo $customer['phone']; ?></td>
                                    <td class="left"><?php echo $customer['status']; ?></td>
                                    <td class="left"><?php echo $customer['member_id']; ?></td>
									<td class="left"><?php echo $customer['email']; ?></td>
									<td class="left"><?php echo $customer['date_added']; ?></td>
                                    <td class="left"><?php echo $customer['package_name']; ?></td>
									<td class="left"><?php echo $customer['sponsor']; ?></td>
									<td class="right">
                                    <?php //echo "<pre>";print_r($customer['action']);?>
										<?php foreach ($customer['action'] as $action) { ?>
											[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
							<tr><td class="center" colspan="5"><?php echo $text_no_results; ?></td></tr>
						<?php } ?>
					</tbody>
				</table>
			</form>
			
			<div class="pagination"><?php echo $pagination; ?></div>
		</div>
	</div>
</div>

<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=mlm/customer&token=<?php echo $token; ?>';
	
	var filter_name = $('input[name=\'filter_name\']').attr('value');
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_email = $('input[name=\'filter_email\']').attr('value');
	if (filter_email) {
		url += '&filter_email=' + encodeURIComponent(filter_email);
	}

	var filter_date_added = $('input[name=\'filter_date_added\']').attr('value');
	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}

	location = url;
}
//--></script>

<script type="text/javascript"><!--
$(document).ready(function() {
	$('#date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script>
<?php echo $footer; ?>  
