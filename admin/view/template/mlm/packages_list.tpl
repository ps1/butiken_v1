<?php echo $header;  ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/information.png" alt="" /> <?php echo $heading_title; ?></h1>
        <!--<div class="buttons"><a onclick="location = '<?php echo $insert; ?>'" class="button">
              <?php echo $button_insert; ?></a>-->
          <!--<a onclick="$('form').submit();" class="button"><?php //echo $button_delete; ?></a>-->
      <!--</div>-->
      
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
                <td class="left">Package ID#</td> 
                <td class="left">Package name</td>
                <td class="left">Monthly Fee</td>
                <td class="left">Annual Fee</td>
                 <td class="left">PV Value</td>
                <td class="left">Added Date</td>                
                <td class="left">Action</td>
            </tr>
          </thead>
          <tbody>
            <?php if (isset($packages)) { 
              
            ?>
            <?php foreach ($packages as $package) { ?>
            <tr>             
              <td class="left"><?php echo $package['package_id']; ?></td>
              <td class="left"><?php echo $package['package_name']; ?></td>
              <td class="left"><?php echo $package['package_monthly']; ?></td>         
              <td class="left"><?php echo $package['package_annual']; ?></td>
              <td class="left"><?php echo $package['pv_value']; ?></td>
              <td class="left"><?php echo $package['package_adddate']; ?></td>              
              <td class="left"><a href="<?php echo $package['action'][0]['href']; ?>"><?php echo $package['action'][0]['text']; ?></a></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<?php echo $footer; ?>