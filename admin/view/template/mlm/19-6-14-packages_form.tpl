<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/information.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">      
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
       
    
        <div id="tab-data">
          <table class="form">
            <tr>
              <td><?php echo $entry_pkgname?></td>
              <td><input type="text" name="package_name" size="100"   id="package_name" value="<?php echo $package_name; ?>" />
                <span class="error"><?php if($error_package_name) {echo $error_package_name;}?></span>
              </td>
            </tr>
            
            <tr>
              <td><?php echo $entry_pkg_month_cost?></td>
              <td><input type="text" name="package_month_cost" size="10" id="package_month_cost" value="<?php echo $package_monthly_fees; ?>" /><span class="error"><?php if($error_package_month_cost) {echo $error_package_month_cost;}?></span></td>
            </tr>
            
             <tr>
              <td><?php echo $entry_pkg_annual_cost; ?></td>
              <td><input type="text" name="package_annual_cost" size="10" id="package_annual_cost" value="<?php echo $package_annual_fees; ?>" /><span class="error"><?php if($error_package_annual_cost) {echo $error_package_annual_cost;}?></span></td>
            </tr>
            
            
            <tr>
              <td><?php echo $entry_pkg_dsc;?></td>
              <td><textarea name="package_description"  id="package_description"  cols="40" rows="5"><?php echo isset($package_description) ? $package_description : ''; ?></textarea><span class="error"><?php if($error_package_description) {echo $error_package_description;}?></span></td>
            </tr> 
            
            
            
             <tr>
              <td><?php echo $entry_pkg_image;?></td>
              <td><input type="file" name="package_image"  size="40"   id="package_image"  />
              <?php  
                if(isset($thumb) && !empty($thumb))
                {?>
                 <img src="<?php echo $thumb?>"   border="0" style="width:100px;height:50px;" />
                <?php                
                }?>
                  
                 
              </td>
            </tr>
            
      
          </table>
        </div>         
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>