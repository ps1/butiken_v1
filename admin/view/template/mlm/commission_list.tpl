<?php echo $header; ?>
<div id="content">

	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>

	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>

	<?php if ($success) { ?>
		<div class="success"><?php echo $success; ?></div>
	<?php } ?>

	<div class="box">
		<!--<div class="heading">
			<h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
			<div class="buttons"><a onclick="location = '<?php echo $insert; ?>'" class="button"><?php echo $button_insert; ?></a><a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a></div>
		</div>-->

		<div class="content">
			<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="list">
					<thead>
						<tr>
							<!--<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>-->
							<td class="left">
								<?php if ($sort == 'level') { ?>
									<a href="<?php echo $sort_level; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_level; ?></a>
								<?php } else { ?>
									<a href="<?php echo $sort_level; ?>"><?php echo $column_level; ?></a>
								<?php } ?>
							</td>
                            
                            <td class="left">
								<?php if ($sort == 'level') { ?>
									<a href="<?php echo $sort_level; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Package Name'; ?></a>
								<?php } else { ?>
									<a href="<?php echo $sort_level; ?>"><?php echo 'Package Name'; ?></a>
								<?php } ?>
							</td>
						
							<td class="left">
								<?php if ($sort == 'commission') { ?>
									<a href="<?php echo $sort_commission; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_commission; ?></a>
								<?php } else { ?>
									<a href="<?php echo $sort_commission; ?>"><?php echo $column_commission; ?></a>
								<?php } ?>
							</td>
						
							<td class="right"><?php echo $column_action; ?></td>
						</tr>
					</thead>
			
					<tbody>
						<?php if ($commissions) { ?>
							<?php foreach ($commissions as $commission) { ?>
								<tr>
									<!--<td style="text-align: center;">-->
										<!--<?php if ($commission['selected']) { ?>
											<input type="checkbox" name="selected[]" value="<?php echo $commission['commission_id']; ?>" checked="checked" />
										<?php } else { ?>
											<input type="checkbox" name="selected[]" value="<?php echo $commission['commission_id']; ?>" />
										<?php } ?>-->
									<!--</td>-->
									<td class="left"><?php echo "Level ".$commission['level']; ?></td>
                                    <td class="left"><?php echo $commission['packname']; ?></td>
									<td class="left"><?php echo $commission['commission']; ?></td>
									<td class="right">
										<?php foreach ($commission['action'] as $action) { ?>
											[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
							<tr><td class="center" colspan="4"><?php echo $text_no_results; ?></td></tr>
						<?php } ?>
					</tbody>
				</table>
			</form>

			<div class="pagination"><?php echo $pagination; ?></div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
