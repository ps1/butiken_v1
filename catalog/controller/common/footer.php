<?php  

class ControllerCommonFooter extends Controller {

	protected function index() {

		$this->language->load('common/footer');



		$this->data['text_information'] = $this->language->get('text_information');

		$this->data['text_service'] = $this->language->get('text_service');

		$this->data['text_extra'] = $this->language->get('text_extra');

		$this->data['text_contact'] = $this->language->get('text_contact');

		$this->data['text_return'] = $this->language->get('text_return');

		$this->data['text_sitemap'] = $this->language->get('text_sitemap');
                
       	$this->data['text_training'] = $this->language->get('text_training'); 
        
        $this->data['text_standard_support'] = $this->language->get('text_standard_support');
        
        $this->data['text_online_training'] = $this->language->get('text_online_training');
        
        $this->data['text_custom_training'] = $this->language->get('text_custom_training');
         
		$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');

		$this->data['text_voucher'] = $this->language->get('text_voucher');

		$this->data['text_affiliate'] = $this->language->get('text_affiliate');
                
        $this->data['text_address'] = $this->language->get('text_address');

		$this->data['text_special'] = $this->language->get('text_special');

		$this->data['text_account'] = $this->language->get('text_account');
 
        $this->data['text_performancemetrix'] = $this->language->get('text_performancemetrix');
                
		$this->data['text_order'] = $this->language->get('text_order');

		$this->data['text_wishlist'] = $this->language->get('text_wishlist');

		$this->data['text_newsletter'] = $this->language->get('text_newsletter');
                
        $this->data['text_joinasmember'] = $this->language->get('text_joinasmember');
        
        $this->data['text_signmember'] = $this->language->get('text_signmember');
       
        $this->data['text_webservices'] = $this->language->get('text_webservices');
        
        $this->data['text_performancemetrix'] = $this->language->get('text_performancemetrix');
        
       
        $this->load->model('catalog/information');



		$this->data['informations'] = array();



		foreach ($this->model_catalog_information->getInformations() as $result) 
                {

			if ($result['bottom']) {

				$this->data['informations'][] = array(

					'title' => $result['title'],

					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])

				);

			}

		}
        $this->data['contact'] = $this->url->link('information/contact');

		$this->data['return'] = $this->url->link('account/return/insert', '', 'SSL');

		$this->data['sitemap'] = $this->url->link('information/information&amp;information_id=7');
                
         $this->data['training'] = $this->url->link('information/information&amp;information_id=9', '', 'SSL');

		$this->data['standard_support'] = $this->url->link('information/information&amp;information_id=8');
                
         $this->data['online_training'] = $this->url->link('information/information&amp;information_id=10', '', 'SSL');

		$this->data['custom_training'] = $this->url->link('information/information&amp;information_id=11');
                
		$this->data['manufacturer'] = $this->url->link('product/manufacturer');

		$this->data['voucher'] = $this->url->link('account/voucher', '', 'SSL');

		$this->data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');

		$this->data['special'] = $this->url->link('product/special');

		$this->data['account'] = $this->url->link('account/account', '', 'SSL');

		$this->data['order'] = $this->url->link('account/order', '', 'SSL');

		$this->data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');

		$this->data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');	
               
        $this->data['joinasmember'] = $this->url->link('mlm/register_member', '', 'SSL');
        
        $this->data['signinsmember'] = $this->url->link('mlm/login', '', 'SSL');	
        
        /*
        $this->data['performancemetrix'] = $this->url->link('#', '', 'SSL');
        
        $this->data['webservices'] = $this->url->link('#', '', 'SSL');
        
        $this->data['training'] = $this->url->link('#', '', 'SSL');
        
        $this->data['standard_support'] = $this->url->link('#', '', 'SSL');
        
        $this->data['online_training'] = $this->url->link('#', '', 'SSL');
        
        $this->data['custom_training'] = $this->url->link('#', '', 'SSL');*/
        
        $this->data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online

		if ($this->config->get('config_customer_online')) {

			$this->load->model('tool/online');



			if (isset($this->request->server['REMOTE_ADDR'])) {

				$ip = $this->request->server['REMOTE_ADDR'];	

			} else {

				$ip = ''; 

			}



			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {

				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];	

			} else {

				$url = '';

			}



			if (isset($this->request->server['HTTP_REFERER'])) {

				$referer = $this->request->server['HTTP_REFERER'];	

			} else {

				$referer = '';

			}



			$this->model_tool_online->whosonline($ip, $this->customer->getId(), $url, $referer);

		}		



		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {

			$this->template = $this->config->get('config_template') . '/template/common/footer.tpl';

		} else {

			$this->template = 'default/template/common/footer.tpl';

		}



		$this->render();

	}

}

?>