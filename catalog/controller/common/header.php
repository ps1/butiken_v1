<?php   

class ControllerCommonHeader extends Controller {

	protected function index() {

		$this->data['title'] = $this->document->getTitle();



		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {

			$server = $this->config->get('config_ssl');

		} else {

			$server = $this->config->get('config_url');

		}



		if (isset($this->session->data['error']) && !empty($this->session->data['error'])) 

                {

		 $this->data['error'] = $this->session->data['error'];

                 unset($this->session->data['error']);

		}

                else 

                {

		 $this->data['error'] = '';

		}



		$this->data['base'] = $server;

		$this->data['description'] = $this->document->getDescription();

		$this->data['keywords'] = $this->document->getKeywords();

		$this->data['links'] = $this->document->getLinks();	 

		$this->data['styles'] = $this->document->getStyles();

		$this->data['scripts'] = $this->document->getScripts();

		$this->data['lang'] = $this->language->get('code');

		$this->data['direction'] = $this->language->get('direction');

		$this->data['google_analytics'] = html_entity_decode($this->config->get('config_google_analytics'), ENT_QUOTES, 'UTF-8');

		$this->data['name'] = $this->config->get('config_name');



		if ($this->config->get('config_icon') && file_exists(DIR_IMAGE . $this->config->get('config_icon'))) 

                {

			$this->data['icon'] = $server . 'image/' . $this->config->get('config_icon');

		} 

                else 

               {

			$this->data['icon'] = '';

		}



		if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo')))

                {

			$this->data['logo'] = $server . 'image/' . $this->config->get('config_logo');

		} else {

			$this->data['logo'] = '';

		}		



		$this->language->load('common/header');



		$this->data['text_home'] = $this->language->get('text_home');

		$this->data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));

		$this->data['text_shopping_cart'] = $this->language->get('text_shopping_cart');

		$this->data['text_search'] = $this->language->get('text_search');

		$this->data['text_welcome'] = sprintf($this->language->get('text_welcome'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));

		$this->data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'));



        $this->data['text_register'] = sprintf($this->language->get('text_register'), $this->url->link('account/register', '', 'SSL'));
       $this->load->model('account/customer');
       
       
       
       ########################Code added for member logout and account#############################
       
       $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());	
      
       if(($customer_info))
       {
               if($customer_info['member_id']!="")
               {
                 $this->data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('mlm/dashboard', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('mlm/logout', '', 'SSL'));
               }
               else
               {	
        
                $this->data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));
                
                }
        }

		$this->data['text_account'] = $this->language->get('text_account');

		$this->data['text_checkout'] = $this->language->get('text_checkout');



		$this->data['home'] = $this->url->link('common/home');

		$this->data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');

		$this->data['logged'] = $this->customer->isLogged();

		$this->data['account'] = $this->url->link('account/account', '', 'SSL');

		$this->data['shopping_cart'] = $this->url->link('checkout/cart');

		$this->data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');



		// Daniel's robot detector

		$status = true;



		if (isset($this->request->server['HTTP_USER_AGENT'])) {

			$robots = explode("\n", trim($this->config->get('config_robots')));



			foreach ($robots as $robot) {

				if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {

					$status = false;



					break;

				}

			}

		}



		// A dirty hack to try to set a cookie for the multi-store feature

		$this->load->model('setting/store');



		$this->data['stores'] = array();



		if ($this->config->get('config_shared') && $status) {

			$this->data['stores'][] = $server . 'catalog/view/javascript/crossdomain.php?session_id=' . $this->session->getId();



			$stores = $this->model_setting_store->getStores();



			foreach ($stores as $store) {

				$this->data['stores'][] = $store['url'] . 'catalog/view/javascript/crossdomain.php?session_id=' . $this->session->getId();

			}

		}



		// Search		

		if (isset($this->request->get['search'])) {

			$this->data['search'] = $this->request->get['search'];

		} else {

			$this->data['search'] = '';

		}



		// Menu

		$this->load->model('catalog/category');



		$this->load->model('catalog/product');



		$this->data['categories'] = array();



		$categories = $this->model_catalog_category->getCategories(0);

//added by virendra
                $module_data = array();
                $module_data1 = array();


		$this->load->model('setting/extension');



		$extensions = $this->model_setting_extension->getExtensions('module');		
             // echo "<pre>";print_r($extensions);die;
                //banner
                foreach ($extensions as $extension) 
                {
                   $modules = $this->config->get('welcome_module');
                    $modules1 = $this->config->get('slideshow_module');
                   
                    if ($modules) {
                           foreach ($modules as $module) 
                                {
                                if ($module['layout_id'] == 12 && $module['position'] == 'content_top' && $module['status']) {

						$module_data[] = array(

							'code'       => 'welcome',

							'setting'    => $module,

							'sort_order' => $module['sort_order']

						);				

					}

				}

			}
                          if ($modules1) {
                           foreach ($modules1 as $module1) 
                                {
                                if ($module1['layout_id'] == 12 && $module1['position'] == 'content_top' && $module1['status'])
                                    {

						$module_data1[] = array(

							'code'       => 'slideshow',

							'setting'    => $module1,

							'sort_order' => $module1['sort_order']

						);				

					}

				}

			}

		}



		$sort_order = array(); 
        //echo "<pre>";print_r($module_data);


		foreach ($module_data as $key => $value) 
                {
                $sort_order[$key] = $value['sort_order'];
                }



		array_multisort($sort_order, SORT_ASC, $module_data);
                $this->data['modules'] = array();
               $this->data['welcome']=array();
               $this->data['banners']=array();
               
                foreach ($module_data as $module) 
                {

			$module = $this->getChild('module/' . $module['code'], $module['setting']);



			if ($module) 
            {
            	 if(!isset($_REQUEST["route"]) || empty($_REQUEST["route"]) || $_REQUEST["route"] == 'common/home' )
				 {
    				$this->data['welcome'][] = $module;
			
				 }
			}
		}
		 
            foreach ($module_data1 as $module1) 
            {
				$module1 = $this->getChild('module/' . $module1['code'], $module1['setting']);
				if ($module1) 
                {                	
					$this->data['banners'][] = $module1;
				}
			}
           //end 

		foreach ($categories as $category) {

			if ($category['top']) {

				// Level 2

				$children_data = array();



				$children = $this->model_catalog_category->getCategories($category['category_id']);



				foreach ($children as $child) {

					$data = array(

						'filter_category_id'  => $child['category_id'],

						'filter_sub_category' => true

					);



					$product_total = $this->model_catalog_product->getTotalProducts($data);



					$children_data[] = array(

						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),

						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])

					);						

				}



				// Level 1

				$this->data['categories'][] = array(

					'name'     => $category['name'],

					'children' => $children_data,

					'column'   => $category['column'] ? $category['column'] : 1,

					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])

				);

			}

		}
        // echo "hello";
        //die;
 
		$this->children = array(

			'module/language',


			'module/cart',
  	         'module/currency'

		);

       

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {

			$this->template = $this->config->get('config_template') . '/template/common/header.tpl';

		} else {

			$this->template = 'default/template/common/header.tpl';

		}        

		$this->render();

	} 	

}

?>

