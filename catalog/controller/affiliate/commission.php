<?php 
class ControllerAffiliateCommission extends Controller { 
	public function index() {
		if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('affiliate/commission', '', 'SSL');
	  		$this->redirect($this->url->link('mlm/login', '', 'SSL'));
                } 
	
		$this->language->load('affiliate/commission');
		
		$this->load->model('account/customer');
		$this->load->model('affiliate/affiliate');

		$this->document->setTitle($this->language->get('heading_title'));

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
		'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 
        
      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_commission'),
		'href'      => $this->url->link('affiliate/commission', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
		
		if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
			
			unset($this->session->data['success']);
                        
		} else {
                    
			$this->data['success'] = '';
		}
		
    	$this->data['heading_title'] = $this->language->get('heading_title');
    	
    	$this->data['users'] = array();
    	
    	if(isset($this->request->get['id']))
    	{
    		$id = $this->request->get['id'];
    	}
    	else{
    		$id = $this->customer->getId();
    	}
        
         $root_customer_id = $this->customer->isLogged();                
        // $root_customer_id =  1; 
         $rootUserName = '';         
         
        $customerlists = $this->Getfirstlevel($root_customer_id);
        
        $rootorderlists = array(); 
        $rootorderlists = $this->db->query("select * from " . DB_PREFIX . "order where customer_id = '".$root_customer_id."'")->rows;
        $this->data["rootorders"] = $rootorderlists;
        
        $Firstlevelorders = array();
        if(isset($customerlists) && !empty($customerlists))
        {        
            $Firstlevelorders = $this->db->query("select * from " . DB_PREFIX . "order where customer_id IN (".$customerlists.")")->rows;
        }
        $this->data["firstlevelorders"] = $Firstlevelorders;
        
        $Secondlevelorders = array();        
        if(isset($customerlists) && !empty($customerlists))
        {
            $secondLevelcustomerlists = $this->GetSecondlevel($customerlists);                
            if(isset($secondLevelcustomerlists) && !empty($secondLevelcustomerlists))
            {
                $Secondlevelorders = $this->db->query("select * from " . DB_PREFIX . "order where customer_id IN (".$secondLevelcustomerlists.")")->rows;
            }
        }
        $this->data["secondlevelorders"] = $Secondlevelorders;
       
        $ThirdLevelOrders = array();
        if(isset($secondLevelcustomerlists) && !empty($secondLevelcustomerlists))
        {
            $thirdLevelCustomerLevel = $this->GetThirdlevel($secondLevelcustomerlists);       
            if(isset($thirdLevelCustomerLevel) && !empty($thirdLevelCustomerLevel))
            {
                $ThirdLevelOrders = $this->db->query("select * from " . DB_PREFIX . "order where customer_id IN (".$thirdLevelCustomerLevel.")")->rows;
            }    
        }
        $this->data["thirdlevelorders"] = $ThirdLevelOrders;
       /* 
        echo "<pre>";
        print_r($Firstlevelorders);
        echo "<hr />";
        print_r($Secondlevelorders);
        echo "<hr />";
        print_r($ThirdLevelOrders);
        exit(); 
       */ 
        $this->data["root_per"] = $this->config->get('config_root_commission');
        $this->data["level_per"] = $this->config->get('config_level_commission');
        
        
        $this->data['back'] = $this->url->link('mlm/dashboard', '', 'SSL');
        	
	if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/affiliate/commission.tpl')) { 
		$this->template = $this->config->get('config_template') . '/template/affiliate/commission.tpl';
	} else {
		$this->template = 'default/template/affiliate/commission.tpl';
	}
	$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);
				
	$this->response->setOutput($this->render());
    }
    public function Getfirstlevel($rootid)
    {
        $customerqu = '';
       $customertreelist= $this->db->query("select customer_id from " . DB_PREFIX . "customertree where sponsor_customerid = '".$rootid."'")->rows;
       if(isset($customertreelist[0]["customer_id"]))
       {
           foreach($customertreelist as $cust)
           {
               $customerqu .= $cust["customer_id"].",";
           }
           
           $customerqu = substr($customerqu,0,-1);
           
       }
       return $customerqu;
    }
    public function GetSecondlevel($firstlevellist)
    {
       $customerqu = '';
       $customertreelist= $this->db->query("select customer_id from " . DB_PREFIX . "customertree where sponsor_customerid IN(".$firstlevellist.")")->rows;
       if(isset($customertreelist[0]["customer_id"]))
       {
           foreach($customertreelist as $cust)
           {
               $customerqu .= $cust["customer_id"].",";
           }
           
           $customerqu = substr($customerqu,0,-1);
           
       }
       return $customerqu;
    }
    public function GetThirdlevel($secondlevellist)
    {
       $customerqu = '';
       $customertreelist= $this->db->query("select customer_id from " . DB_PREFIX . "customertree where sponsor_customerid IN(".$secondlevellist.")")->rows;
       if(isset($customertreelist[0]["customer_id"]))
       {
           foreach($customertreelist as $cust)
           {
               $customerqu .= $cust["customer_id"].",";
           }
           
           $customerqu = substr($customerqu,0,-1);
           
       }
       return $customerqu;
    }
    
   public function wallet()
   {
      if (!$this->customer->isLogged()) {
 		$this->session->data['redirect'] = $this->url->link('affiliate/commission', '', 'SSL');
  		$this->redirect($this->url->link('mlm/login', '', 'SSL'));
      } 

      $this->language->load('affiliate/commission');
      $this->load->model('account/customer');
      $this->load->model('affiliate/affiliate');
      $this->document->setTitle($this->language->get('wallet_heading'));
      $this->data['breadcrumbs'] = array();
      
      
      $this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
		    'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 
  	
     $this->data['breadcrumbs'][] = array(
            'text'      => 'Dashboard',
		    'href'      => $this->url->link('mlm/dashboard'),
            'separator' => $this->language->get('text_separator')
            
            
      	     ); 
    
      $this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_wallet'),
		    'href'      => $this->url->link('affiliate/commission/wallet', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
       if (isset($this->session->data['success'])) {
    	  $this->data['success'] = $this->session->data['success'];
	 unset($this->session->data['success']);
         
        } else {
        	$this->data['success'] = '';
	}
		
    	$this->data['heading_title'] = $this->language->get('wallet_heading');    	
    	$this->data['users'] = array();    	
    	if(isset($this->request->get['id']))
    	{
            $id = $this->request->get['id'];
    	}
    	else{
            $id = $this->customer->getId();
    	}
        
        $root_customer_id = $this->customer->isLogged();                
        
        $commission_lists = $this->db->query("select (SELECT concat_ws(' ',firstname,lastname) FROM " . DB_PREFIX . "customer where customer_id = c.customer_id) as custname , c.* from " . DB_PREFIX . "cust_commission as c where c.customer_id  = '".$root_customer_id."'")->rows;

        $this->data["commissionlists"] = $commission_lists ;
        $commisssion_val = 0 ;
        $pending_comm = $this->db->query("select sum(commission_total) as totalv from " . DB_PREFIX . "cust_commission as c where c.customer_id  = '".$root_customer_id."' and c.is_paid = '1' ")->row;
        
        if(isset($pending_comm["totalv"]) && !empty($pending_comm["totalv"]))
        {
            $commisssion_val = $pending_comm["totalv"];
        }
        
        $this->data["totalc"]  = $commisssion_val;
                 
        $this->data['back'] = $this->url->link('mlm/dashboard', '', 'SSL');
        	
	if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/affiliate/wallet.tpl')) { 
		$this->template = $this->config->get('config_template') . '/template/affiliate/wallet.tpl';
	} else {
		$this->template = 'default/template/affiliate/wallet.tpl';
	}
	$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);				
	$this->response->setOutput($this->render());
   }
   
   public function submitrequest()
   {
      if (!$this->customer->isLogged())
      {
           $this->session->data['redirect'] = $this->url->link('affiliate/commission/submitrequest', '', 'SSL');
           $this->redirect($this->url->link('mlm/login', '', 'SSL'));
      }
      $this->language->load('affiliate/commission');
      $this->load->model('account/customer');
      $this->load->model('affiliate/affiliate');
      $this->document->setTitle($this->language->get('request_heading'));
      $this->data['breadcrumbs'] = array();
      
      
      
      if (isset($this->session->data['success'])) {
    	  $this->data['success'] = $this->session->data['success'];
          unset($this->session->data['success']);
         
      } else {
            
        	$this->data['success'] = '';
	}
        $usertype = $this->db->query("select user_type from  " . DB_PREFIX . "customer where customer_id='".$this->customer->isLogged()."'")->row;
        
       
        if(isset($usertype['user_type']))
        {
                if($usertype['user_type']=='C')
                {
                	$this->data['heading_title'] = $this->language->get('request_heading'); 
                    $this->data['breadcrumbs'][] = array(
                    'text'      => $this->language->get('text_home'),
            		'href'      => $this->url->link('common/home'),
                    'separator' => false
                  	); 
              	$this->data['breadcrumbs'][] = array(
                        'text'      => 'Dashboard',
            		    'href'      => $this->url->link('mlm/dashboard'),
                        'separator' => $this->language->get('text_separator')
                        
                        
                  	     ); 
                  $this->data['breadcrumbs'][] = array(       	
                    'text'      => $this->language->get('text_tickets'),
            		'href'      => $this->url->link('affiliate/commission/submitrequest', '', 'SSL'),
                    'separator' => $this->language->get('text_separator')
                  	);
                    }
                else
                {
                    $this->data['heading_title'] = $this->language->get('ibo_request_heading'); 
                    $this->data['breadcrumbs'][] = array(
                    'text'      => $this->language->get('text_home'),
        		    'href'      => $this->url->link('common/home'),
                    'separator' => false
                    
              	     ); 
                    
                        $this->data['breadcrumbs'][] = array(
                        'text'      => 'Dashboard',
            		    'href'      => $this->url->link('mlm/dashboard'),
                        'separator' => $this->language->get('text_separator')
                        
                        
                  	     ); 
                
  	}
      $this->data['breadcrumbs'][] = array(       	
        	   'text'      => $this->language->get('ibo_text_tickets'),
                'href'      => $this->url->link('affiliate/commission/submitrequest', '', 'SSL'),
        	   'separator' => $this->language->get('text_separator')
      	);
        }
       
    	$this->data['users'] = array();    	
    	if(isset($this->request->get['id']))
    	{
            $id = $this->request->get['id'];
    	}
    	else{
            $id = $this->customer->getId();
    	}
        
        $root_customer_id = $this->customer->isLogged();                
        
        $message_lists = $this->db->query("select (SELECT concat_ws(' ',firstname,lastname) FROM " . DB_PREFIX . "customer where customer_id = m.customer_id) as custname , m.* from " . DB_PREFIX . "cust_messages as m where m.customer_id  = '".$root_customer_id."'")->rows;
        
      
        foreach ($message_lists as $message_list) {
			$action = array();
             $supportmsgheader=$this->db->query("select distinct user_type,concat_ws(' ',firstname,lastname) as custname,msg_desc,msg_subject,msg_adddate,msg_status," . DB_PREFIX . "cust_messages.customer_id," . DB_PREFIX . "cust_messages.msg_id from  " . DB_PREFIX . "cust_messages inner join 
             " . DB_PREFIX . "customer on " . DB_PREFIX . "cust_messages.customer_id=" . DB_PREFIX . "customer.customer_id 
             where " . DB_PREFIX . "cust_messages.msg_id='".$message_list['msg_id']."' and msg_status=0")->rows;
             $this->data['supportmsgheader']=$supportmsgheader;
         
             $supportmsglists=$this->db->query("select * from  " . DB_PREFIX . "cust_support 
             inner join " . DB_PREFIX . "cust_messages on " . DB_PREFIX . "cust_messages.msg_id=" . DB_PREFIX . "cust_support.msg_id where " . DB_PREFIX . "cust_support.msg_id='".$message_list['msg_id']."' and readstatus='N' and identity='A'")->rows;
  
             $unread=count(array_merge($supportmsgheader,$supportmsglists));  
			$action[] = array(
			       'text' => 'REPLY',
                   'unread'=>$unread,
			       'href' => $this->url->link('affiliate/commission/support', 'msg_id=' . $message_list['msg_id'], 'SSL')
			);
               
			$this->data['message_lists'][] = array(
				'custname'    => $message_list['custname'],
				'msg_id'           => $message_list['msg_id'],
                                'msg_subject'     => $message_list['msg_subject'],
                                'msg_desc'     => $message_list['msg_desc'],
				'msg_adddate'          => $message_list['msg_adddate'],
				'msg_status' => $message_list['msg_status'],
				'action'         => $action
			);
		}	
        $this->data['back'] = $this->url->link('mlm/dashboard', '', 'SSL');
        $this->data['new_ticket'] = $this->url->link('affiliate/commission/newticket', '', 'SSL');
	if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/affiliate/messages.tpl')) { 
		$this->template = $this->config->get('config_template') . '/template/affiliate/messages.tpl';
	} 
        else
        {
		$this->template = 'default/template/affiliate/messages.tpl';
	}
	$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);				
	$this->response->setOutput($this->render());
   }
   public function newticket()
   {
      if (!$this->customer->isLogged()) 
      {
           $this->session->data['redirect'] = $this->url->link('affiliate/commission/newticket', '', 'SSL');
           $this->redirect($this->url->link('mlm/login', '', 'SSL'));
      }
      $this->language->load('affiliate/commission');
      $this->load->model('account/customer');
      $this->load->model('affiliate/affiliate');
      $this->document->setTitle($this->language->get('new_request_heading'));
      $this->data['breadcrumbs'] = array();
      $this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
		    'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 
  	
    $this->data['breadcrumbs'][] = array(
            'text'      => 'Dashboard',
		    'href'      => $this->url->link('mlm/dashboard'),
            'separator' => $this->language->get('text_separator')
            
            
      	     ); 
    
      $this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_new_tickets'),
		    'href'      => $this->url->link('affiliate/commission/submitrequest', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
      if (isset($this->session->data['success'])) {
    	  $this->data['success'] = $this->session->data['success'];
          unset($this->session->data['success']);
         
      } else {
            
        	$this->data['success'] = '';
	}
		
    	$this->data['heading_title'] = $this->language->get('new_request_heading');    	
    	$this->data['users'] = array();    	
    	if(isset($this->request->get['id']))
    	{
            $id = $this->request->get['id'];
    	}
    	else{
            $id = $this->customer->getId();
    	}
        
        
        
        if(isset($_POST['submit']))
        {
            $cid = $this->customer->isLogged();                
            $subject=trim($_POST["msg_subject"]);
            $message=trim($_POST["msg_desc"]);
            $this->db->query("insert into " . DB_PREFIX . "cust_messages (msg_subject,msg_desc,msg_adddate,msg_status,customer_id) values ('".$subject."','".$message."','".date("Y-m-d")."','0', '".$cid."')");
            $this->data["success"] = "Request has been send.";
            header("Location:index.php?route=mlm/dashboard&status=send");
        }
        
        $this->data['back'] = $this->url->link('affiliate/commission/submitrequest', '', 'SSL');
        	
	if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/affiliate/new_messages.tpl')) { 
		$this->template = $this->config->get('config_template') . '/template/affiliate/new_messages.tpl';
	} else {
		$this->template = 'default/template/affiliate/new_messages.tpl';
	}
	$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);				
	$this->response->setOutput($this->render());
   }
   public function support()
   {
      
      if (!$this->customer->isLogged()) 
      {
           $this->session->data['redirect'] = $this->url->link('affiliate/commission/newticket', '', 'SSL');
           $this->redirect($this->url->link('mlm/login', '', 'SSL'));
      }
      $this->language->load('affiliate/commission');
      $this->load->model('account/customer');
      $this->load->model('affiliate/affiliate');
      $this->document->setTitle($this->language->get('new_request_heading'));
      $this->data['breadcrumbs'] = array();
      $this->data['breadcrumbs'][] = 
              array(
        	'text'      => $this->language->get('text_home'),
		    'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 
  	$this->data['breadcrumbs'][] = array(
            'text'      => 'Dashboard',
		    'href'      => $this->url->link('mlm/dashboard'),
            'separator' => $this->language->get('text_separator')
            
            
      	     ); 
    
      $this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_new_tickets'),
            'href'      => $this->url->link('affiliate/commission/submitrequest', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
      if (isset($this->session->data['success'])) {
    	  $this->data['success'] = $this->session->data['success'];
          unset($this->session->data['success']);
         
      } else {
            
        	$this->data['success'] = '';
	}
		
    	$this->data['heading_title'] = $this->language->get('new_request_heading');    	
    	$this->data['users'] = array();    	
    	if(isset($this->request->get['id']))
    	{
            $id = $this->request->get['id'];
    	}
    	else{
            $id = $this->customer->getId();
    	}
        $msg_id=trim($_REQUEST["msg_id"]);
        $this->db->query("update " . DB_PREFIX . "cust_support set readstatus='R' where msg_id='".$msg_id."' and  identity='A'");
        if(isset($_POST['submit']))
        {
            $cid = $this->customer->isLogged(); 
            $msg_id=trim($_REQUEST["msg_id"]);
            $identity=$_REQUEST["identity"];
            $message=trim($_REQUEST["msg_desc"]);
            $msg_date=date('Y-m-d');
            $this->db->query("insert into " . DB_PREFIX . "cust_support (message,msg_date,readstatus,msg_id,identity) values ('".$message."','".$msg_date."','N','".$msg_id."','".$identity."')"); 
            $this->data["success"] = "Request has been send.";
            header("Location:index.php?route=affiliate/commission/support&msg_id=$msg_id");
      }
         
        $msgid=$_REQUEST['msg_id'];
        $this->data['new_ticket'] = $this->url->link('affiliate/commission/newticket', '', 'SSL');
         $supportmsgheader=$this->db->query("select distinct user_type,concat_ws(' ',firstname,lastname) as custname,msg_desc,msg_subject,msg_adddate,msg_status," . DB_PREFIX . "cust_messages.customer_id," . DB_PREFIX . "cust_messages.msg_id from  " . DB_PREFIX . "cust_messages inner join 
         " . DB_PREFIX . "customer on " . DB_PREFIX . "cust_messages.customer_id=" . DB_PREFIX . "customer.customer_id 
         where " . DB_PREFIX . "cust_messages.customer_id='".$this->customer->isLogged()."' and  " . DB_PREFIX . "cust_messages.msg_id='".$msgid."'")->rows;
         $this->data['supportmsgheader']=$supportmsgheader;
        /*  "select * from  mlm_cust_support 
         inner join mlm_cust_messages on mlm_cust_messages.msg_id=mlm_cust_support.msg_id where mlm_cust_support.msg_id='".$msgid."'"; exit;*/
         $supportmsglists=$this->db->query("select * from  " . DB_PREFIX . "cust_support 
         inner join " . DB_PREFIX . "cust_messages on " . DB_PREFIX . "cust_messages.msg_id=" . DB_PREFIX . "cust_support.msg_id where " . DB_PREFIX . "cust_messages.customer_id='".$this->customer->isLogged()."' and " . DB_PREFIX . "cust_support.msg_id='".$msgid."'")->rows;
         $this->data['supportmsglists']=$supportmsglists;
         $this->data['back'] = $this->url->link('affiliate/commission/submitrequest', '', 'SSL');
        	
	if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/affiliate/support.tpl')) { 
		$this->template = $this->config->get('config_template') . '/template/affiliate/support.tpl';
	}
        else
        {
		$this->template = 'default/template/affiliate/support.tpl';
	}
	$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);				
	$this->response->setOutput($this->render());
   }
}
?>
