<?php
class ControllerMlmEdit extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('mlm/edit', '', 'SSL');

			$this->redirect($this->url->link('mlm/login', '', 'SSL'));
		}

		$this->language->load('mlm/edit');

		$this->document->setTitle($this->language->get('heading_title'));

		
        $this->load->model('mlm/edit');
        
        /*if($this->request->server['REQUEST_METHOD'] == 'POST')
        {
            echo "<pre>";print_r($_POST);
        }*/
        
       // echo "<pre>";print_r($_REQUEST);die;
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) 
        {
            
            
			$this->model_mlm_edit->editCustomer($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('mlm/dashboard', '', 'SSL'));
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),     	
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('mlm/dashboard', '', 'SSL'),        	
			'separator' => $this->language->get('text_separator')
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_edit'),
			'href'      => $this->url->link('mlm/edit', '', 'SSL'),       	
			'separator' => $this->language->get('text_separator')
		);

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_your_details'] = $this->language->get('text_your_details');

		$this->data['entry_firstname'] = $this->language->get('entry_firstname');
		$this->data['entry_lastname'] = $this->language->get('entry_lastname');
		$this->data['entry_email'] = $this->language->get('entry_email');
        
        
        $this->data['entry_dob'] = $this->language->get('entry_dob');
		$this->data['entry_addme'] = $this->language->get('entry_addme');
        $this->data['entry_company'] = $this->language->get('entry_company');
		$this->data['entry_company_id'] = $this->language->get('entry_company_id');
        
        
        $this->data['text_sponsor'] = $this->language->get('text_sponsor');
		$this->data['text_pvvalue'] = $this->language->get('text_pvvalue');
        
        $this->data['text_select'] = $this->language->get('text_select');
        
        $this->data['entry_address_1'] = $this->language->get('entry_address_1');
		$this->data['entry_address_2'] = $this->language->get('entry_address_2');
        $this->data['entry_city'] = $this->language->get('entry_city');
		$this->data['entry_postcode'] = $this->language->get('entry_postcode');
    	$this->data['entry_city'] = $this->language->get('entry_city');
    	$this->data['entry_country'] = $this->language->get('entry_country');
    	$this->data['entry_zone'] = $this->language->get('entry_zone');
        $this->data['entry_account'] = $this->language->get('entry_account');
        $this->data['text_your_address'] = $this->language->get('text_your_address');
       

		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['firstname'])) {
			$this->data['error_firstname'] = $this->error['firstname'];
		} else {
			$this->data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			$this->data['error_lastname'] = $this->error['lastname'];
		} else {
			$this->data['error_lastname'] = '';
		}

		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}	
       
       	if (isset($this->error['company_id'])) {
			$this->data['error_company_id'] = $this->error['company_id'];
		} else {
			$this->data['error_company_id'] = '';
		}
		
  	
								
  		if (isset($this->error['address_1'])) {
			$this->data['error_address_1'] = $this->error['address_1'];
		} else {
			$this->data['error_address_1'] = '';
		}
   		
		if (isset($this->error['city'])) {
			$this->data['error_city'] = $this->error['city'];
		} else {
			$this->data['error_city'] = '';
		}
		
		if (isset($this->error['postcode'])) {
			$this->data['error_postcode'] = $this->error['postcode'];
		} else {
			$this->data['error_postcode'] = '';
		}
		if (isset($this->error['country'])) {
			$this->data['error_country'] = $this->error['country'];
		} else {
			$this->data['error_country'] = '';
		}
		if (isset($this->error['zone'])) {
			$this->data['error_zone'] = $this->error['zone'];
		} else {
			$this->data['error_zone'] = '';
		}
		
       
		$this->data['action'] = $this->url->link('mlm/edit', '', 'SSL');


        //echo "<pre>";
        //print_r($_POST);die;
		if ($this->request->server['REQUEST_METHOD'] != 'POST') 
        {
			
             $customer_info = $this->model_mlm_edit->getCustomer($this->customer->getId());
  	         $address_info = $this->model_mlm_edit->getAddress($this->customer->getId());
             $member_tree = $this->model_mlm_edit->getMemberDetails($this->customer->getId());
             
            $country_id= $address_info['country_id'];
             if($country_id)
             {
            
            	$this->load->model('localisation/zone');
                $this->data['zone_info']=$this->model_localisation_zone->getZonesByCountryId($country_id);
                
             }
            
		}

		if (isset($this->request->post['firstname'])) {
			$this->data['firstname'] = $this->request->post['firstname'];
		} elseif (isset($customer_info)) {
			$this->data['firstname'] = $customer_info['firstname'];
		} else {
			$this->data['firstname'] = '';
		}

		if (isset($this->request->post['lastname'])) {
			$this->data['lastname'] = $this->request->post['lastname'];
		} elseif (isset($customer_info)) {
			$this->data['lastname'] = $customer_info['lastname'];
		} else {
			$this->data['lastname'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (isset($customer_info)) {
			$this->data['email'] = $customer_info['email'];
		} else {
			$this->data['email'] = '';
		}
        
        
        
        
        if (isset($customer_info)) {
            
            $sponsor_detail=$this->model_mlm_edit->getMemberDetails($customer_info['customer_id']);
            if(isset($sponsor_detail))
            {
			$this->data['sponsor'] = $sponsor_detail['sponsor_code'];
            }
		} else {
			$this->data['sponsor'] = '';
		}
        
        if (isset($customer_info)) {
			$this->data['pv_value'] = $customer_info['pv_value'];
		} else {
			$this->data['pv_value'] = '';
		}
        
        
        if (isset($this->request->post['company'])) {
    		$this->data['company'] = $this->request->post['company'];
		} elseif (isset($address_info)) {
			$this->data['company'] = $address_info['company'];
		} else {
			$this->data['company'] = '';
		}
        
        
        	// Company ID
		if (isset($this->request->post['company_id'])) {
    		$this->data['company_id'] = $this->request->post['company_id'];
		}elseif (isset($address_info)) {
			$this->data['company_id'] = $address_info['company_id'];
		}  else {
			$this->data['company_id'] = '';
		}
		
		// Tax ID
				
		if (isset($this->request->post['address_1'])) {
    		$this->data['address_1'] = $this->request->post['address_1'];
		}elseif (isset($address_info)) {
			$this->data['address_1'] = $address_info['address_1'];
		}  else {
			$this->data['address_1'] = '';
		}
		if (isset($this->request->post['address_2'])) {
    		$this->data['address_2'] = $this->request->post['address_2'];
		}elseif (isset($address_info)) {
			$this->data['address_2'] = $address_info['address_2'];
		}  else {
			$this->data['address_2'] = '';
		}
        
        
        if (isset($this->request->post['city'])) {
    		$this->data['city'] = $this->request->post['city'];
		}elseif (isset($address_info)) {
			$this->data['city'] = $address_info['city'];
		}  else {
			$this->data['city'] = '';
		}
        
		if (isset($this->request->post['postcode'])) {
    		$this->data['postcode'] = $this->request->post['postcode'];
		} elseif (isset($address_info)) {
			$this->data['postcode'] = $address_info['postcode'];		
		} else {
			$this->data['postcode'] = '';
		}
		
		
    	if (isset($this->request->post['country_id'])) {
      		$this->data['country_id'] = $this->request->post['country_id'];
		} elseif ( isset($address_info)) {
			$this->data['country_id'] = $address_info['country_id'];		
		} else {	
      		$this->data['country_id'] = $this->config->get('config_country_id');
    	}
        
        
        
        /*
        
        if (isset($this->request->post['add_me'])) {
      		$this->data['add_me'] = $this->request->post['add_me'];
		} elseif ( isset($member_tree)) {
			$this->data['add_me'] = $member_tree['leg'];		
		} else {	
      		$this->data['add_me'] = '';
    	}
        
        */
        
        //echo $this->request->post['zone_id'];die;
        
    	if (isset($this->request->post['zone_id'])) {
      		$this->data['zone_id'] = $this->request->post['zone_id']; 	
		} elseif (isset($address_info)) {
			$this->data['zone_id'] =$address_info['zone_id'];			
		} else {
      		$this->data['zone_id'] = '';
    	}
		//echo $this->data['zone_id'];
		$this->load->model('localisation/country');
		
    	$this->data['countries'] = $this->model_localisation_country->getCountries();
        
        $country_id = $this->data['country_id'];
		
          if($country_id)
             {
            
            	$this->load->model('localisation/zone');
                $this->data['zone_info']=$this->model_localisation_zone->getZonesByCountryId($country_id);
                
             }
      
		$this->data['back'] = $this->url->link('mlm/dashboard', '', 'SSL');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mlm/edit.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/mlm/edit.tpl';
		} else {
			$this->template = 'default/template/mlm/edit.tpl';
		}

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);

		$this->response->setOutput($this->render());	
	}

	protected function validate() {
		if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen($this->request->post['firstname']) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen($this->request->post['lastname']) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}
        
		if (($this->customer->getEmail() != $this->request->post['email']) && $this->model_mlm_edit->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}
        
        if ((utf8_strlen($this->request->post['address_1']) < 3) || (utf8_strlen($this->request->post['address_1']) > 128)) {
      		$this->error['address_1'] = $this->language->get('error_address_1');
    	}
    	if ((utf8_strlen($this->request->post['city']) < 2) || (utf8_strlen($this->request->post['city']) > 128)) {
      		$this->error['city'] = $this->language->get('error_city');
    	}
        if ($this->request->post['country_id'] == '') {
      		$this->error['country'] = $this->language->get('error_country');
    	}
		
    	if ($this->request->post['zone_id'] == '') {
      		$this->error['zone'] = $this->language->get('error_zone');
    	}
       // echo "<pre>"; print_r($this->error);die;
        /*
        		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
        			$this->error['telephone'] = $this->language->get('error_telephone');
        		}
        */
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
    
    
    
    public function country() {
		$json = array();
		$this->load->model('localisation/country');
                $country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);
		if ($country_info) {
			$this->load->model('localisation/zone');
			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']		
			);
		}
                $this->response->setOutput(json_encode($json));
    }	
    
    
}
?>