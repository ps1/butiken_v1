<?php 
class ControllerMlmDownline extends Controller {
	private $error = array();
	      
  	public function index() {
  
       if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('mlm/edit', '', 'SSL');

			$this->redirect($this->url->link('mlm/login', '', 'SSL'));
		}	   
        $this->language->load('mlm/downline');
        $this->load->model('mlm/downline');
              
        $this->document->setTitle($this->language->get('heading_title'));
    	 
      	$this->data['breadcrumbs'] = array();
      	
        $this->data['breadcrumbs'][] = array(
                        'text'      => $this->language->get('text_home'),
						'href'      => $this->url->link('common/home'),        	
                        'separator' => false
                ); 
      	$this->data['breadcrumbs'][] = array(
			        	'text'      => $this->language->get('text_account'),
						'href'      => $this->url->link('mlm/dashboard', '', 'SSL'),      	
			        	'separator' => $this->language->get('text_separator')
			   	);
		
      	$this->data['breadcrumbs'][] = array(
			        	'text'      => $this->language->get('text_downline'),
						'href'      => $this->url->link('mlm/downline', '', 'SSL'),      	
			        	'separator' => $this->language->get('text_separator')
      			);
		
		if(!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('mlm/dashboard', '', 'SSL');	  
            $this->redirect($this->url->link('mlm/login', '', 'SSL'));
    	}                                 
    
    $root_customer_id = $this->customer->isLogged();    
    $this->data["rootid"] = $root_customer_id;
    
    $codedetails = $this->db->query("select member_id from ".DB_PREFIX."customer where customer_id = '".$root_customer_id."'")->row;
    $codedetails["member_id"] = trim($codedetails["member_id"]);
    
    
    $this->data['leftcount']     = $this->model_mlm_downline->cntlft($codedetails["member_id"]);
    $this->data['middlecount']     = $this->model_mlm_downline->cntmidd($codedetails["member_id"]);
    $this->data['rightcount']    = $this->model_mlm_downline->cntrgt($codedetails["member_id"]);
    
    // $this->data['middlecount']     = $this->cntmidd($codedetails["member_id"]);
    //$this->data['leftcount']     = $this->cntlft($codedetails["member_id"]);
   
   // $this->data['rightcount']    = $this->cntrgt($codedetails["member_id"]);
    
    
    	        //$this->data['lefPVtcount']   = $this->model_mlm_downline->cntlftPV($userid);
				//$this->data['rightPVcount']  = $this->model_mlm_downline->cntrgtPV($userid);
                //$this->data['middlePVcount']  = $this->model_mlm_downline->cntmiddPV($userid);
    
       
    $downline_members = $this->db->query("select c.* ,t.* , (select package_name from ".DB_PREFIX."mst_packages where package_id = c.package_id) as package from ".DB_PREFIX."customer c , ".DB_PREFIX."membertree t where c.customer_id = t.customer_id  and  c.customer_id = '".$root_customer_id."'")->row;
    
	//echo "<pre>"; print_r($downline_members); exit();
	
    $this->data["members"] = $downline_members;
    $this->data['selectedusername']  = $downline_members['member_id'];
       
    if(isset($downline_members['left_sponsorcode']) && !empty($downline_members['left_sponsorcode'])) {
       
        $leftmemids = $this->model_mlm_downline->leftmembersid($root_customer_id);  
    }   
    if(isset($downline_members['middle_sponsorcode']) && !empty($downline_members['middle_sponsorcode'])) {
       
        $middlememids = $this->model_mlm_downline->middlemembersid($root_customer_id);  
    }    
    if(isset($downline_members['right_sponsorcode']) && !empty($downline_members['right_sponsorcode'])) {
        $rightmemids = $this->model_mlm_downline->rightmembersid($root_customer_id); 
    }
     
    
    $this->data['lpagination']  = '';
      
    if(isset($leftmemids) && !empty($leftmemids))
    {
        $this->data['lmemids'] = $leftmemids  ; //	substr(rtrim($leftmemids),0,-1);
        if (isset($this->request->get['lpage'])) {
			$lpage = $this->request->get['lpage'];
		} else {
			$lpage = 1;
        }       
        $start = ($lpage - 1) * 15;
        $end = 15;    
        if ($start < 0) {
            $start = 0;
		}
		
		if ($end < 1) {
            $end = 15;
        }
       
         $lefthandMembers = '';
         $totalleftcount = 0 ;
         $ototal = 0;
         $totalleftmembers = 0 ; 
         if(isset($leftmemids) && !empty($leftmemids))
         {
           $lefthandMembers = $this->db->query("select c.* , (select package_name from ".DB_PREFIX."mst_packages where package_id = c.package_id) as package from ".DB_PREFIX."customer c where c.customer_id IN (".$leftmemids.")  LIMIT " .(int)$start . "," .(int)$end )->rows;
           $totalleftcount = $this->db->query("select count(*) as num from ".DB_PREFIX."customer c where c.customer_id IN (".$leftmemids.")")->row;
           $totalleftmembers = $totalleftcount["num"];
        //   $lefthandgsv = $this->db->query("SELECT SUM(order_pv) as ototal FROM ".DB_PREFIX."order WHERE customer_id IN (select customer_id from ".DB_PREFIX."customer_relation where distributor_id IN (".$leftmemids.")) or customer_id IN (".$leftmemids.")   ")->row;
       //    $ototal = $lefthandgsv["ototal"];
        }
       
	   //     $this->data['left_gsv'] = $ototal;
	        $this->data["left_members"] = $lefthandMembers;
	        // $pagination = new Pagination();  
			// $pagination->total = $totalleftmembers;
			// $pagination->page = $lpage;
			// $pagination->limit = 100; // $this->config->get('config_admin_limit');
		    // $pagination->text = $this->language->get('text_pagination');
			// $pagination->url = $this->url->link('mlm/downline', 'token=' . $this->session->data['token'] . $url . '&lpage={page}', 'SSL');
			// $this->data['pagination'] = $pagination->render();         
     } 
     
         
if(isset($middlememids) && !empty($middlememids))
    {
        $this->data['middlemids'] = $middlememids  ; //	substr(rtrim($leftmemids),0,-1);
        if (isset($this->request->get['lpage'])) {
			$lpage = $this->request->get['lpage'];
		} else {
			$lpage = 1;
        }       
        $start = ($lpage - 1) * 15;
        $end = 15;    
        if ($start < 0) {
            $start = 0;
		}
		
		if ($end < 1) {
            $end = 15;
        }
       
         $lefthandMembers = '';
         $totalleftcount = 0 ;
         $ototal = 0;
         $totalleftmembers = 0 ; 
         if(isset($middlememids) && !empty($middlememids))
         {
           $lefthandMembers = $this->db->query("select c.* , (select package_name from ".DB_PREFIX."mst_packages where package_id = c.package_id) as package from ".DB_PREFIX."customer c where c.customer_id IN (".$middlememids.")  LIMIT " .(int)$start . "," .(int)$end )->rows;
           $totalleftcount = $this->db->query("select count(*) as num from ".DB_PREFIX."customer c where c.customer_id IN (".$middlememids.")")->row;
           $totalleftmembers = $totalleftcount["num"];
       
        } $this->data["middle_members"] = $lefthandMembers;
	             
     }
     
        
     if(isset($rightmemids) && !empty($rightmemids))
     {
          if (isset($this->request->get['rpage'])) {
			$rpage = $this->request->get['rpage'];
	  } else {
			$rpage = 1;
      }       
        $start = ($rpage - 1) * 100;
        $end = 100;    
        if ($start < 0) {
            $start = 0;
		}
		
		if ($end < 1) {
            $end = 100;
        }
        
         $rendchar =  substr($rightmemids,-1);
         if(isset($rendchar) && $rendchar == ',')
         {
            $rightmemids = substr($rightmemids, 0,-1);
         }
         $righthandMembers = '';
         $totalrightcount = 0 ;
         $rototal = 0;
         $totalrightmembers = 0 ;         
         if(isset($rightmemids) && !empty($rightmemids))
         {
             //  LIMIT " .(int)$start . "," .(int)$end
             $righthandMembers = $this->db->query("select c.*,(select package_name from ".DB_PREFIX."mst_packages where package_id = c.package_id) as package from ".DB_PREFIX."customer c where c.customer_id IN (".$rightmemids.")" )->rows;
             $totalrightcount = $this->db->query("select count(*) as num from ".DB_PREFIX."customer c where c.customer_id IN (".$rightmemids.")")->row;
          //   $righthandgsv = $this->db->query("SELECT SUM(order_pv) as ototal FROM ".DB_PREFIX."order WHERE customer_id IN (select customer_id from ".DB_PREFIX."customer_relation where distributor_id IN (".$rightmemids."))  or customer_id IN (".$rightmemids.")")->row;
           //  $rototal  = (isset($righthandgsv["ototal"]) && !empty($righthandgsv["ototal"]) ? $righthandgsv["ototal"] : 0 );
           //  $totalrightmembers = $totalrightcount["num"];
         }
		    $this->data['right_gsv'] = $rototal;
		    $this->data["right_members"] = $righthandMembers;
		    // $rpagination = new Pagination();
		    // $rpagination->total = $totalrightmembers ; 
		    // $rpagination->page = $rpage;
		    // $rpagination->limit = $this->config->get('config_admin_limit');
		    // $rpagination->text = $this->language->get('text_pagination');
		    // $rpagination->url = $this->url->link('account/account/directdownline', 'token=' . $this->session->data['token'] . $url . '&rpage={page}', 'SSL');
		    // $this->data['rpagination'] = $rpagination->render();    
	 }
			$this->data['heading_title'] = $this->language->get('heading_title');		
			$this->data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('mlm/login_member', '', 'SSL'));
			$this->data['text_your_details'] = $this->language->get('text_your_details');
	    	$this->data['text_your_address'] = $this->language->get('text_your_address');
	    	$this->data['text_your_password'] = $this->language->get('text_your_password');
			$this->data['text_newsletter'] = $this->language->get('text_newsletter');
			$this->data['text_yes'] = $this->language->get('text_yes');
			$this->data['text_no'] = $this->language->get('text_no');
			$this->data['text_select'] = $this->language->get('text_select');
			$this->data['button_continue'] = $this->language->get('button_continue');
    
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
	 
	    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mlm/memberdownline.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/mlm/memberdownline.tpl';
		} else {
			$this->template = 'default/template/mlm/memberdownline.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
				
		$this->response->setOutput($this->render());	
  	
	}


}
?>