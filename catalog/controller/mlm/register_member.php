<?php 
class ControllerMlmRegisterMember extends Controller {
	private $error = array();
	      
  	public function index() {
  
        if ($this->customer->isLogged()) 
        {
	       $this->redirect($this->url->link('mlm/dashboard', '', 'SSL'));
        }
        $this->language->load('mlm/register_member');
        $this->load->model('mlm/register_member');
        
        $packages=$this->model_mlm_register_member->getPackages(); 
        
        foreach($packages as $pck)
        {
            $pck_array[] = array(
            	
                "package_id"		=> $pck['package_id'],
                "package_name"		=> $pck['package_name'],
                "package_annual_fees"=>$this->currency->format($pck['package_annual_fees']),
                "package_img"		=> $pck['package_img']              
            
            );
        }        
        $this->data["packages"] = $pck_array;         
        $this->document->setTitle($this->language->get('heading_title'));
    	if(($this->request->server['REQUEST_METHOD'] == 'POST'  && $this->validate() == true ))
        {
            $post_url = "https://secure.authorize.net/gateway/transact.dll";
            $package_information = $this->db->query("select * from ". DB_PREFIX . "mst_packages where package_id =  '".$this->request->post["choosed_package"]."'")->row;
            $amount = number_format($package_information["package_annual_fees"],2);
            $post_values = array(	
          	    "x_login"		=> "65PNp4fZ",
                    "x_tran_key"	=> "9a9f6h2KkU93X9Cc",
                    "x_version"		=> "3.1",
                    "x_delim_data"	=> "TRUE",
                    "x_delim_char"	=> "|",
                    "x_relay_response"	=> "FALSE",
                    "x_type"		=> "AUTH_CAPTURE",
                    "x_method"		=> "CC",
                    "x_card_num"	=> trim($_POST['ccnumber']),
                    "x_exp_date"	=> trim($_POST['card_month'].$_POST['card_year']),
                    "x_amount"		=> trim($amount),
                    "x_description"	=> "Congratulation, you have successfully registered as Member with [butiken.COM]",
                    "x_first_name"	=> $_POST['firstname'],
                    "x_last_name"	=> $_POST['lastname'],
                    "x_address"		=> $_POST['address_1'],
                    "x_zip"			=> $_POST['postcode']
           );                
           $post_string = "";
           foreach( $post_values as $key => $value )
           { 
    	      $post_string .= "$key=" . urlencode( $value ) . "&"; 
           }
           $post_string = rtrim( $post_string, "& " );
	       $request = curl_init($post_url); 
           $response_array[0]=1  ;  
           if($response_array[0]== 1 )
           {
               $customerid  =  $this->model_mlm_register_member->addCustomer($this->request->post);                 
			   $leg = trim($_POST["add_me"]);    
			   $sponsor_code = strtoupper(trim($_POST["sponsor_code"]));
			//sponsored member details
			   $sponsorReference  = $this->db->query("select customer_id,sponsor_str,member_id from ".DB_PREFIX."membertree where UPPER(member_id) = '".$sponsor_code."'")->row;    
			   if(isset($sponsorReference['sponsor_str']) && !empty($sponsorReference["sponsor_str"]))
			   {             
			       $sponsorStr = $sponsorReference['sponsor_str'].",".$customerid;                     
			   } else {        
				   $sponsorStr = ",".$customerid;     
			   }       
			 
			   if($leg == 'L')
			   {
				    $leftLegSponsors   = $this->db->query("select left_sponsorcode,customer_id,parent_str,sponsor_str from ".DB_PREFIX."membertree where UPPER(member_id) = '".strtoupper(trim($sponsor_code))."'")->row;
				    if(empty($leftLegSponsors["left_sponsorcode"]))
				    {
				        $ForParent= trim($_POST["sponsor_code"]);
				    } else {
				        $ForParent = $leftLegSponsors["left_sponsorcode"];
				    }      
                                   // echo $ForParent; exit(); 
			   }
			   
			   if($leg == 'C')
			   {
				    $centerLegSponsors   = $this->db->query("select middle_sponsorcode,customer_id,parent_str,sponsor_str from ".DB_PREFIX."membertree where UPPER(member_id) = '".strtoupper(trim($sponsor_code))."'")->row;
				    if(empty($centerLegSponsors["middle_sponsorcode"]))
				    {
				        $ForParent= trim($_POST["sponsor_code"]);
				    } else {
				        $ForParent = $centerLegSponsors["middle_sponsorcode"];
				    }      
			   }
			
			   if($leg == 'R')
			   {
			        $rightLegSponsors   = $this->db->query("select right_sponsorcode,customer_id,parent_str,sponsor_str from ".DB_PREFIX."membertree where UPPER(member_id) = '".  strtoupper(trim($_POST["sponsor_code"]))."'")->row;
			        $sponsorStr = $rightLegSponsors['sponsor_str'].",".$customerid;
			        if(empty($rightLegSponsors["right_sponsorcode"]))
			        {
			              $ForParent= strtoupper(trim($_POST["sponsor_code"]));
			        } else {
                                      $ForParent = strtoupper($rightLegSponsors["right_sponsorcode"]);
				    }      
			    }
			     if($leg=='L')
				{        
				     $count = 1;
				     $LSide = $ForParent;
				     $PrevLSide =  $ForParent;
                                     
                                  //   echo $LSide."-----------". $PrevLSide; exit(); 
				       while($count > 0) 
				       {
				        $leftmsrn= $this->db->query("select left_sponsorcode from ".DB_PREFIX."membertree where UPPER(member_id) = '".strtoupper($LSide)."'")->row;
				        
                                        if(!isset($leftmsrn['left_sponsorcode']) || empty($leftmsrn['left_sponsorcode']))
				        {
				            $parentId = $PrevLSide;                
				            $count=0;                 
				            $this->db->query("update ".DB_PREFIX."membertree set left_sponsorcode = '".strtoupper(trim($_POST["memberid"]))."' where UPPER(member_id) = '".strtoupper($parentId)."'");
				        }
				        else
				        {
				            $PrevLSide = strtoupper($leftmsrn['left_sponsorcode']);
                                            $LSide = $PrevLSide;
				        }
				     } 
				 }
				 
				 if($leg=='C')
				 {        
				       $count = 1;
				       $CSide = $ForParent;
				       $PrevCSide =  $ForParent;
				       while($count > 0) 
					   {
				         $centermsrn= $this->db->query("select middle_sponsorcode from ".DB_PREFIX."membertree where UPPER(member_id) = '".strtoupper($CSide)."'")->row;
				         if(!isset($centermsrn['middle_sponsorcode']) || empty($centermsrn['middle_sponsorcode']))
				         {
				            $parentId = $PrevCSide;                
				            $count=0;                 
				            $this->db->query("update ".DB_PREFIX."membertree set middle_sponsorcode = '".strtoupper(trim($_POST["memberid"]))."' where UPPER(member_id) = '".strtoupper($parentId)."'");
				         }
				         else
				         {
				            $PrevCSide = strtoupper($centermsrn['middle_sponsorcode']);
                                            $CSide = $PrevCSide;
				         }
				        } 
				  }
				  
				  if($leg=='R')
				  {
				       $RSide = $ForParent;
				       $PrevRSide = $ForParent;
				       $count = 1;
				       while($count > 0) 
					   {
				         $rightmsrn= $this->db->query("select right_sponsorcode from ".DB_PREFIX."membertree where UPPER(member_id) = '".strtoupper($RSide)."'")->row;
				         if(!isset($rightmsrn['right_sponsorcode']) || empty($rightmsrn['right_sponsorcode']))
				         {
				         	 $rightmsrn['right_sponsorcode'] = strtoupper($rightmsrn['right_sponsorcode']);
				             $parentId = $PrevRSide;                
				             $count=0;                 
				             $this->db->query("update ".DB_PREFIX."membertree set right_sponsorcode = '".strtoupper(trim($_POST["memberid"]))."' where UPPER(member_id) = '".strtoupper($parentId)."'");
				         }
				         else
				         {
				              $PrevRSide = strtoupper($rightmsrn['right_sponsorcode']);
                                              $RSide = $PrevRSide; 
				         }
				        }         
				    }
				         $parentMemberStr =  $this->db->query("select parent_str,parent_customerid from ".DB_PREFIX."membertree where UPPER(member_id) = '".trim(strtoupper($parentId))."'")->row;         
						 $pcustomerid =  (isset($parentMemberStr["parent_customerid"]) && !empty($parentMemberStr["parent_customerid"]) ? $parentMemberStr["parent_customerid"] : '');
				         $sponsor_cid = (isset($sponsorReference["customer_id"]) && !empty($sponsorReference["customer_id"]) ? $sponsorReference["customer_id"] : '');
				       //  $sponsor_code = $sponsorReference["member_id"];
				         $sponsorStr = $sponsorStr;
						 $parentMemberStr["parent_str"] = (isset($parentMemberStr["parent_str"]) && !empty($parentMemberStr["parent_str"]) ? $parentMemberStr["parent_str"] : '');
				         $parentStr = $parentMemberStr["parent_str"].",".$customerid;
				         $total_pv = '';
				         $parent_sponsorcode = $parentId;
				         $left_sponsorcode = '';
				         $right_sponsorcode = '';
						 /*******Logic for update the customer Tree Table ***/
							$this->db->query("insert into ".DB_PREFIX."membertree (member_id,customer_id,parent_customerid,sponsor_customerid,sponsor_code,sponsor_str,parent_str,leg,total_pv,parent_sponsorcode,left_sponsorcode,right_sponsorcode,date_added) values ('".trim(strtoupper($_POST["memberid"]))."','".$customerid."','".$pcustomerid."','".$sponsor_cid."','".strtoupper($sponsor_code)."','".$sponsorStr."','".$parentStr."','".$leg."','".$total_pv."','".strtoupper($parent_sponsorcode)."','".strtoupper($left_sponsorcode)."','".strtoupper($right_sponsorcode)."','".date("Y-m-d H:i:s")."')");
							$sponsorPersonID =  $this->db->query("select customer_id from ".DB_PREFIX."customer where UPPER(member_id) = '".strtoupper($sponsor_code)."'")->row;         
							$sponsorPersonID["customer_id"] = (isset($sponsorPersonID["customer_id"]) ? $sponsorPersonID["customer_id"] : '');
							$this->db->query("insert into ".DB_PREFIX."member_relations (distributor_id,distributor_memberid,sponsor_cookie_code,add_date,reference_distributor_id) values ('".$customerid."','".strtoupper(trim($_POST["memberid"]))."','".strtoupper($sponsor_code)."','".date("Y-m-d H:i:s")."' , '".trim($sponsorPersonID["customer_id"])."') ");
							$p_customers = explode(",",$parentStr);                                                          
						//$pv_value = $package_information["pv_value"];
						$s_customers = explode(",",$sponsorStr); 
                                                
                                                /*
						for($j=0;$j<sizeof($p_customers);$j++)
						{
						    if(isset($s_customers[$j]) && !empty($s_customers[$j]) && $s_customers[$j] > 0 )
						    {
								  //  $this->db->query("UPDATE ".DB_PREFIX."customer set pv_value = pv_value + $pv_value  WHERE customer_id = '".$s_customers[$j]."'");
								 // $this->db->query("UPDATE ".DB_PREFIX."membertree set total_pv = total_pv + $pv_value , effective_retail_pv = effective_retail_pv + $pv_value ,  effective_faststart_pv =  effective_faststart_pv + $pv_value ,  effective_checkmatch_pv = effective_checkmatch_pv + $pv_value , effective_bronzemaker_pv = effective_bronzemaker_pv + $pv_value , effective_cycle_pv = effective_cycle_pv + $pv_value , effective_team_pv = effective_team_pv + $pv_value , effective_diamond_pv = effective_diamond_pv + $pv_value , effective_mercedes_pv = effective_mercedes_pv + $pv_value  WHERE customer_id = '".$s_customers[$j]."'");
							}
						} 
                                                 * 
                                                 */
						/********End *****************/
                                            /*    
						$amount = $response_array[9];
						$fullname =  $response_array[13].' '.$response_array[14];
						$transaction = $response_array[37];
						$shortcc = $response_array[50];
						$cardtype = $response_array[51];
 						
                                             * //$this->db->query("insert into mlm_card_transaction (amount,customerid,customername,card,ctype,ckey) values ('".$amount."','".$customerid."','".$fullname."','".$shortcc."','".$cardtype."','".$transaction."')");
                                             */
						/********End ******************************/           
						 
					
    			if ($this->config->get('config_tax_customer') == 'shipping') 
                        {
    				$this->session->data['shipping_country_id'] = $this->request->post['country_id'];
    				$this->session->data['shipping_zone_id'] = $this->request->post['zone_id'];
    				$this->session->data['shipping_postcode'] = $this->request->post['postcode'];				
    			}
     
	 		// Default Payment Address
    			if ($this->config->get('config_tax_customer') == 'payment') 
                        {
    				$this->session->data['payment_country_id'] = $this->request->post['country_id'];
    				$this->session->data['payment_zone_id'] = $this->request->post['zone_id'];			
    			}       
                    $this->language->load('mail/customer');				
    
                    /******* Notification mail to registered user ******/
                    $subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));
                    $message = sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "\n\n";
    
                    if (isset($_POST['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($_POST['customer_group_id'], $this->config->get('config_customer_group_display'))) {
                            $customer_group_id = $_POST['customer_group_id'];
                    } else {
                            $customer_group_id = $this->config->get('config_customer_group_id');
                    } 
    
                    $this->load->model('account/customer_group');
                    $customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);
    
    
    
                    if (!$customer_group_info['approval']) {
                            $message .= $this->language->get('text_login') . "\n";
                    } else {
                            $message .= $this->language->get('text_approval') . "\n";
                    }
    		/*
            		$message .= $this->url->link('account/login', '', 'SSL') . "\n\n";
            		$message .= $this->language->get('text_services') . "\n\n";
            		$message .= $this->language->get('text_thanks') . "\n";
            		$message .= $this->config->get('config_name');
            		$mail = new Mail();
            		$mail->protocol = $this->config->get('config_mail_protocol');
            		$mail->parameter = $this->config->get('config_mail_parameter');
            		$mail->hostname = $this->config->get('config_smtp_host');
            		$mail->username = $this->config->get('config_smtp_username');
            		$mail->password = $this->config->get('config_smtp_password');
            		$mail->port = $this->config->get('config_smtp_port');
            		$mail->timeout = $this->config->get('config_smtp_timeout');				
            		$mail->setTo($_POST['email']);
            		$mail->setFrom($this->config->get('config_email'));
            		$mail->setSender($this->config->get('config_name'));
            		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
            		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
            		$mail->send();*/
                    echo "<script>window.location.href='".$this->url->link('mlm/success')."'</script>";
                   //$this->redirect($this->url->link('mlm/success'));
                } 
                else 
                {
                		$this->error['warning'] = $errorMsg=$response_array[3];
              	 }			
    	} 
      	$this->data['breadcrumbs'] = array();
      	
        $this->data['breadcrumbs'][] = array(
                        'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),        	
                        'separator' => false
                ); 
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('mlm/dashboard', '', 'SSL'),      	
        	'separator' => $this->language->get('text_separator')
      	);
		
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_register'),
			'href'      => $this->url->link('mlm/register_member', '', 'SSL'),      	
        	'separator' => $this->language->get('text_separator')
      	);
		
    	$this->data['heading_title'] = $this->language->get('heading_title');		
		$this->data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('mlm/login', '', 'SSL'));
		$this->data['text_your_details'] = $this->language->get('text_your_details');
    	$this->data['text_your_address'] = $this->language->get('text_your_address');
    	$this->data['text_your_password'] = $this->language->get('text_your_password');
		$this->data['text_newsletter'] = $this->language->get('text_newsletter');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');						
    	$this->data['entry_firstname'] = $this->language->get('entry_firstname');
    	$this->data['entry_lastname'] = $this->language->get('entry_lastname');
		$this->data['entry_sponsorcode'] = $this->language->get('entry_sponsorcode');		
    	$this->data['entry_email'] = $this->language->get('entry_email');
    	$this->data['entry_telephone'] = $this->language->get('entry_telephone');
    	$this->data['entry_fax'] = $this->language->get('entry_fax');        
    	$this->data['entry_dob'] = $this->language->get('entry_dob');        
        $this->data['entry_addme'] = $this->language->get('entry_addme');
    	$this->data['entry_referrer_code'] = $this->language->get('entry_referrer_code');
        $this->data['entry_memberid'] = $this->language->get('entry_memberid');
        $this->data['entry_sponsorcode'] = $this->language->get('entry_sponsorcode');
        $this->data['entry_addme'] = $this->language->get('entry_addme');
		$this->data['entry_company'] = $this->language->get('entry_company');
		$this->data['entry_account'] = $this->language->get('entry_account');
		$this->data['entry_company_id'] = $this->language->get('entry_company_id');
		$this->data['entry_tax_id'] = $this->language->get('entry_tax_id');
    	$this->data['entry_address_1'] = $this->language->get('entry_address_1');
    	$this->data['entry_address_2'] = $this->language->get('entry_address_2');
    	$this->data['entry_postcode'] = $this->language->get('entry_postcode');
    	$this->data['entry_city'] = $this->language->get('entry_city');
    	$this->data['entry_country'] = $this->language->get('entry_country');
    	$this->data['entry_zone'] = $this->language->get('entry_zone');
		$this->data['entry_newsletter'] = $this->language->get('entry_newsletter');
    	$this->data['entry_password'] = $this->language->get('entry_password');
    	$this->data['entry_confirm'] = $this->language->get('entry_confirm');
		$this->data['button_continue'] = $this->language->get('button_continue');
    
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
                
       if (isset($this->error['error_memberid']) && !empty($this->error["error_memberid"])) {
                    
			$this->data['error_memberid'] = $this->error['error_memberid'];
		} else {
            $this->data['error_memberid'] = '';
		}
       
       if (isset($this->error['firstname'])) {
			$this->data['error_firstname'] = $this->error['firstname'];
		} else {
			$this->data['error_firstname'] = '';
		}	
		
		if (isset($this->error['lastname'])) {
			$this->data['error_lastname'] = $this->error['lastname'];
		} else {
			$this->data['error_lastname'] = '';
		}
                
       	if (isset($this->error['dob'])) {
			$this->data['error_dob'] = $this->error['dob'];
		} else {
			$this->data['error_dob'] = '';
		}
        
		if (isset($this->error['screen_name'])) {
			$this->data['error_screen_name'] = $this->error['screen_name'];
		} else {
			$this->data['error_screen_name'] = '';
		}		
	
		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}
		/*
		if (isset($this->error['telephone'])) {
			$this->data['error_telephone'] = $this->error['telephone'];
		} else {
			$this->data['error_telephone'] = '';
		}
		*/
		if (isset($this->error['ccnumber'])) {
			$this->data['error_ccnumber'] = "Please enter credit card number!";
		} else {
			$this->data['error_ccnumber'] = '';
		}
		if (isset($this->error['expdate'])) {
      		$this->data['error_expdate'] = "Please select expiry date!";
    	} else {
			$this->data['error_expdate'] = "";
		}
		if (isset($this->error['cvv'])) {
      		$this->data['error_cvv'] = "Please enter cvv number!";
    	} else {
			$this->data['error_cvv'] = "";
		}             
		
		if (isset($this->error['password'])) {
			$this->data['error_password'] = $this->error['password'];
		} else {
			$this->data['error_password'] = '';
		}
		
 		if (isset($this->error['confirm'])) {
			$this->data['error_confirm'] = $this->error['confirm'];
		} else {
			$this->data['error_confirm'] = '';
		}
		
  		if (isset($this->error['company_id'])) {
			$this->data['error_company_id'] = $this->error['company_id'];
		} else {
			$this->data['error_company_id'] = '';
		}
		
  		if (isset($this->error['tax_id'])) {
			$this->data['error_tax_id'] = $this->error['tax_id'];
		} else {
			$this->data['error_tax_id'] = '';
		}
								
  		if (isset($this->error['address_1'])) {
			$this->data['error_address_1'] = $this->error['address_1'];
		} else {
			$this->data['error_address_1'] = '';
		}
   		
		if (isset($this->error['city'])) {
			$this->data['error_city'] = $this->error['city'];
		} else {
			$this->data['error_city'] = '';
		}
		
		if (isset($this->error['postcode'])) {
			$this->data['error_postcode'] = $this->error['postcode'];
		} else {
			$this->data['error_postcode'] = '';
		}
		if (isset($this->error['country'])) {
			$this->data['error_country'] = $this->error['country'];
		} else {
			$this->data['error_country'] = '';
		}
		if (isset($this->error['zone'])) {
			$this->data['error_zone'] = $this->error['zone'];
		} else {
			$this->data['error_zone'] = '';
		}
		
    	$this->data['action'] = $this->url->link('mlm/register_member', '', 'SSL');
		
		if (isset($this->request->post['firstname'])) {
    		$this->data['firstname'] = $this->request->post['firstname'];
		} else {
			$this->data['firstname'] = '';
		}
		if (isset($this->request->post['lastname'])) {
    		$this->data['lastname'] = $this->request->post['lastname'];
		} else {
			$this->data['lastname'] = '';
		}
		if (isset($this->request->post['screen_name'])) {
    		$this->data['screen_name'] = $this->request->post['screen_name'];
		} else {
			$this->data['screen_name'] = '';
		}
		
        if (isset($this->request->post['day']) && isset($this->request->post['month']) && isset($this->request->post['year'])) {
            
    		$this->data['dob'] = $this->request->post['day']."/".$this->request->post['month']."/".$this->request->post['year'];
		} else {
			$this->data['dob'] = '';
		}
        //echo $this->data['dob'];die;
		if (isset($this->request->post['email'])) {
    		$this->data['email'] = $this->request->post['email'];
		} else {
			$this->data['email'] = '';
		}
		
		if (isset($this->request->post['telephone'])) {
    		$this->data['telephone'] = $this->request->post['telephone'];
		} else {
			$this->data['telephone'] = '';
		}
		
		if (isset($this->request->post['fax'])) {
    		$this->data['fax'] = $this->request->post['fax'];
		} else {
			$this->data['fax'] = '';
		}
                
                if (isset($this->request->post['memberid'])) {
        		$this->data['member_id'] = $this->request->post['memberid'];
		} else {
			$this->data['member_id'] = '';
		}
                
        if (isset($this->request->post['ssn'])) {
        		$this->data['ssn'] = $this->request->post['ssn'];
		} else {
			$this->data['ssn'] = '';
		}

		if (isset($this->request->post['referrer_code'])) {
    		$this->data['referrer_code'] = $this->request->post['referrer_code'];
		} else {
			$this->data['referrer_code'] = '';
		}
		
		if (isset($this->request->post['company'])) {
    		$this->data['company'] = $this->request->post['company'];
		} else {
			$this->data['company'] = '';
		}
		$this->load->model('account/customer_group');
		
		$this->data['customer_groups'] = array();
		
		if (is_array($this->config->get('config_customer_group_display'))) {
			$customer_groups = $this->model_account_customer_group->getCustomerGroups();
			
			foreach ($customer_groups as $customer_group) {
				if (in_array($customer_group['customer_group_id'], $this->config->get('config_customer_group_display'))) {
					$this->data['customer_groups'][] = $customer_group;
				}
			}
		}
		
		if (isset($this->request->post['customer_group_id'])) {
    		$this->data['customer_group_id'] = $this->request->post['customer_group_id'];
		} else {
			$this->data['customer_group_id'] = $this->config->get('config_customer_group_id');
		}
		
		// Company ID
		if (isset($this->request->post['company_id'])) {
    		$this->data['company_id'] = $this->request->post['company_id'];
		} else {
			$this->data['company_id'] = '';
		}
		if (isset($this->request->post['memberid'])) {
    		$this->data['memberid'] = $this->request->post['memberid'];
		} else {
			$this->data['memberid'] = '';
		}
		
		// Tax ID
		if (isset($this->request->post['tax_id'])) {
    		$this->data['tax_id'] = $this->request->post['tax_id'];
		} else {
			$this->data['tax_id'] = '';
		}
						
		if (isset($this->request->post['address_1'])) {
    		$this->data['address_1'] = $this->request->post['address_1'];
		} else {
			$this->data['address_1'] = '';
		}
		if (isset($this->request->post['address_2'])) {
    		$this->data['address_2'] = $this->request->post['address_2'];
		} else {
			$this->data['address_2'] = '';
		}
		if (isset($this->request->post['postcode'])) {
    		$this->data['postcode'] = $this->request->post['postcode'];
		} elseif (isset($this->session->data['shipping_postcode'])) {
			$this->data['postcode'] = $this->session->data['shipping_postcode'];		
		} else {
			$this->data['postcode'] = '';
		}
		
		if (isset($this->request->post['city'])) {
    		$this->data['city'] = $this->request->post['city'];
		} else {
			$this->data['city'] = '';
		}
    	if (isset($this->request->post['country_id'])) {
      		$this->data['country_id'] = $this->request->post['country_id'];
		} elseif (isset($this->session->data['shipping_country_id'])) {
			$this->data['country_id'] = $this->session->data['shipping_country_id'];		
		} else {	
      		$this->data['country_id'] = $this->config->get('config_country_id');
    	}
    	if (isset($this->request->post['zone_id'])) {
      		$this->data['zone_id'] = $this->request->post['zone_id']; 	
		} elseif (isset($this->session->data['shipping_zone_id'])) {
			$this->data['zone_id'] = $this->session->data['shipping_zone_id'];			
		} else {
      		$this->data['zone_id'] = '';
    	}
		
		$this->load->model('localisation/country');
		
    	$this->data['countries'] = $this->model_localisation_country->getCountries();
		
                if (isset($this->request->post['ccnumber'])) {
                       $this->data['ccnumber'] = $this->request->post['ccnumber'];
		} else {
                        $this->data['ccnumber'] = '';
		}
		
		if (!empty($this->request->post['card_month'])) {
    		$this->data['card_month'] = $this->request->post['card_month'];
		} else {
			$this->data['card_month'] = '';
		}
		if (!empty($this->request->post['card_year'])) {
    		$this->data['card_year'] = $this->request->post['card_year'];
		} else {
			$this->data['card_year'] = '';
		}
		if (isset($this->request->post['cvv'])) {
    		$this->data['cvv'] = $this->request->post['cvv'];
		} else {
			$this->data['cvv'] = '';
		}
		if (isset($this->request->post['password'])) {
    		$this->data['password'] = $this->request->post['password'];
		} else {
			$this->data['password'] = '';
		}
		
		if (isset($this->request->post['confirm'])) {
    		$this->data['confirm'] = $this->request->post['confirm'];
		} else {
			$this->data['confirm'] = '';
		}
		
		if (isset($this->request->post['newsletter'])) {
    		$this->data['newsletter'] = $this->request->post['newsletter'];
		} else {
			$this->data['newsletter'] = '';
		}	
		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');
			
			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));
			
			if ($information_info) {
				$this->data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/info', 'information_id=' . $this->config->get('config_account_id'), 'SSL'), $information_info['title'], $information_info['title']);
			} else {
				$this->data['text_agree'] = '';
			}
		} else {
			$this->data['text_agree'] = '';
		}
		
		if (isset($this->request->post['agree'])) {
      		$this->data['agree'] = $this->request->post['agree'];
		} else {
			$this->data['agree'] = false;
		}
		
	    if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mlm/register_member.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/mlm/register_member.tpl';
		} else {
			$this->template = 'default/template/mlm/register_member.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
				
		$this->response->setOutput($this->render());	
  	}
  	private function validate() {
    	if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen($this->request->post['firstname']) > 32)) {
      		$this->error['firstname'] = $this->language->get('error_firstname');
    	}
    	if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen($this->request->post['lastname']) > 32)) {
      		$this->error['lastname'] = $this->language->get('error_lastname');
    	}
       
        if ((($this->request->post['day'])=="day") || (($this->request->post['month'])=="month") ||(($this->request->post['year']) =="year")) {
      		$this->error['dob'] = $this->language->get('error_dob');
    	}

    	if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
      		$this->error['email'] = $this->language->get('error_email');
    	}
		
	 
    	if ($this->model_mlm_register_member->getTotalCustomersByEmail($this->request->post['email'])) {
      		$this->error['warning'] = $this->language->get('error_exists');
    	}
        
       
		/*
    	if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
      		$this->error['telephone'] = $this->language->get('error_telephone');
    	}*/
		
		// Customer Group
		$this->load->model('account/customer_group');
		
		if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->post['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}
		$customer_group = $this->model_account_customer_group->getCustomerGroup($customer_group_id);
			
		if ($customer_group) {	
			// Company ID
			if ($customer_group['company_id_display'] && $customer_group['company_id_required'] && !$this->request->post['company_id']) {
				$this->error['company_id'] = $this->language->get('error_company_id');
			}
			
			// Tax ID 
			if ($customer_group['tax_id_display'] && $customer_group['tax_id_required'] && !$this->request->post['tax_id']) {
				$this->error['tax_id'] = $this->language->get('error_tax_id');
			}						
		}
		
    	if ((utf8_strlen($this->request->post['address_1']) < 3) || (utf8_strlen($this->request->post['address_1']) > 128)) {
      		$this->error['address_1'] = $this->language->get('error_address_1');
    	}
    	if ((utf8_strlen($this->request->post['city']) < 2) || (utf8_strlen($this->request->post['city']) > 128)) {
      		$this->error['city'] = $this->language->get('error_city');
    	}
		$this->load->model('localisation/country');
		
		$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);
		
		if ($country_info) {
			if ($country_info['postcode_required'] && (utf8_strlen($this->request->post['postcode']) < 2) || (utf8_strlen($this->request->post['postcode']) > 10)) {
				$this->error['postcode'] = $this->language->get('error_postcode');
			}
			
			// VAT Validation
			$this->load->helper('vat');
			
			if ($this->config->get('config_vat') && $this->request->post['tax_id'] && (vat_validation($country_info['iso_code_2'], $this->request->post['tax_id']) != 'invalid')) {
				$this->error['tax_id'] = $this->language->get('error_vat');
			}
		}
    	if ($this->request->post['country_id'] == '') {
      		$this->error['country'] = $this->language->get('error_country');
    	}
		if(isset($this->request->post['zone_id']))
        {
    	if ($this->request->post['zone_id'] == '') {
      		$this->error['zone'] = $this->language->get('error_zone');
    	}
		}
		if ($this->request->post['ccnumber'] == '') {
      		$this->error['ccnumber'] = "Please enter Credit Card Number!";
    	}
	if ($this->request->post['card_month'] == '' || $this->request->post['card_year']=='') {
            $this->error['expdate'] = "Please select expiry date!";
    	}
        if ($this->request->post['cvv'] == '') {
            $this->error['cvv'] = "Please enter cvv number!";
    	}
	  
    	if ($this->model_mlm_register_member->checkMemberIDExistense($this->request->post['memberid']) == true) 
        {
            $this->error['error_memberid'] =  'Entered Member id already exist!!';
    	}
		if ($this->request->post['memberid'] == '' || strlen($this->request->post['memberid']) < 2 ) {
             $this->error['error_memberid'] = "Member ID is required !";
    	}
		
    	if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
             $this->error['password'] = $this->language->get('error_password');
    	}
    	if ($this->request->post['confirm'] != $this->request->post['password']) {
      	     $this->error['confirm'] = $this->language->get('error_confirm');
    	}	
        if ($this->config->get('config_account_id')) {
             $this->load->model('catalog/information');
             $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));
             if ($information_info && !isset($this->request->post['agree'])) {
                    $this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
             }
        }
       // echo "<pre>" ; print_r($this->error);die;
    	if (!$this->error) {
      		return true;
    	} else {
      		return false;
    	}
    }
    public function country() {
		$json = array();
		$this->load->model('localisation/country');
                $country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);
		if ($country_info) {
			$this->load->model('localisation/zone');
			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']		
			);
		}
                $this->response->setOutput(json_encode($json));
    }	
}
?>