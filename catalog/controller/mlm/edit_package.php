<?php
class ControllerMlmEditPackage extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('mlm/edi_packaget', '', 'SSL');

			$this->redirect($this->url->link('mlm/login', '', 'SSL'));
		}

		$this->language->load('mlm/edit_package');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('mlm/edit_package');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		  
          
          $customer = $_SESSION['customer_id'];
          
		  $this->model_mlm_edit_package->editPackage($this->request->post['allPackage'],$customer);
		  $this->session->data['success'] = $this->language->get('text_success');
		  $this->redirect($this->url->link('mlm/dashboard', '', 'SSL'));
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),     	
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('mlm/dashboard', '', 'SSL'),        	
			'separator' => $this->language->get('text_separator')
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_edit'),
			'href'      => $this->url->link('mlm/edit_pacakage', '', 'SSL'),       	
			'separator' => $this->language->get('text_separator')
		);

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_your_details'] = $this->language->get('text_your_details');

		$this->data['entry_packname'] = $this->language->get('entry_packname');
		
        $this->data['entry_pack_annualcost'] = $this->language->get('entry_pack_annualcost');
		
        $this->data['entry_pack_monthcost'] = $this->language->get('entry_pack_monthcost');
		
        $this->data['entry_descr'] = $this->language->get('entry_descr');
		
        $this->data['entry_image'] = $this->language->get('entry_image');
        
       	$this->data['entry_choose_package'] = $this->language->get('entry_choose_package');
        
        

		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

    
	
		$this->data['action'] = $this->url->link('mlm/edit_package', '', 'SSL');

		if ($this->request->server['REQUEST_METHOD'] != 'POST') 
        { 
			$customer_info = $this->model_mlm_edit_package->getCustomer($this->customer->getId());
            //echo "<pre>";print_r($customer_info);die;
            
            
            $pack_id = $customer_info['package_id'];
            $package_info = $this->model_mlm_edit_package->getPackagesById($pack_id);
                $this->data['pack_details']=array(
                'id'                   =>  $package_info['package_id'],
                'name'                  =>  $package_info['package_name'],
                'annual_cost'           =>  $this->currency->format($package_info['package_annual_fees']),
                'monthly_cost'          =>  $this->currency->format($package_info['package_monthly_fees']),
                'package_img'           =>  $package_info['package_img'],
                'package_descr'         =>  $package_info['package_descr']
                
                );
             
            
		}
         $allpackage = $this->model_mlm_edit_package->getPackages();
         
         $this->data['all_package']= $allpackage;
        
        /*
        $alpackage = $this->model_mlm_edit_package->getPackages();
        
        foreach($alpackage as $pack)
        {
            $all_package[] = array(
            	
                "package_id"		=> $pck['package_id'],
                "package_name"		=> $pck['package_name'],
                "package_annual_fees"=>$this->currency->format($pck['package_annual_fees']),
                "package_img"		=> $pck['package_img']              
            
            );
        }*/
        
        //echo "<pre>";print_r($all_package);

		if (isset($this->request->post['firstname'])) {
			$this->data['firstname'] = $this->request->post['firstname'];
		} elseif (isset($customer_info)) {
			$this->data['firstname'] = $customer_info['firstname'];
		} else {
			$this->data['firstname'] = '';
		}

		if (isset($this->request->post['lastname'])) {
			$this->data['lastname'] = $this->request->post['lastname'];
		} elseif (isset($customer_info)) {
			$this->data['lastname'] = $customer_info['lastname'];
		} else {
			$this->data['lastname'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (isset($customer_info)) {
			$this->data['email'] = $customer_info['email'];
		} else {
			$this->data['email'] = '';
		}

		if (isset($this->request->post['telephone'])) {
			$this->data['telephone'] = $this->request->post['telephone'];
		} elseif (isset($customer_info)) {
			$this->data['telephone'] = $customer_info['telephone'];
		} else {
			$this->data['telephone'] = '';
		}

		if (isset($this->request->post['fax'])) {
			$this->data['fax'] = $this->request->post['fax'];
		} elseif (isset($customer_info)) {
			$this->data['fax'] = $customer_info['fax'];
		} else {
			$this->data['fax'] = '';
		}

		$this->data['back'] = $this->url->link('mlm/dashboard', '', 'SSL');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mlm/edit_package.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/mlm/edit_package.tpl';
		} else {
			$this->template = 'default/template/mlm/edit_package.tpl';
		}

		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);

		$this->response->setOutput($this->render());	
	}

	protected function validate() {
		

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
    
    public function changePackage()
    {       
            $json = array(); 
           	$this->load->model('mlm/edit_package');
            
            if(isset($this->request->get['id']))
            {
                    $pack_id=$this->request->get['id'];
                   
                    
                    $get_packagedetails= $this->model_mlm_edit_package->getPackagesById($pack_id);
                    //$this->data['packdetails'] 
                    $packdetails  =array(
                    'name'                  =>  $get_packagedetails['package_name'],
                    'annual_cost'           =>  $this->currency->format($get_packagedetails['package_annual_fees']),
                    'monthly_cost'          =>  $this->currency->format($get_packagedetails['package_monthly_fees']),
                    'package_img'           =>  $get_packagedetails['package_img'],
                    'package_descr'         =>  $get_packagedetails['package_descr']
                
                    );
                    
                    
                    $value="";
                    if(isset($get_packagedetails['package_name']))
                    {
                       $value.=$get_packagedetails['package_name']."~";
                    }
                    else
                    {
                        $value.="  "."~";
                    }
                    if(isset($get_packagedetails['package_annual_fees']))
                    {
                       $value.=$this->currency->format($get_packagedetails['package_annual_fees'])."~";
                    }
                    else
                    {
                        $value.="  "."~";
                    }
                    if(isset($get_packagedetails['package_monthly_fees']))
                    {
                       $value.=$this->currency->format($get_packagedetails['package_monthly_fees'])."~";
                    }
                    else
                    {
                        $value.="  "."~";
                    }
                    
                    if(isset($get_packagedetails['package_descr']))
                    {
                       $value.=$get_packagedetails['package_descr']."~";
                    }
                    else
                    {
                        $value.="  "."~";
                    }
                    
                    
                    if(($get_packagedetails['package_img'])!="")
                    {
                       $value.=HTTP_IMAGE."packages".$get_packagedetails['package_img']."~";
                    }
                    else
                    {
                        $value.="  "."~";
                    }
                    
                  
            }
           echo $value;
		   exit();
    }
}
?>