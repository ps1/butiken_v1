<?php
//-----------------------------------------------------
// News Module for Opencart v1.5.6   					
// Modified by villagedefrance                          		
// contact@villagedefrance.net                         		
//-----------------------------------------------------

// Heading 
$_['heading_title']   	= 'News/Events';

// Text
$_['text_error']      	= 'No News!';
$_['text_more']  		= 'Full Article';
$_['text_posted'] 		= 'Posted: ';
$_['text_viewed'] 		= '(%s views) ';

// Buttons
$_['button_news']    	= 'Back';
$_['button_continue']	= 'Continue';
?>
