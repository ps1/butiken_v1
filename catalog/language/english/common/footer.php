<?php
// Text
$_['text_information']  = 'Navigation';

//Work
$_['text_service']      = 'Work';
$_['text_contact']      = 'Customer Support';
$_['text_return']       = 'Platinum Support';
$_['text_sitemap']      = 'Gold Support';
$_['text_standard_support']= 'Standard Support';
$_['text_training']        = 'Training';
$_['text_online_training'] = 'Online Training';
$_['text_custom_training'] = 'Custom Training';


//Solution
$_['text_extra']        = 'Solutions';
$_['text_manufacturer'] = 'Contact Center';
$_['text_voucher']      = 'Customer Support';
$_['text_affiliate']    = 'Help Desk';
$_['text_special']      = 'Knowledge Management';
$_['text_webservices']  = 'Web Self-Service';
$_['text_performancemetrix']  = 'Performance Metrics';

//Contact
$_['text_account']      = 'Contact';
$_['text_address']      = '8901 Marmora Road <br/> Glasgow,DO489GR.';
$_['text_order']        = '<span style="color:#000000">Freephone:</span>+1&nbsp;800&nbsp;559&nbsp;6580';
$_['text_wishlist']     = '<span style="color:#000000">Telephone:</span>+1&nbsp;959&nbsp;603&nbsp;6035';
$_['text_newsletter']   = '<span style="color:#000000">FAX:</span>+1&nbsp;504&nbsp;889&nbsp;9898';
$_['text_joinasmember']   = 'Join As Member';
$_['text_signmember']      = 'Member Sign In';
$_['text_powered']      = '@2014 All Rights Resevered Designed by Dreamsoft4u';
?>