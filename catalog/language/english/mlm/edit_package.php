<?php
// Heading 
$_['heading_title']             = 'My Package Information';

// Text
$_['text_account']              = 'Dashboard';
$_['text_edit']                 = 'Edit Packages';
$_['text_your_details']         = 'Your Package Details';
$_['text_success']              = 'Success: Your Package has been successfully updated.';

// Entry
$_['entry_packname']            = 'Package Name:';
$_['entry_pack_annualcost']     = 'Annual Cost:';
$_['entry_pack_monthcost']      = 'Monthly Cost:';
$_['entry_descr']               = 'Package Description:';
$_['entry_image']               = 'Image:';

$_['entry_choose_package']      = 'Choose Other Package';



// Error
$_['error_exists']     = 'Warning: E-Mail address is already registered!';
$_['error_firstname']  = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']   = 'Last Name must be between 1 and 32 characters!';
$_['error_email']      = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']  = 'Telephone must be between 3 and 32 characters!';
?>