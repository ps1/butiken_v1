<?php
// Heading 
$_['heading_title']      = 'My Dashboard';

// Text
$_['text_account']       = 'Dashboard';
$_['text_my_account']    = 'My Account';
$_['text_my_orders']     = 'My Orders';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Edit Profile';
$_['text_password']      = 'Change your password';

$_['text_logout']        = 'Logout';
$_['text_downline']       = 'My Downline';
$_['text_tree']           = 'My Tree';
$_['text_package']        = 'Change Package';

$_['text_address']       = 'Modify your address book entries';
$_['text_wishlist']      = 'Modify your wish list';
$_['text_order']         = 'View your order history';
$_['text_download']      = 'Downloads';
$_['text_reward']        = 'Your Reward Points'; 
$_['text_return']        = 'View your return requests'; 
$_['text_transaction']   = 'My Transactions'; 
$_['text_newsletter']    = 'Subscribe / unsubscribe to newsletter';
$_['text_recurring']     = 'Recurring payments';
$_['text_transactions']  = 'Transactions';
?>