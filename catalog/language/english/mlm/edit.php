<?php
// Heading 
$_['heading_title']     = 'My Account Information';

// Text
$_['text_account']      = 'Dashboard';
$_['text_edit']         = 'Edit Information';
$_['text_your_details'] = 'Your Personal Details';
$_['text_success']      = 'Success: Your account has been successfully updated.';
$_['text_your_address']    = 'Your Address';

$_['entry_account']      = 'Account:';
// Entry
$_['entry_firstname']  = 'First Name:';
$_['entry_lastname']   = 'Last Name:';
$_['entry_email']      = 'E-Mail:';

$_['entry_dob']         = 'Date of Birth:';
$_['entry_addme']       = 'Add Me To:';

$_['entry_company']     = 'Company:';
$_['entry_company_id']   = 'Company ID:';
$_['entry_address_1']    = 'Address1:';
$_['entry_address_2']    = 'Address2:';
$_['entry_city']        = 'City:';
$_['entry_postcode']       = 'Post Code:';
$_['entry_city']           = 'City:';
$_['entry_country']        = 'Country:';
$_['entry_zone']           = 'Region / State:';

$_['text_select']          ='Select';

$_['text_sponsor']          ='Sponsor Code:';
$_['text_pvvalue']          ='Pv Value:';
//$_['entry_telephone']  = 'Telephone:';
//$_['entry_fax']        = 'Fax:';

// Error
$_['error_exists']         = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']      = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';

$_['error_dob']            = 'Please select your Date of Birth!';

$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      = 'Telephone must be between 3 and 32 characters!';
$_['error_password']       = 'Password must be between 4 and 20 characters!';
$_['error_confirm']        = 'Password confirmation does not match password!';
$_['error_company_id']     = 'Company ID required!';
$_['error_tax_id']         = 'Tax ID required!';
$_['error_vat']            = 'VAT number is invalid!';
$_['error_address_1']      = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']           = 'City must be between 2 and 128 characters!';
$_['error_postcode']       = 'Postcode must be between 2 and 10 characters!';
$_['error_country']        = 'Please select a country!';
$_['error_zone']           = 'Please select a region / state!';
$_['error_agree']          = 'Warning: You must agree to the %s!';
?>