<?php
// Text
$_['text_title']           = 'Betala med kreditkort';
$_['text_credit_card']     = 'Kreditkortsuppgifter';
$_['text_start_date']      = '(om tillg&auml;ngligt)';
$_['text_issue']           = '(f&ouml;r Maestro och Solo kort endast)';
$_['text_wait']            = 'Var god v&auml;nta!';

// Entry
$_['entry_cc_type']        = 'Korttyp:';
$_['entry_cc_number']      = 'Kortnummer:';
$_['entry_cc_start_date']  = 'Kort g&auml;ller fr&aring;n datum:';
$_['entry_cc_expire_date'] = 'Kortets sista giltighetsdatum:';
$_['entry_cc_cvv2']        = 'CVC(CVV2):';
$_['entry_cc_issue']       = 'Kort Issue Nummer:';
?>