<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Kundtjänst';
$_['text_extra']        = 'Extra';
$_['text_contact']      = 'Kontakta oss';
$_['text_return']       = 'Reklamation/Retur';
$_['text_sitemap']      = 'Sajtkarta';
$_['text_manufacturer'] = 'Tillverkare';
$_['text_voucher']      = 'Presenkort';
$_['text_affiliate']    = 'Affiliate';
$_['text_special']      = 'Erbjudanden';
$_['text_account']      = 'Mitt konto';
$_['text_order']        = 'Orderhistorik';
$_['text_wishlist']     = 'Önskelista';
$_['text_newsletter']   = 'Nyhetsbrev';



//Work
$_['text_service']      = 'Arbete';
$_['text_contact']      = 'Kundsupport';
$_['text_return']       = 'Platina Support';
$_['text_sitemap']      = 'Gold-support';
$_['text_standard_support']= 'Standard Support';
$_['text_training']        = 'Träning';
$_['text_online_training'] = 'Onlineutbildning';
$_['text_custom_training'] = 'anpassad utbildning';


//Solution
$_['text_extra']        = 'Lösningar';
$_['text_manufacturer'] = 'Kontaktcenter';
$_['text_voucher']      = 'Kundsupport';
$_['text_affiliate']    = 'Help Desk';
$_['text_special']      = 'Knowledge Management';
$_['text_webservices']  = 'Web Självbetjäning';
$_['text_performancemetrix']  = 'Performance Metrics';

//Contact
$_['text_account']      = 'Kontakt';
$_['text_address']      = '8901 Marmora Road <br/> Glasgow,DO489GR.';
$_['text_order']        = '<span style="color:#000000">Gratisnummer:</span>+1&nbsp;800&nbsp;559&nbsp;6580';
$_['text_wishlist']     = '<span style="color:#000000">telefon:</span>+1&nbsp;959&nbsp;603&nbsp;6035';
$_['text_newsletter']   = '<span style="color:#000000">FAX:</span>+1&nbsp;504&nbsp;889&nbsp;9898';
$_['text_joinasmember']   = 'Bli medlem Som medlem';
$_['text_signmember']      = 'Medlem Logga in';
$_['text_powered']      = '@2014 Alla rättigheter resevered Skapat av Dreamsoft4u';
?>