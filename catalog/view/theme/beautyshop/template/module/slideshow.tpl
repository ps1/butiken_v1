 <ul class="bxslider">

    <?php foreach ($banners as $banner) { ?>

        <?php if ($banner['link']) { ?>
     
            <li><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></li>
    
        <?php } else { ?>
    
        <li><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></li>
    
        <?php } ?>

    <?php } ?>

</ul>
 
 <script>
 $(document).ready(function(){
  $('.bxslider').bxSlider({
  		auto: true,
		mode: 'fade'
  });
});
</script>
 