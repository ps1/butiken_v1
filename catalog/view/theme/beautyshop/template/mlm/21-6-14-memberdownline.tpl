
<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
	<?php echo $column_right; ?>
  <h1><?php echo $heading_title; ?></h1>
	
  <!-- <p><?php echo $text_total; ?><b> <?php echo $total; ?></b>.</p> -->
  
  
  <table class="list" style="width:100% !important;">
       <tr>
           <td align="left">Total left members: <?php echo $leftcount;?></td>
           <td align="right">Total Right members : <?php echo $rightcount;?></td>           
      </tr>      
  <!--    
      <tr>
        <td align="left">Total Left PV : <?php echo $lefPVtcount." PV";?></td>    
        <td align="right">Total Right PV : <?php echo $rightPVcount." PV";?></td>
      </tr> 
      
          <tr>
        <td align="left">Total Left GSV : <?php echo $left_gsv;?></td>    
        <td align="right">Total Right GSV : <?php echo $right_gsv;?></td>
      </tr> 
   -->   
      
<!--      <tr>
        <td align="left"> <input type="button" name="left_btn"  id="clickmeleft" value="View details" /></td>    
        <td align="right"> <input type="button" name="right_btn"  id="clickmeright" value="View details" /></td>
      </tr> -->
      
      <tr>
          <td align="left">
            <table class="list" style="width:100% !important;">       
                    <thead>
                      <tr>
                        <td class="center">Sr.No.</td>
                        <td class="center">Full name</td>
                        <td class="center">Member ID</td>
                        <td class="center">Email address</td>
                        <td class="center">Package</td>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (isset($left_members) && sizeof($left_members) > 0 ) { $count = 1;?>
                      <?php foreach ($left_members  as $member) { 
                          if(!isset($_GET["page"]) || $_GET["page"] == 0) { $page = 1; } else { $page = $_GET["page"]; }
                            $countd = ($page * 2) - 2 + $count;
                          ?>
                      <tr>
                        <td class="left"><?php echo $countd; ?></td>
                        <td class="left"><?php echo $member['firstname']."&nbsp;".$member['lastname']; ?></td>
                        <td class="left"><?php echo $member['member_id']; ?></td>
                        <td class="left"><?php echo $member['email']; ?></td>
                        <td class="left"><?php echo $member['package']; ?></td>
                       
                      </tr>
                      <?php $count++; } ?>
                <!--      <tr><td colspan="6">
                          <div class="pagination"><?php echo $pagination; ?></div>
                          </td>
                      </tr>
                    -->
                      <?php } else { ?>
                      <tr>
                        <td class="center" colspan="6"><?php echo "No result found!!"; ?></td>
                      </tr>
                      <?php } ?> 
                    </tbody>
                     
              </table>  
          </td>
          
          <td align="right">
              <table class="list" style="width:100% !important;">
       
                <thead>
                  <tr>
                      <td class="center">Sr.No.</td>
                    <td class="center">Full name</td>
                    <td class="center">Member ID</td>
                    <td class="center">Email address</td>
                    <td class="center">Package</td>
                   
                  </tr>
                </thead>
                <tbody>
                  <?php if (isset($right_members) && sizeof($right_members) > 0 ) { $count = 1; ?>
                  <?php foreach ($right_members  as $member) { ?>
                  <tr>
                    <td class="left"><?php echo $count; ?></td>
                    <td class="left"><?php echo $member['firstname']."&nbsp;".$member['lastname']; ?></td>
                    <td class="left"><?php echo $member['member_id']; ?></td>
                    <td class="left"><?php echo $member['email']; ?></td>
                    <td class="left"><?php echo $member['package']; ?></td>
                  
                  </tr>
                  <?php $count++; } ?>
                <!--  
                  <tr><td colspan="6">
                          <div class="pagination"><?php echo $rpagination; ?></div>
                          </td>
                      </tr>
                    -->  
                  <?php } else { ?>
                  <tr>
                    <td class="center" colspan="6"><?php echo "No result found!!"; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
          </td>
      </tr>  
  </table>
  
	 
	
	
  <div class="buttons reward-btns">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a>
    </div>
  </div>
  </div>
<?php echo $footer; ?>