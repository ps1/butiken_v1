<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <!--<h1><?php echo $heading_title; ?></h1>-->
  <?php // echo "<pre>"; print_r($pack_details); ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <h1><?php echo $text_your_details; ?></h1>
    <div class="content">
      <table class="form">
        <tr>
          <td><?php echo $entry_packname; ?></td>
          <td><span id="pack_name"><?php if(isset($pack_details['name'])) echo $pack_details['name']; ?></span></td>
        </tr>
        <tr>
          <td><?php echo $entry_pack_annualcost; ?></td>
          <td><span id="pack_annual"><?php if(isset($pack_details['annual_cost'])) echo $pack_details['annual_cost']; ?></span></td>
        </tr>
        <tr>
          <td><?php echo $entry_pack_monthcost; ?></td>
          <td><span id="pack_month"><?php if(isset($pack_details['monthly_cost'])) echo $pack_details['monthly_cost']; ?></span></td>
        </tr>
        <tr>
          <td><?php echo $entry_descr; ?></td>
          <td><span id="pack_detail"><?php if(isset($pack_details['package_descr'])) echo $pack_details['package_descr']; ?></span></td>
        </tr>
        <tr>
          <td><?php echo $entry_image; ?></td>
          <td><span id="pack_image"><?php if(($pack_details['package_img'])!=""){?><img src="<?php echo HTTP_IMAGE.'packages/'.$pack_details['package_img']?>" /><?php } ?></span></td>
        </tr>
      </table>
    </div>
    
     <h1><?php echo $entry_choose_package; ?></h1>
    
    <div class="content">
       
      <table class="form">
      
        <?php
        foreach($all_package as $packresult)
        {
        ?>
          <tr>
              <td>
                <input  type="radio" <?php if(($packresult['package_id'])==($pack_details['id'])){ echo "checked";}?> name="allPackage" value="<?php echo $packresult['package_id']?>" onchange="changePackage_details(this.id)" id='<?php echo $packresult['package_id']?>' />&nbsp;&nbsp;&nbsp;<?php echo $packresult['package_name']?>
              </td>
          </tr>
          
          <?php
          
          }?>
      </table>
    </div>
    <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="button"><?php echo $button_back; ?></a></div>
      <div class="right">
        <input type="submit" value="<?php echo $button_continue; ?>" class="button" />
      </div>
    </div>
  </form>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>

<script>
//  dataType: 'json',
//data: {'id':encodeURIComponent(id)},
function changePackage_details(id)
{
    if(id)
    {   
          $.ajax({
              url:"index.php?route=mlm/edit_package/changePackage",
              dataType: 'html',
              data: {'id':encodeURIComponent(id)},
              success: function(json) {
                
               // alert(json);
                 var res = json.split("~"); 
                 if(res[0]!=" ")
                 {
                    document.getElementById('pack_name').innerHTML=res[0]; 
                 
                 }
                  if(res[1]!=" ")
                 {
                    document.getElementById('pack_annual').innerHTML=res[1]; 
                 
                 }  
                 
                 if(res[2]!=" ")
                 {
                    document.getElementById('pack_month').innerHTML=res[2]; 
                 
                 }  
                 
                  if(res[3]!=" ")
                 {
                    document.getElementById('pack_detail').innerHTML=res[3]; 
                 
                 } 
                 if(res[4]!=" ")
                 {
                    document.getElementById('pack_image').innerHTML="<img src="+res[4]+">"; 
                 
                 }
                     
			  }              
          });      
    }
}
</script>