<?php echo $header; ?>



<script src="catalog/view/javascript/jquery.validate.js"></script>
<script>
	$().ready(function() {
	// validate the comment form when it is submitted
	$("#iboregistration").validate();
	});


</script>
<style>
input.error {
    border: 1px dotted red !important;
	background:pink !important;;
}
.primary-define .required
{
 color:black !important;
}
a.tooltip {outline:none; }
 a.tooltip strong {line-height:30px;} 
 a.tooltip:hover {text-decoration:none;} 
 a.tooltip span { z-index:10;display:none; padding:14px 20px; margin-top:-30px; margin-left:10px; width:240px; line-height:16px; } 
 a.tooltip:hover span{ display:inline; position:absolute; color:#111; border:1px solid #DCA; background:#fffAF0;} 
 .callout {z-index:20;position:absolute;top:30px;border:0;left:-12px;} 
 /*CSS3 extras*/ 
 a.tooltip span { border-radius:4px; -moz-border-radius: 4px; -webkit-border-radius: 4px; -moz-box-shadow: 5px 5px 8px #CCC; -webkit-box-shadow: 5px 5px 8px #CCC; box-shadow: 5px 5px 8px #CCC; }
</style>


<?php echo $column_right; ?>
<div id="content">
  <div class="breadcrumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>

    <?php } ?>

  </div>

  <h1><?php echo "Register Member Account"; ?></h1>

  <p class="reg-main-para"><?php echo $text_account_already; ?></p>
 
	 
  <form class="reg-frm" action="<?php echo $action; ?>" method="post" name="iboregistration" id="iboregistration" enctype="multipart/form-data">

    <h2><?php echo $text_your_details; ?></h2>

    <div class="content">

      <table class="form">      
      
			<?php if ($error_warning) { ?>
				<tr><td colspan="2" style="color:red;"><?php echo $error_warning; ?></td></tr>
			<?php } ?>
			
       
        <tr>

          <td><span class="required">*</span><?php echo $entry_firstname; ?></td>

          <td><input type="text" name="firstname"  value="<?php echo $firstname; ?>" />

            <?php if ($error_firstname) { ?>

            <span class="error"><?php echo $error_firstname; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><span class="required">*</span><?php echo $entry_lastname; ?></td>

          <td><input type="text" name="lastname" value="<?php echo $lastname; ?>" />

            <?php if ($error_lastname) { ?>

            <span class="error"><?php echo $error_lastname; ?></span> 

            <?php } ?></td>

        </tr>
        
        
        <tr>

          <td><span class="required">*</span><?php echo $entry_memberid; ?></td>

          <td><input type="text" name="memberid" value="<?php echo $memberid; ?>" />

            <?php if ($error_memberid) { ?>

            <span class="error"><?php echo $error_memberid; ?></span> 

            <?php } ?></td>

        </tr>
        
        <tr>

          <td><span class="required">&nbsp;</span> <?php echo $entry_sponsorcode; ?></td>
          <td><?=(isset($_COOKIE['sponsor']) && !empty($_COOKIE['sponsor']) ?  $_COOKIE['sponsor'] : 'B1000'); ?><input type="hidden" name="sponsor_code" value="<?=(isset($_COOKIE['sponsor_code']) && !empty($_COOKIE['sponsor_code']) ?  $_COOKIE['sponsor_code'] : 'B1000'); ?>" />
          <input type="hidden" name="sponsor_code" id="sponsor_code"  value="<?=(isset($_COOKIE['sponsor']) && !empty($_COOKIE['sponsor']) ?  $_COOKIE['sponsor'] : 'B1000'); ?>" />
          </td>
          
        </tr>
        <!-- <tr>

          <td>&nbsp;<?php echo "PV Value"; ?></td>
          <td>
          <input type="text" name="pv_value" id="pv_value"  value="" />
          </td>
          
        </tr>  

        <tr>-->
    
    
        <?php
         if(isset($dob))
         {
         $seperated =explode('/',$dob);
         }
        ?>
          <td><span class="required">*</span><?php echo $entry_dob; ?></td>
          <td>
            <select name="day" id="day">            
                <option value="day">day</option>
                <?php
               
                
                for($d=1;$d<=31;$d++)
                {
                ?>
                <option <?php if(isset($seperated[0]) && $d==($seperated[0])) echo "selected"; ?>  value="<?php echo $d;?>"><?php echo $d;?></option>
                <?php
                }
                ?>
            </select>   
            <select name="month" id="month">            
                <option value="month">Month</option>
                <?php   
                          
                        $mon_array  = array("01"=>"Jan","02" => "Feb","03" => "Mar","04"=> "Apr","05"=>"May","06" => "Jun","07"=>"Jul","08"=>"Aug","09"=>"Sep","10"=>"Oct","11"=>"Nov","12"=>"Dec");
                     
                        foreach ($mon_array as $key => $mon){
                        if(isset($_POST["month"]) && $_POST["month"] == $key) 
                        {
                            echo "<option value='".$key."' selected='selected' >".$mon."</option>";
                        }
                        else
                        {
                            echo "<option value='".$key."'>".$mon."</option>";
                        }
                    }
                ?>
            </select> 
            <select name="year" id="year">            
                <option value="year">Year</option>
                <?php
                $date=date('Y');                
                
                for($y=1978;$y<=$date;$y++)
                {   
?>
                    <option <?php if(isset($seperated[2]) && $y==$seperated[2]) echo "selected";?> value="<?php echo $y;?>"><?php echo $y?></option>
                    <?php
                }
?>
            </select> 
            
            <?php if ($error_dob) { ?>

            <span class="error"><?php echo $error_dob; ?></span> 

            <?php } ?>       
          
          </td>

        </tr>

       <tr>

          <td><span class="required">*</span><?php echo $entry_addme; ?></td>
           <td>
                  <?php if(isset($_POST["add_me"])) { ?>
                  
                  <input type="radio" name="add_me" id="add_meL" value="L" <?=($_POST["add_me"] == 'L' ? "checked='checked'" : ''); ?>  />&nbsp;<a style="text-decoration:none;" href="javascript:void(0);clickme('add_meL')">Left</a>&nbsp;
                  <input type="radio" name="add_me" id="add_meC" value="C"  <?=($_POST["add_me"] == 'C' ? "checked='checked'" : ''); ?> />&nbsp;<a style="text-decoration:none;" href="javascript:void(0);clickme('add_meC')">Center </a>
                  <input type="radio" name="add_me" id="add_meR" value="R"  <?=($_POST["add_me"] == 'R' ? "checked='checked'" : ''); ?> />&nbsp;<a style="text-decoration:none;" href="javascript:void(0);clickme('add_meR')">Right  </a>            
                  <?php } else {  ?>
                    <input type="radio" name="add_me" value="L" style="text" id="add_meL" checked="checked"/>&nbsp;<a style="text-decoration:none;" href="javascript:void(0);clickme('add_meL')">Left</a>&nbsp;
                    <input type="radio" name="add_me" value="C"  id="add_meC" />&nbsp;<a style="text-decoration:none;" href="javascript:void(0);clickme('add_meC')">Center</a>&nbsp;
                    <input type="radio" value="R"  name="add_me" id="add_meR" />&nbsp;<a style="text-decoration:none;" href="javascript:void(0);clickme('add_meR')">Right</a>            
                    <?php } ?>
                  
               
          </td>

        </tr>

        <tr>

          <td><span class="required">*</span><?php echo $entry_email; ?></td>

          <td><input type="text" name="email" value="<?php echo $email; ?>" />

            <?php if ($error_email) { ?>

            <span class="error"><?php echo $error_email; ?></span>

            <?php } ?></td>

        </tr>


      </table>

    </div>

    <h2><?php echo $text_your_address; ?></h2>

    <div class="content">

      <table class="form form-addr">

        <tr>

          <td><span class="required">&nbsp;&nbsp;</span> <?php echo $entry_company; ?></td>

          <td><input type="text" name="company" value="<?php echo $company; ?>" /></td>

        </tr>     

        <tr style="display: <?php echo (count($customer_groups) > 1 ? 'table-row' : 'none'); ?>;">

          <td><span class="required">&nbsp;&nbsp;</span>  <?php echo $entry_account; ?></td>

              <td>
                    <select name="customer_group_id">
        
                      <?php foreach ($customer_groups as $customer_group) { ?>
        
                      <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
        
                      <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
        
                      <?php } else { ?>
        
                      <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
        
                      <?php } ?>
        
                      <?php } ?>
        
                    </select>
            </td>

        </tr>         

        <tr id="company-id-display">
       
              <td><span  class="required">&nbsp;&nbsp;</span> <?php echo $entry_company_id; ?></td>
    
              <td><input type="text" name="company_id" value="<?php echo $company_id; ?>" />

                <?php if ($error_company_id) { ?>
    
                <span class="error"><?php echo $error_company_id; ?></span>
    
                <?php } ?></td>

        </tr>

        <tr id="tax-id-display">

             <td><span id="tax-id-required" class="required">*</span> <?php echo $entry_tax_id; ?></td>

            <td><input type="text" name="tax_id" value="<?php echo $tax_id; ?>" />

            <?php if ($error_tax_id) { ?>

            <span class="error"><?php echo $error_tax_id; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

            <td><span class="required">*</span> <?php echo $entry_address_1; ?></td>

            <td><input type="text" name="address_1" value="<?php echo $address_1; ?>" />

            <?php if ($error_address_1) { ?>

            <span class="error"><?php echo $error_address_1; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><span class="required">&nbsp;&nbsp;</span> <?php echo $entry_address_2; ?></td>

          <td><input type="text" name="address_2" value="<?php echo $address_2; ?>" /></td>

        </tr>

        <tr>

          <td><span class="required">*</span> <?php echo $entry_city; ?></td>

          <td><input type="text" name="city" value="<?php echo $city; ?>" />

            <?php if ($error_city) { ?>

            <span class="error"><?php echo $error_city; ?></span>

            <?php } ?></td>

        </tr>


        <tr>

          <td><span class="required">*</span> <?php echo $entry_country; ?></td>

          <td><select name="country_id"  >

              <option value=""><?php echo $text_select; ?></option>

              <?php foreach ($countries as $country) { ?>

              <?php if ($country['country_id'] == $country_id) { ?>

              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>

              <?php } else { ?>

              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>

              <?php } ?>

              <?php } ?>

            </select>

            <?php if ($error_country) { ?>

            <span class="error"><?php echo $error_country; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><span class="required">*</span> <?php echo $entry_zone; ?></td>

          <td><select name="zone_id"  >

            </select>

            <?php if ($error_zone) { ?>

            <span class="error"><?php echo $error_zone; ?></span>

            <?php } ?></td>

        </tr>


        <tr>

            <td><span class="required">&nbsp;&nbsp;</span> <?php echo $entry_postcode; ?></td>

          <td><input type="text" name="postcode" value="<?php echo $postcode; ?>" />

            <?php if ($error_postcode) { ?>

            <span class="error"><?php echo $error_postcode; ?></span>

            <?php } ?></td>

        </tr>
        
      </table>

    </div>



<!-- ***************Membership Package Selection form ********** -->
<h2><?php echo "Membership Package Selection"; ?></h2>
<div class="content">       

       <table class="form" >
        <?php 
         if(isset($_POST["choosed_package"]) && $_POST["choosed_package"] > 0) 
         {
            foreach($packages as $package) 
            {
                    $img ="";
                 $checked = '';
                 if($_POST["choosed_package"] == $package["package_id"])
                 {
                         $checked = "checked='checked'";
                 }
                
                echo "<tr>
                <td width='50%'>
                    <input type='radio' ".$checked."  name='choosed_package' id='choosed_package' onclick='updateprice(".$package["package_id"].");' value='".$package["package_id"]."' />&nbsp;&nbsp;".$package["package_name"]." &nbsp; &nbsp;".$img."                     
                    
                </td>
                <td width='30%' style='padding-top:23px;text-align:center;'>".$package["package_annual_fees"]."</td>
</tr>"  ;             
        }             
             
         } 
         else  
         {
            foreach($packages as $package) {                           
             $checked = '';
             if(isset($package["package_id"]) && $package["package_id"] == '1')
             {
                 
                 $checked = "checked='checked'";
             }
              if($package["package_img"]!="")
                 {
                    
                   
                    $img = "<img src='image/packages/".$package['package_img']."' border='0' style='width:90px;height:60px;'     />";
                 }
                 else
                 {
                    $img = "";
                 }
              echo "<tr>
                    <td width='50%'>
                    
                        <input type='radio' name='choosed_package' ".$checked."  onclick='updateprice(".$package["package_id"].");'  id='choosed_package' value='".$package["package_id"]."'  />&nbsp;&nbsp;".$package['package_name']." &nbsp;&nbsp;".$img."
                     
                    </td>
                    <td width='30%' style='padding-top:23px;text-align:center;'>".$package['package_annual_fees']."</td>        
            </tr>"  ;
         }
    
    }
?>

      </table>

    </div>    
<!--  End Membership Selection -->  

<!-- ***************credit card details ********** -->

    

    

    <h2><?php echo "Payment Details"; ?></h2>

    <div class="content">

      <table class="form">

      <!--  <tr><td style="width:100% !important;" colspan="2"><h2 style="font-size:16px;"><b><div style="font-weight:bold; ">Annual Renewal Fee: <span id='pholder'>$29.99</span>. Please fill the payment information.</div><?php // echo "Annual Registration Fee: $".$ibo_reg_fee; ?></b>
             </h2></td>
        </tr>-->

        <tr>

          <td>
          <span class="required">*</span> <?php echo "Credit Card Number"; ?></td>

          <td><input type="text" name="ccnumber" value="<?php echo $ccnumber; ?>" />

            <?php if ($error_ccnumber) { ?>

            <span class="error"><?php  echo $error_ccnumber; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><span class="required">*</span> <?php echo "Expiry Date"; ?></td>

          <td>
            <?php
            $month_array  = array("01"=>"Jan","02" => "Feb","03" => "Mar","04"=> "Apr","05"=>"May","06" => "Jun","07"=>"Jul","08"=>"Aug","09"=>"Sep","10"=>"Oct","11"=>"Nov","12"=>"Dec");
            ?>
            <select name="card_month">
            <?php    foreach ($month_array as $key => $v){
                if(isset($_POST["card_month"]) && $_POST["card_month"] == $key) 
                {
                    echo "<option value='".$key."' selected='selected' >".$v."</option>";
                }else{
                    echo "<option value='".$key."'>".$v."</option>";
                }
            }
            ?>
 

		      </select> 

		

                <select name="card_year">
                <?php 
                $y=date("Y");
                
                for($year=$y;$year<=$y+10;$year++){
                if(isset($_POST["card_year"]) && $_POST["card_year"] == $year) 
                    {
                        echo "<option value='".$year."' selected='selected' >".$year."</option>";
                    }else{
                        echo "<option value='".$year."'>".$year."</option>";
                    }
                }
                ?></select>
                
            <?php if ($error_expdate) { ?>
            <span class="error"><?php echo $error_expdate; ?></span>
            <?php } ?></td>

        </tr> 

        <tr>

            <td><span class="required">*</span> <?php echo "CVV Number"; ?></td>

            <td><input type="text" name="cvv" value="<?php echo $cvv; ?>" />

            <?php if ($error_cvv) { ?>

            <span class="error"><?php echo $error_cvv; ?></span>

            <?php } ?></td>

        </tr>

      </table>

    </div>

    

    <!-- ***************credit card details ********** -->

    

    

    <h2><?php echo $text_your_password; ?></h2>

    <div class="content">

      <table class="form">

        <tr>

          <td><span class="required">*</span> <?php echo $entry_password; ?></td>

          <td><input type="password" name="password" value="<?php echo $password; ?>" />

            <?php if ($error_password) { ?>

            <span class="error"><?php echo $error_password; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><span class="required">*</span> <?php echo $entry_confirm; ?></td>

          <td><input type="password" name="confirm"  value="<?php echo $confirm; ?>" />

            <?php if ($error_confirm) { ?>

            <span class="error"><?php echo $error_confirm; ?></span>

            <?php } ?></td>

        </tr>

      </table>

    </div>

    <h2><?php echo $text_newsletter; ?></h2>

    <div class="content">

      <table class="form">

        <tr>

          <td><?php echo $entry_newsletter; ?></td>

          <td><?php if ($newsletter) { ?>

            <input type="radio" name="newsletter" value="1" checked="checked" />

            <?php echo $text_yes; ?>

            <input type="radio" name="newsletter" value="0" />

            <?php echo $text_no; ?>

            <?php } else { ?>

            <input type="radio" name="newsletter" value="1" checked="checked" />

            <?php echo $text_yes; ?>

            <input type="radio" name="newsletter" value="0"  />

            <?php echo $text_no; ?>

            <?php } ?></td>

        </tr>

      </table>

    </div>

    <?php if ($text_agree) { ?>

    <div class="buttons">
    

      <div class="right"><?php // echo $text_agree; ?>
          I have read and agree to the <a alt="Member Terms & Agreement" href="<?php echo HTTP_SERVER;?>/index.php?route=information/information&amp;information_id=5" target="_blank"><b>Member Terms & Agreement</b></a>

        <?php if ($agree) { ?>

        <input type="checkbox" name="agree" value="1" checked="checked" />

        <?php } else { ?>

        <input type="checkbox" name="agree" value="1" />

        <?php } ?>

        <input type="submit" value="<?php echo $button_continue; ?>" class="button" onclick="return confirmssn();"/>

      </div>

    </div>

    <?php } else { ?>

    <div class="buttons">

      <div class="right">

        <input type="submit" value="<?php echo $button_continue; ?>" class="button" onclick="return confirmssn();" />

      </div>

    </div>

    <?php } ?>

  </form>
</div>

<script type="text/javascript">
function clickme(id)
{
    
    document.getElementById(id).checked="checked";
}
function confirmssn()
{
    
    ssn = document.getElementById("ssn").value;
    cssn = document.getElementById("cssn").value;
    if(ssn != cssn)
    {
        alert("SSN/ID and Confirm SSN/ID must be same..!!"); return false;
    }else{
        return true;
    }
    
}


<!--

$('select[name=\'customer_group_id\']').live('change', function() {

	var customer_group = [];

	

<?php foreach ($customer_groups as $customer_group) { ?>

	customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];

	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';

	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_required'] = '<?php echo $customer_group['company_id_required']; ?>';

	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';

	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_required'] = '<?php echo $customer_group['tax_id_required']; ?>';

<?php } ?>	



	if (customer_group[this.value]) {

		if (customer_group[this.value]['company_id_display'] == '1') {

			$('#company-id-display').show();

		} else {

			$('#company-id-display').hide();

		}

		

		if (customer_group[this.value]['company_id_required'] == '1') {

			$('#company-id-required').show();

		} else {

			$('#company-id-required').hide();

		}

		

		if (customer_group[this.value]['tax_id_display'] == '1') {

			$('#tax-id-display').show();

		} else {

			$('#tax-id-display').hide();

		}

		

		if (customer_group[this.value]['tax_id_required'] == '1') {

			$('#tax-id-required').show();

		} else {

			$('#tax-id-required').hide();

		}	

	}

});



$('select[name=\'customer_group_id\']').trigger('change');

//--></script>   

<script type="text/javascript"><!--

$('select[name=\'country_id\']').bind('change', function() {

	$.ajax({

		url: 'index.php?route=account/register/country&country_id=' + this.value,

		dataType: 'json',

		beforeSend: function() {

			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');

		},

		complete: function() {

			$('.wait').remove();

		},			

		success: function(json) {

			if (json['postcode_required'] == '1') {

				$('#postcode-required').show();

			} else {

				$('#postcode-required').hide();

			}

			

			html = '<option value=""><?php echo $text_select; ?></option>';

			

			if (json['zone'] != '') {

				for (i = 0; i < json['zone'].length; i++) {

        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';

	    			

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {

	      				html += ' selected="selected"';

	    			}

	

	    			html += '>' + json['zone'][i]['name'] + '</option>';

				}

			} else {

				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';

			}

			

			$('select[name=\'zone_id\']').html(html);

		},

		error: function(xhr, ajaxOptions, thrownError) {

			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);

		}

	});

});



$('select[name=\'country_id\']').trigger('change');

//--></script>

<script type="text/javascript"><!--

$('.colorbox').colorbox({

	width: 640,

	height: 480

});

//--></script> 

<?php echo $footer; ?>

<?php if(isset($_POST["choosed_package"]) && $_POST["choosed_package"] > 0 ) {
    echo "<script>updateprice(".$_POST["choosed_package"].");</script>";
} ?>