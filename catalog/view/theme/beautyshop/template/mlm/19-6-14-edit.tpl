<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <h2><?php echo $text_your_details; ?></h2>
    <div class="content">
      <table class="form">
        <tr>
          <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
          <td><input type="text" name="firstname" value="<?php echo $firstname; ?>" />
            <?php if ($error_firstname) { ?>
            <span class="error"><?php echo $error_firstname; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
          <td><input type="text" name="lastname" value="<?php echo $lastname; ?>" />
            <?php if ($error_lastname) { ?>
            <span class="error"><?php echo $error_lastname; ?></span>
            <?php } ?></td>
        </tr>
       
       <!-- <tr>
          <td><span class="required">*</span> <?php echo $entry_telephone; ?></td>
          <td><input type="text" name="telephone" value="<?php echo $telephone; ?>" />
            <?php if ($error_telephone) { ?>
            <span class="error"><?php echo $error_telephone; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_fax; ?></td>
          <td><input type="text" name="fax" value="<?php echo $fax; ?>" /></td>
        </tr>-->
        
        <!-- <tr>
    
    
        <?php
        
        
         if(isset($dob))
         {
         $seperated =explode('/',$dob);
         }
        ?>
          <td><span class="required">*</span><?php echo $entry_dob; ?></td>
          <td>
            <select name="day" id="day">            
                <option value="day">Day</option>
                <?php
               
                
                for($d=1;$d<=31;$d++)
                {
                ?>
                <option <?php if(isset($seperated[0]) && $d==($seperated[0])) echo "selected"; ?>  value="<?php echo $d;?>"><?php echo $d;?></option>
                <?php
                }
                ?>
            </select>   
            <select name="month" id="month">            
                <option value="month">Month</option>
                <?php   
                          
                        $mon_array  = array("01"=>"Jan","02" => "Feb","03" => "Mar","04"=> "Apr","05"=>"May","06" => "Jun","07"=>"Jul","08"=>"Aug","09"=>"Sep","10"=>"Oct","11"=>"Nov","12"=>"Dec");
                     
                        foreach ($mon_array as $key => $mon){
                        if(isset($_POST["month"]) && $_POST["month"] == $key) 
                        {
                            echo "<option value='".$key."' selected='selected' >".$mon."</option>";
                        }
                        else
                        {
                            echo "<option value='".$key."'>".$mon."</option>";
                        }
                    }
                ?>
            </select> 
            <select name="year" id="year">            
                <option value="year">Year</option>
                <?php
                
                $date=date('Y');                
                
                for($y=1950;$y<=$date;$y++)
                {
                    
                    
                    ?>
                    <option <?php if(isset($seperated[2]) && $y==$seperated[2]) echo "selected";?> value="<?php echo $y;?>"><?php echo $y?></option>
                    <?php
                }
                ?>
            </select> -->
            <!---
            <?php if($error_dob) { ?>

            <span class="error"><?php echo $error_dob; ?></span> 

            <?php } ?> -->      
          <!--
          </td>

        </tr>-->
         
         <tr>
          <td><span class="required">*</span> <?php echo $entry_email; ?></td>
          <td><input type="text" name="email" value="<?php echo $email; ?>" />
            <?php if ($error_email) { ?>
            <span class="error"><?php echo $error_email; ?></span>
            <?php } ?></td>
        </tr>
         <tr>
          <td><?php echo $text_sponsor; ?></td>
          <td><?php echo $sponsor; ?>
           </td>
        </tr>
         <tr>
          <td><?php echo $text_pvvalue; ?></td>
          <td><?php echo $pv_value; ?>
            </td>
        </tr>
      </table>
    </div>
    
    
     <h2><?php echo $text_your_address; ?></h2>

    <div class="content">

      <table class="form form-addr">

        <tr>

          <td><span class="required">&nbsp;&nbsp;</span> <?php echo $entry_company; ?></td>

          <td><input type="text" name="company" value="<?php echo $company; ?>" /></td>

        </tr>  
        <tr id="company-id-display">
       
              <td><span  class="required">&nbsp;&nbsp;</span> <?php echo $entry_company_id; ?></td>
    
              <td><input type="text" name="company_id" value="<?php echo $company_id; ?>" />

                <?php if (isset($error_company_id)) { ?>
    
                <span class="error"><?php echo $error_company_id; ?></span>
    
                <?php } ?></td>

        </tr>
       
        <tr>

            <td><span class="required">*</span> <?php echo $entry_address_1; ?></td>

            <td><input type="text" name="address_1"  class="required"  value="<?php echo $address_1; ?>" />

            <?php if (isset($error_address_1)) { ?>

            <span class="error"><?php echo $error_address_1; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><span class="required">&nbsp;&nbsp;</span> <?php echo $entry_address_2; ?></td>

          <td><input type="text" name="address_2" value="<?php echo $address_2; ?>" /></td>

        </tr>

        <tr>

          <td><span class="required">*</span> <?php echo $entry_city; ?></td>

          <td><input type="text" name="city" class="required" value="<?php echo $city; ?>" />

            <?php if ($error_city) { ?>

            <span class="error"><?php echo $error_city; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

            <td><span class="required">&nbsp;&nbsp;</span> <?php echo $entry_postcode; ?></td>

          <td><input type="text" name="postcode" value="<?php echo $postcode; ?>" />

            <?php if (isset($error_postcode)) { ?>

            <span class="error"><?php echo $error_postcode; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><span class="required">*</span> <?php echo $entry_country; ?></td>

          <td><select name="country_id"  >

              <option value=""><?php echo $text_select; ?></option>

              <?php foreach ($countries as $country) { ?>

              <?php if ($country['country_id'] == $country_id) { ?>

              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>

              <?php } else { ?>

              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>

              <?php } ?>

              <?php } ?>

            </select>

            <?php if (isset($error_country)) { ?>

            <span class="error"><?php echo $error_country; ?></span>

            <?php } ?></td>

        </tr>

        <tr>

          <td><span class="required">*</span> <?php echo $entry_zone; ?></td>

          <td><select name="zone_id"  >
                <option value="0"><?php echo $text_select;?></option>
                
                <?php
                
                foreach($zone_info as $zone)
                {
                    if($zone['zone_id']==$zone_id)
                    {
                         echo "<option selected='selected' value=".$zone['zone_id'].">".$zone['name']."</option>";
                    }
                    else
                    {
                        echo "<option value=".$zone['zone_id'].">".$zone['name']."</option>";
                    
                    }
                }
                ?>
            </select>

            <?php if (isset($error_zone)) { ?>

            <span class="error"><?php echo $error_zone; ?></span>

            <?php } ?></td>

        </tr>

      </table>

    </div>

    <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="button"><?php echo $button_back; ?></a></div>
      <div class="right">
        <input type="submit" value="<?php echo $button_continue; ?>" class="button" />
      </div>
    </div>
  </form>
  
  
  
  <?php echo $content_bottom; ?></div>
  
  <script>
  $(document).ready(function(){
    $('select[name=\'country_id\']').bind('change', function() {alert(this.value);
  
        $.ajax({

		url: 'index.php?route=mlm/edit/country&country_id=' + this.value,

		dataType: 'json',
    
                beforeSend: function() 
                {
                  
        			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
        
        		},
                
                complete: function() 
                { alert("complete");
    
    			 $('.wait').remove();
    
                },
                success: function(json) {

            			
            
            			html = '<option value=""><?php echo $text_select; ?></option>';
                        alert(json['zone'] );
                    
                        	if (json['zone'] != '') 
                            {

                			 	for (i = 0; i < json['zone'].length; i++) {
                                   
                
                        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
                
                	    			
                					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                
                	      				html += ' selected="selected"';
                
                	    			}
                
                	    			html += '>' + json['zone'][i]['name'] + '</option>';
                
                				}
                
                			 } 
                          else 
                            {
                               
                
                				html += '<option value="0" selected="selected"></option>';
                
                			}
                            
                        $('select[name=\'zone_id\']').html(html);
                        
                },

            		error: function(xhr, ajaxOptions, thrownError) {
            
            			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            
            		}
	
            });

		
        });

  });
  
  //$('select[name=\'country_id\']').trigger('change');
  
  </script>
<?php echo $footer; ?>
