<?php echo $header; ?>

<?php if ($success) { ?>
	<div class="success"><?php echo $success; ?></div>
<?php } ?>
<div id="content">
    <div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
    </div>

    <?php //echo $column_right; ?>
    <h1><?php echo $heading_title; ?></h1>
    <table cellpadding="5" cellspacing="5" border="1" class="list" style="width:100% !important;">
        <thead>
        <tr>
            <td align="left">Serial No.</td>
            <td align="left">First name</td>
            <td align="left">Last name</td>
            <td align="left">Email address</td>
            <td align="left">Level</td>
            <td align="left">Order type</td>
            <td align="left">Order total</td>
            <td align="left">Commission Percentage</td>
            <td align="left">Commission Amount</td>
            <td align="left">Order date</td>
        </tr>
        </thead>     
        <tbody>
    <?php  
    if(isset($rootorders) && sizeof($rootorders) > 0 )
    {
    $count=1;
    foreach($rootorders as $order)
    {
    $commission = ($order["total"] * $root_per)/100;
    if(isset($order["offsite_order_id"]) && !empty($order["offsite_order_id"]) && $order["offsite_order_id"] > 0 )
    {
        $otype = 'Offsite Affiliate';
    }else{
        $otype = 'WOWII';
    }
     echo "<tr>";
     echo "<td>".$count."</td>";
     echo "<td>".$order["firstname"]."</td>";
     echo "<td>".$order["lastname"]."</td>";
     echo "<td>".$order["email"]."</td>";
     echo "<td>Root Level</td>";
     echo "<td>".$otype."</td>";
     echo "<td>$".number_format($order["total"],2)."</td>";
     echo "<td>".$root_per."%</td>";
     echo "<td>$".number_format($commission,2)."</td>";
     echo "<td>".date("m/d/Y",strtotime($order["date_added"]))."</td>";
     echo "</tr>";    
     $count = $count + 1;
     }
    }
    
    ?>
<?php  
    if(isset($firstlevelorders) && sizeof($firstlevelorders) > 0 )
    {
   
    foreach($firstlevelorders as $order)
    {
    if(isset($order["offsite_order_id"]) && !empty($order["offsite_order_id"]) && $order["offsite_order_id"] > 0 )
    {
        $otype = 'Offsite Affiliate';
    }else{
        $otype = 'WOWII';
    }
    $commission = ($order["total"] * $level_per )/100;
     echo "<tr>";
     echo "<td>".$count."</td>";
     echo "<td>".$order["firstname"]."</td>";
     echo "<td>".$order["lastname"]."</td>";
     echo "<td>".$order["email"]."</td>";
     echo "<td>Level 1</td>";
     echo "<td>".$otype."</td>";
     echo "<td>$".number_format($order["total"],2)."</td>";
     echo "<td>".$level_per."%</td>";
     echo "<td>$".number_format($commission,2)."</td>";
     echo "<td>".date("m/d/Y",strtotime($order["date_added"]))."</td>";
     echo "</tr>";    
     $count = $count + 1;
     }
    }
    
    ?>        
  <?php  
    if(isset($secondlevelorders) && sizeof($secondlevelorders) > 0 )
    {
   
    foreach($secondlevelorders as $order)
    {
    if(isset($order["offsite_order_id"]) && !empty($order["offsite_order_id"]) && $order["offsite_order_id"] > 0 )
    {
        $otype = 'Offsite Affiliate';
    }else{
        $otype = 'WOWII';
    }
    
     $commission = ($order["total"] * $level_per)/100;
     echo "<tr>";
     echo "<td>".$count."</td>";
     echo "<td>".$order["firstname"]."</td>";
     echo "<td>".$order["lastname"]."</td>";
     echo "<td>".$order["email"]."</td>";
     echo "<td>Level 2</td>";
     echo "<td>".$otype."</td>";
     echo "<td>$".number_format($order["total"],2)."</td>";
     echo "<td>".$level_per."%</td>";
     echo "<td>$".number_format($commission,2)."</td>";
     echo "<td>".date("m/d/Y",strtotime($order["date_added"]))."</td>";
     echo "</tr>";    
     $count = $count + 1;
     }
    }
    
    ?>  
    
    <?php  
    if(isset($thirdlevelorders) && sizeof($thirdlevelorders) > 0 )
    {
  
    foreach($thirdlevelorders as $order)
    {
    if(isset($order["offsite_order_id"]) && !empty($order["offsite_order_id"]) && $order["offsite_order_id"] > 0 )
    {
        $otype = 'Offsite Affiliate';
    }else{
        $otype = 'WOWII';
    }
     $commission = ($order["total"] * $level_per)/100;
     echo "<tr>";
     echo "<td>".$count."</td>";
     echo "<td>".$order["firstname"]."</td>";
     echo "<td>".$order["lastname"]."</td>";
     echo "<td>".$order["email"]."</td>";
     echo "<td>Level 3</td>";
     echo "<td>".$otype."</td>";
     echo "<td>$".number_format($order["total"],2)."</td>";
     echo "<td>".$level_per."%</td>";
     echo "<td>$".number_format($commission,2)."</td>";
     echo "<td>".date("m/d/Y",strtotime($order["date_added"]))."</td>";
     echo "</tr>";    
     $count = $count + 1;
     }
    }

?>  
        </tbody>
    </table>
</div>

<?php echo $footer; ?>