<?php echo $header; ?>

<?php if ($success) { ?>
	<div class="success"><?php echo $success; ?></div>
<?php } ?>
<div id="content">
    <div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
    </div>

    <?php //echo $column_right; ?>
    <h1><?php echo $heading_title; ?></h1>
    <div style="float:right;"><a href="<?php echo $new_ticket; ?>"> New Ticket</a></div>
    <table cellpadding="5" cellspacing="5" border="1" class="list" style="width:100% !important;">
        <thead>
        <tr>
            <td align="left">Serial No.</td>
            <td align="left">Ticket No.</td>
            <td align="left">Customer name</td>
            <td align="left">Message Subject</td>
            <td align="left">Message Date</td>
            <td align="left">Status</td>   
            <td align="left">Reply</td> 
        </tr>
        </thead>     
        <tbody>
    <?php  
    if(isset($message_lists) && sizeof($message_lists) > 0 )
    {
    $count=1;
    foreach($message_lists as $msg)
    { 
     $action=$new_ticket['href'];
         $newmsg=$new_ticket['unread'];
         if($newmsg>0)
         {
         $newmsg1="&nbsp;&nbsp;(".$newmsg.")";
         }
         else
         {
          $newmsg1="";
         }
       $status = 'Pending';
        if(isset($msg["msg_status"]) && $msg["msg_status"] == '1')
        {
           $status = 'Solved';
        }
       $zero=0; 
       $totalzero=intval(2)-strlen($msg['msg_id']);
        for($i=0;$i<$totalzero;$i++)
       {
         $zero=$zero.'0';   
       }
        $ticketno=$zero.''.$msg['msg_id']; 
        echo "<tr>";
        echo "<td>".$count."</td>";
        echo "<td>".$ticketno."</td>";
        echo "<td>".$msg["custname"]."</td>";
        echo "<td>".$msg["msg_subject"]."</td>";
        echo "<td>".date("m/d/Y",strtotime($msg["msg_adddate"]))."</td>";
        echo "<td>".$status."</td>";
        foreach($msg['action'] as $msgkey=>$value)
        {
            
         $newmsg=$value['unread'];
         if($newmsg>0)
         {
         $newmsg1="&nbsp;&nbsp;(".$newmsg.")";
         }
         else
         {
          $newmsg1="";
         }
        ?>
        <td><a href="<?php echo $value['href']; ?>"><?php echo $value['text'];?>&nbsp;<span style="color:red;font-weight:bold;"><?php echo $newmsg1;?></span></a></td
        <?php
        }
        ?>
        <?php
        echo "</tr>";    
        $count = $count + 1;
     }
    }
    else{
        echo "<tr><td colspan='5'>No message found!!</td></tr>";
    }    
    ?>      
        </tbody>
    </table>
</div>

<?php echo $footer; ?>