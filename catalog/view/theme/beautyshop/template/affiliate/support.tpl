<?php echo $header; ?>

<div id="content">
    <div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
    </div>
    <br/>
<br/>
<?php if ($success) { ?>
	<div class="success"><?php echo $success; ?></div>
<?php } ?>    
<script src="catalog/view/javascript/jquery.validate.js"></script>
<script>
	$().ready(function() {
	// validate the comment form when it is submitted
	$("#custregisteration").validate();
	});
</script>

<style>
input.error {
    border: 1px dotted red;
	background:pink;
}
textarea.error {
    border: 1px dotted red;
	background:pink;
}
label.error
{
 color:red;
margin-left:5px;
}
</style>
    <?php //echo $column_right; ?>
    <h1><?php echo $heading_title; ?></h1>
    <div style="float:right;"><a href="<?php echo $new_ticket; ?>"> New Support Ticket</a></div>
    <table cellpadding="5" cellspacing="5" border="1" class="list" style="width:100% !important;">
        <thead>
        <?php
         foreach($supportmsgheader as $msg)
        {
         $zero=0; 
       $totalzero=intval(2)-strlen($msg['msg_id']);
        for($i=0;$i<$totalzero;$i++)
        {
         $zero=$zero.'0';   
        }
        $ticketno=$zero.''.$msg['msg_id']; 
   
      ?>
       <tr>
          <td>
          <h1 style="font-size:150%">Ticket Number :&nbsp;<?php echo $ticketno;?></h1>
          </td>
       </tr>
       <tr>
          <td>
          <span style="font-size:150%;font-weight:bold">Subject:<?php echo $msg['msg_subject'];?></span>
          </td>
       </tr>
       </thead> 
       <tr>
          <td> <b>Customer:</b>
           <?php echo date("d/m/Y",strtotime($msg["msg_adddate"]));?>
          </td>
        </tr> 
        <tr>
          <td>
           <?php echo $msg['msg_desc'];?>
          </td>
        </tr> 
      <?php
      }
      ?> 
          
    <?php  
    if(isset($supportmsglists) && sizeof($supportmsglists) > 0 )
    { 
    $count=1;  
    foreach($supportmsglists as $msg)
    {
    if($msg['identity']=='A')
    {
    $caption='Admin';
    }
    else
    {
    $caption='Customer';
    }
    ?>
     <tr>
        <td align="left"><b><?php echo $caption;?>&nbsp;:</b> <?php echo date("d/m/Y",strtotime($msg["msg_date"]));?></td>
    </tr>
    <tr>
    <td>
    <?php echo $msg['message'];?>
    </td>
    <?php
        echo "</tr>";    
        $count = $count + 1;
     }
    }    
    ?>      
   </tbody>
 </table>
</div>
    <div>
    <h1><?php //echo $heading_title; ?></h1>
    <form name="custregisteration" id="custregisteration" method="post">
    <table cellpadding="5" cellspacing="5" border="1" class="list" style="width:100% !important;">
       <tr>
            <td align="left" colspan="2"><b>Post a Reply</b></td>
        </tr>
        <tr>
            <td align="left">Message</td>
            <td><textarea name="msg_desc" id="msg_desc"  style="width:60%;height:170px;" ></textarea></td>
        </tr>
        
        <tr>
            <td colspan="2">
          <?php   foreach($supportmsgheader as $msg)
            {
          ?>
                <input type="hidden" name="msg_id"  value="<?php echo $msg['msg_id'];?>" style="width:120px;font-weight:bold;"/>
                <input type="hidden" name="identity"  value="<?php echo $msg['user_type'];?>" style="width:120px;font-weight:bold;"/>
                <?
             }
          ?>
                 
                 <input type="submit" name="submit" id="submit" value="Reply" style="width:120px;font-weight:bold;"/>
                 <input type="button" name="back" id="back" value="Back" onclick="window.location.href='<?php echo $back; ?>' " style="width:120px;font-weight:bold;"/>
            </td>
        
        </tr>
    </table>
   </form>
</div>
<?php echo $footer; ?>