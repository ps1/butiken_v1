<?php 
class ModelMlmRegisterMember extends Model {

public function getPackages()
{
        $query= $this->db->query("select * from ". DB_PREFIX . "mst_packages order by package_id");         
        return $query->rows;
}	
public function addCustomer($data) 
{ 
  		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
   			$customer_group_id = $data['customer_group_id'];
   		} else {
   			$customer_group_id = $this->config->get('config_customer_group_id');
   		}
	   		$customer_group_id =  2;
    		$this->load->model('account/customer_group'); 
    		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);
            $renewdate = date("Y-m-d", strtotime(date("Y-m-d") . " + 365 day"));
            
            
            $dob = date('Y-m-d',strtotime($this->db->escape($data['year']).$this->db->escape($data['month']). $this->db->escape($data['day'])));
            
            
            $pv_value = "select pv_value   from " . DB_PREFIX . "mst_packages where package_id='".$this->db->escape($data['choosed_package'])."'" ;
            $val = $this->db->query($pv_value)->row;  
            
            
            
            $sql="INSERT INTO " . DB_PREFIX . "customer SET store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', password = '" . $this->db->escape(md5($data['password'])) . "', pv_value = '" .$val. "', member_id = '" . $this->db->escape(($data['memberid'])) . "',dob='".$this->db->escape($dob)."',user_type = 'M' , newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', customer_group_id = '" . (int)$customer_group_id . "',  ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "' , package_id = '".$this->db->escape($data['choosed_package'])."' ,  date_added = NOW()";
                        
            $this->db->query($sql);
       		$customer_id = $this->db->getLastId(); 
            
            $this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', company_id = '" . $this->db->escape($data['company_id']) . "', tax_id = '" . $this->db->escape($data['tax_id']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "'");
			$address_id = $this->db->getLastId();
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
            
			/*********Insert New Order **************************/
        
        $choosed_package = (int)$data["choosed_package"];
        $package_info = $this->db->query("SELECT * FROM  " . DB_PREFIX . "mst_packages WHERE package_id = '".$choosed_package."'")->row;
        $total = $package_info["package_annual_fees"];
        
        $inv_prefix = "INV-".date("Y")."-".  date("m"); 
        $this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET 
        invoice_prefix = '" . $this->db->escape($inv_prefix) . "', 
        store_id = '0', 
        store_name = 'butiken.com The Online Evolution', 
        store_url = '', 
        customer_id = '" . (int)$customer_id. "', 
        customer_group_id = '1', 
        firstname = '" . $this->db->escape($data['firstname']) . "', 
        lastname = '" . $this->db->escape($data['lastname']) . "', 
        email = '" . $this->db->escape($data['email']) . "', 
      
        payment_lastname = '', 
        payment_company = '', 
        payment_company_id = '', 
        payment_tax_id = '" . $this->db->escape($data['tax_id']) . "', 
        payment_address_1 = '" . $this->db->escape($data['address_1']) . "', 
        payment_address_2 = '" . $this->db->escape($data['address_2']) . "', 
        payment_city = '" . $this->db->escape($data['city']) . "', 
        payment_postcode = '" . $this->db->escape($data['postcode']) . "', 
        payment_country = '" . $this->db->escape($data['country_id']) . "', 
        payment_country_id = '" . (int)$data['country_id'] . "', 
        payment_zone = '" . $this->db->escape($data['zone_id']) . "', 
        payment_zone_id = '" . (int)$data['zone_id'] . "', 
        payment_address_format = '', 
        payment_method = 'Credit Card',
        payment_code = '', 
        shipping_firstname = '" . $this->db->escape($data['firstname']) . "', 
        shipping_lastname = '" . $this->db->escape($data['lastname']) . "', 
        shipping_company = '" . $this->db->escape($data['company']) . "', 
        shipping_address_1 = '" . $this->db->escape($data['address_1']) . "', 
        shipping_address_2 = '', 
        shipping_city = '" . $this->db->escape($data['city']) . "', 
        shipping_postcode = '" . $this->db->escape($data['postcode']) . "', 
        shipping_country = '" . $this->db->escape($data['country_id']) . "',
        shipping_country_id = '" . (int)$data['country_id'] . "', 
        shipping_zone = '" . $this->db->escape($data['zone_id']) . "', 
        shipping_zone_id = '" . (int)$data['zone_id'] . "', 
        shipping_address_format = '', 
        shipping_method = '', 
        shipping_code = '', 
        comment = 'Membership Order',
        total = '" . (float)$total . "', 
        order_status_id = '1' , 
        affiliate_id = '0', 
        commission = '0.00', 
        language_id = '1', 
        user_type =  'distributor',
        package_id =  '".$choosed_package."',
        currency_id = '2', 
        currency_code = 'USD' , 
        currency_value = '1.00000000', 
        ip = '".$_SERVER['REMOTE_ADDR'] . "', 
        forwarded_ip = '', 
        user_agent = '',         
        accept_language = 'en-us', 
        date_added = '".date("Y-m-d H:i:s")."' , 
        date_modified =  '".date("Y-m-d H:i:s")."'");
        $order_id = $this->db->getLastId();        
        
        $this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$choosed_package . "', name = '" . $this->db->escape($package_info['package_name']) . "', model = '" . $this->db->escape($package_info['package_name']) . "', quantity = '1' ,   price = '" . (float)$package_info['package_annual_fees'] . "', total = '" . (float)$package_info['package_annual_fees'] . "'");
		$order_product_id = $this->db->getLastId();
        $this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '', product_option_value_id = '' ,  name = '', `value` = '' , `type` = 'distributor_package'");
         
        
        return $customer_id; 
}
	
public function editCustomer($data)
{
	
	$this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "',path = '" . $this->db->escape($data[0]) . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
}
    
public function editPassword($email, $password) 
    {
      	$this->db->query("UPDATE " . DB_PREFIX . "customer SET password = '" . $this->db->escape(md5($password)) . "' WHERE email = '" . $this->db->escape($email) . "'");
	}
public function editNewsletter($newsletter)
{
	$this->db->query("UPDATE " . DB_PREFIX . "customer SET newsletter = '" . (int)$newsletter . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
}
public function getCustomer($customer_id)
{
	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
	return $query->row;
}	
public function getCustomerByEmail($email) 
{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE email = '" . $this->db->escape($email) . "'");
		
		return $query->row;
}
     
public function getCustomerByToken($token)
{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");
		
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET token = ''");
		
		return $query->row;
}
		
public function getCustomers($data = array()) 
{
		$sql = "SELECT *, CONCAT(c.firstname, ' ', c.lastname) AS name, cg.name AS customer_group FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "customer_group cg ON (c.customer_group_id = cg.customer_group_id) ";
		$implode = array();
		
		if (isset($data['filter_name']) && !is_null($data['filter_name'])) {
			$implode[] = "LCASE(CONCAT(c.firstname, ' ', c.lastname)) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}
		
		if (isset($data['filter_email']) && !is_null($data['filter_email'])) {
			$implode[] = "c.email = '" . $this->db->escape($data['filter_email']) . "'";
		}
		
		if (isset($data['filter_customer_group_id']) && !is_null($data['filter_customer_group_id'])) {
			$implode[] = "cg.customer_group_id = '" . $this->db->escape($data['filter_customer_group_id']) . "'";
		}	
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "c.status = '" . (int)$data['filter_status'] . "'";
		}	
		
		if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
			$implode[] = "c.approved = '" . (int)$data['filter_approved'] . "'";
		}	
			
		if (isset($data['filter_ip']) && !is_null($data['filter_ip'])) {
			$implode[] = "c.customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
		}	
				
		if (isset($data['filter_date_added']) && !is_null($data['filter_date_added'])) {
			$implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
		
		if ($implode) 
                {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}
		
		$sort_data = array(
			'name',
			'c.email',
			'customer_group',
			'c.status',
			'c.ip',
			'c.date_added'
		);	
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data))
                {
			$sql .= " ORDER BY " . $data['sort'];	
		} 
                else
                {
			$sql .= " ORDER BY name";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) 
                {
			$sql .= " DESC";
		} 
                else
                {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}		
		
		$query = $this->db->query($sql);
		
		return $query->rows;	
}		
public function getTotalCustomersByEmail($email) 
{
	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(strtolower($email)) . "'");
	return $query->row['total'];
}

public function checkMemberIDExistense($memberid)
{
	$result = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "customer WHERE LOWER(member_id) = '" . $this->db->escape(strtoupper($memberid)) . "'")->row;
	if(isset($result["customer_id"]) && $result["customer_id"] > 0 )
	{
		return true;
	} else {
		return false;
	}
}
}
?>