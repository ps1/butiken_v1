<?php 
class ModelMlmEdit extends Model {

   
    public function getAddress($customer_id)
    {
    	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");
    	return $query->row;
    }
    public function getCustomer($customer_id)
    {
    	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
    	return $query->row;
    }
    public function getMemberDetails($id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "membertree WHERE customer_id = '" . (int)$id . "'");
    	return $query->row;
    }
    
    public function editCustomer($data) {
	
    //,leg = '" . $this->db->escape($data['add_me']) . "',
      $this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
      
      // $this->db->query("UPDATE " . DB_PREFIX . "membertree SET leg = '" . $this->db->escape($data['add_me']) . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
    
        $this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', company_id = '" . $this->db->escape($data['company_id']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id	 = '" . $this->db->escape($data['country_id']) . "' , zone_id = '" . $this->db->escape($data['zone_id']) . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	}
    
    public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}
}
?>